var gulp = require('gulp');
var ngAnnotate = require('gulp-ng-annotate');
var gutil = require('gulp-util');
var bower = require('bower');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var ngmin = require('gulp-ngmin');
var stripDebug = require('gulp-strip-debug');

var basePath = 'src/main/webapp';

gulp.task('buildapp', function() {
  
  var devPath = basePath + '/assets/js/app';
  var releasePath = basePath + '/release/admin/js';

  var srcs = [ 
    devPath + '/app.js',
    devPath + '/controllers/**/*.js',
    devPath + '/directives/**/*.js',
    devPath + '/filters/**/*.js',
    devPath + '/services/**/*.js'
  ];
  
  return gulp.src(srcs)
      // .pipe(ngAnnotate())
      // .pipe(ngmin({dynamic: false}))  
      // .pipe(stripDebug())  
      // .pipe(uglify({outSourceMap: false}))  
      .pipe(concat('app.min.js'))  
      .pipe(gulp.dest(releasePath))
});

//将bower的库文件对应到指定位置
gulp.task('buildlib',function(){
  
  var bowerPath = basePath + '/assets';
  var releasePath = basePath + '/release/admin/js';
  var releaseCssPath = basePath + '/release/admin/css';
  var releaseFontPath = basePath + '/release/admin/fonts';
  
  gulp.src(bowerPath + '/bower-components/jquery/dist/jquery.min.js')
      .pipe(gulp.dest(releasePath));

  gulp.src(bowerPath + '/bower-components/angular/angular.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-route/angular-route.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-animate/angular-animate.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-resource/angular-resource.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-bootstrap/ui-bootstrap-tpls.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-loading-bar/build/loading-bar.min.js')
      .pipe(gulp.dest(releasePath));
  
  gulp.src(bowerPath + '/bower-components/angular-file-upload/dist/angular-file-upload.min.js')
      .pipe(gulp.dest(releasePath));
  
  //--------------------------css-------------------------------------

  gulp.src(bowerPath + '/bower-components/bootstrap/dist/fonts/*')
      .pipe(gulp.dest(releaseFontPath));

  gulp.src(bowerPath + '/bower-components/bootstrap/dist/css/bootstrap.min.css')
      .pipe(gulp.dest(releaseCssPath));
  
  gulp.src(bowerPath + '/bower-components/angular-loading-bar/build/loading-bar.min.css')
      .pipe(gulp.dest(releaseCssPath));
  
});

/*
//takes in a callback so the engine knows when it'll be done
gulp.task('one', function(cb) {
    // do stuff -- async or otherwise
    cb(err); // if err is not null and not undefined, the run will stop, and note that it failed
});

// identifies a dependent task must be complete before this one begins
gulp.task('two', ['one'], function() {
    // task 'one' is done now
});
*/

gulp.task('default', ['buildapp', 'buildlib']);