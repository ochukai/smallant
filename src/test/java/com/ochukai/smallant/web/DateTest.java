package com.ochukai.smallant.web;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import com.ochukai.smallant.common.util.DateUtil;

public class DateTest {

    @Test
    public void test() {
        int days = DateUtil.getCurrentMonthLastDay();
        System.out.println(days);
    }

    @Test
    public void testK() {
        int days = DateUtil.getMonthLastDay(2015, 6);
        Assert.assertEquals(30, days);

        Calendar cal = Calendar.getInstance();
        int currentDay = cal.get(Calendar.DATE);
        System.out.println(days - currentDay);

    }

    @Test
    public void test3() {
        Calendar cal = DateUtil.getCurrentYearAndMonth();
        cal.add(Calendar.MONTH, 10);
        System.out.println(cal.get(Calendar.YEAR));
    }

}
