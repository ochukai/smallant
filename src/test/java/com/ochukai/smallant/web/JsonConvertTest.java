package com.ochukai.smallant.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ochukai.smallant.common.util.JsonConvertUtil;
import com.ochukai.smallant.web.vo.ProductPropVo;

public class JsonConvertTest {

    @Test
    public void testRead() throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        String json = "{\"title\":\"133 - 12312\",\"attrs\":[{\"name\":\"12\",\"value\":\"133\"},{\"name\":\"12\",\"value\":\"12312\"}],\"$$hashKey\":\"088\",\"price\":\"121\"}";
        // + "{\"title\":\"133 -
        // 123123\",\"attrs\":[{\"name\":\"12\",\"value\":\"133\"},{\"name\":\"12\",\"value\":\"123123\"}],\"$$hashKey\":\"089\",\"price\":\"123\"},"
        // + "{\"title\":\"23123 -
        // 12312\",\"attrs\":[{\"name\":\"12\",\"value\":\"23123\"},{\"name\":\"12\",\"value\":\"12312\"}],\"$$hashKey\":\"08A\",\"price\":\"123\"},"
        // + "{\"title\":\"23123 -
        // 123123\",\"attrs\":[{\"name\":\"12\",\"value\":\"23123\"},{\"name\":\"12\",\"value\":\"123123\"}],\"$$hashKey\":\"08B\",\"price\":\"123\"}";
        ProductPropVo pp = mapper.readValue(json, ProductPropVo.class);
        System.out.println(pp);
    }

    @Test
    public void testReadArray() throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "[{\"title\":\"133 - 12312\",\"attrs\":[{\"name\":\"12\",\"value\":\"133\"},{\"name\":\"12\",\"value\":\"12312\"}],\"$$hashKey\":\"088\",\"price\":\"121\"},"
                + "{\"title\":\"133 - 123123\",\"attrs\":[{\"name\":\"12\",\"value\":\"133\"},{\"name\":\"12\",\"value\":\"123123\"}],\"$$hashKey\":\"089\",\"price\":\"123\"},"
                + "{\"title\":\"23123 - 12312\",\"attrs\":[{\"name\":\"12\",\"value\":\"23123\"},{\"name\":\"12\",\"value\":\"12312\"}],\"$$hashKey\":\"08A\",\"price\":\"123\"},"
                + "{\"title\":\"23123 - 123123\",\"attrs\":[{\"name\":\"12\",\"value\":\"23123\"},{\"name\":\"12\",\"value\":\"123123\"}],\"$$hashKey\":\"08B\",\"price\":\"123\"}]";

        JavaType javaType = JsonConvertUtil.getCollectionType(mapper, ArrayList.class, ProductPropVo.class);
        List<ProductPropVo> lst = mapper.readValue(json, javaType);
        System.out.println(lst);
    }

}
