package com.ochukai.smallant.mappers;

import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.Role;

public class AdminMapperTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Test
    public void testSelectOne() {
        Admin admin = adminMapper.selectByPrimaryKey(1);
        System.out.println(admin);
    }

    @Test
    public void testSelectPage() {
        List<Admin> admins = adminMapper.selectPage("家");
        Assert.assertEquals("budui ", 1, admins.size());
        System.out.println(admins);
    }

    @Test
    public void testSelectByPrimaryKey() {
        fail("Not yet implemented");
    }

    @Test
    public void testInsert() {

        // Role role = new Role();
        // role.setName("admin");
        // roleMapper.insert(role);

        Role role = roleMapper.selectByPrimaryKey(2);

        Admin record = new Admin();
        record.setLoginName("login name");
        record.setName("name");
        record.setRoleId(role.getId());
        record.setLtdId(1);
        record.setPassword("1234");
        record.setCreateDate(new Date());
        record.setUpdateDate(new Date());
        record.setMarkForDelete(false);
        adminMapper.insert(record);

        List<Admin> admin = adminMapper.select(record);
        System.out.println(admin.size());

    }

    @Test
    public void testInsertSelective() {
        fail("Not yet implemented");
    }

    @Test
    public void testDelete() {
        fail("Not yet implemented");
    }

    @Test
    public void testDeleteByPrimaryKey() {
        fail("Not yet implemented");
    }

}
