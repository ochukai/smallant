package com.ochukai.smallant.mappers;

import java.util.Calendar;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;

public class OrderMapperTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void test() {
        Calendar cal = Calendar.getInstance();
        cal.set(2015, 2, 2);
        int count = orderMapper.countNewOrder(1, cal.getTime());
        System.out.println(count);

    }

}
