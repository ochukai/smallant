package com.ochukai.smallant.mappers;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.Product;

public class ProductMapperTest extends BaseSpringJUnitTransationTests{

    @Autowired
    private ProductMapper productMapper;
    
    @Test
    public void test() {
        Product p = productMapper.selectById(10);
        System.out.println(p);
    }

}
