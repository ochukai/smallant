package com.ochukai.smallant.services;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.Product;

public class ProductServiceTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private ProductService productService;

    @Test
    public void testQuey() {
        Product p = new Product();
        p.setMarkForDelete(false);
        PageInfo<Product> ps = productService.selectPage(1, 6, p);

        System.out.println(ps);
    }

    @Test
    public void testQueyIndex() {
        List<Product> ps = productService.selectIndexProducts();
        System.out.println(ps);
    }

}
