package com.ochukai.smallant.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.Admin;

public class AdminServiceTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private AdminService adminService;

    @Test
    public void updateStatusTest() {
        Admin admin = new Admin();
        admin.setId(1);
        System.out.println(admin);
    }

    @Test
    public void selectPageTest() {
        PageInfo<Admin> page = adminService.selectPage(1, 6, "ni");
        System.out.println(page);
    }

}
