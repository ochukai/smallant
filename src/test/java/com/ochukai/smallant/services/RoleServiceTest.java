package com.ochukai.smallant.services;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.Menu;
import com.ochukai.smallant.models.Role;

public class RoleServiceTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private RoleService roleService;

    @Test
    public void testFindByIdWithMenus() {
        int id = 1;
        Role role = roleService.findByIdWithMenus(id);
        Menu menu = role.getMenus().get(0);

        System.out.println(role);
        System.out.println(role.getMenus().size());
        System.out.println(menu);
    }

    @Test
    public void testSelectPage() {
        roleService.selectPage(1, 10, new Role());
    }

    @Test
    public void testSelectAll() {
        List<Role> role = roleService.findAllRoles();
        Assert.assertEquals("ss", 2, role.size());
    }

}
