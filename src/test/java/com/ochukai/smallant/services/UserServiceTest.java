package com.ochukai.smallant.services;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

//import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.test.BaseSpringJUnitTransationTests;
import com.ochukai.smallant.models.User;

public class UserServiceTest extends BaseSpringJUnitTransationTests {

    @Autowired
    private UserService userService;

    @Test
    public void selectPageTest() {
        // PageInfo<User> users = userService.selectPage(1, 10,new User());
        List<User> users = userService.findAllUsers();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void deleteTest() {
        User u = new User();
        u.setMarkForDelete(true);
        long count = userService.delete(u);
        Assert.assertEquals(0, count);

    }

    @Test
    public void querTest() {
        System.out.println(userService.selectPage(1, 6, "131", 1, 2));
    }

}
