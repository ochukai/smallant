package com.ochukai.smallant.services;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.test.BaseSpringJUnitTests;
import com.ochukai.smallant.common.util.IdWorker;
import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.web.vo.OrderCriteria;

public class OrderServiceTest extends BaseSpringJUnitTests {

    // @Autowired
    // private ProductService productService;

    @Autowired
    private OrderService orderService;

    @Test
    public void testSave() {
        IdWorker worker = new IdWorker(13L);
        Order order = new Order();
        order.setOrderId("" + worker.nextId());
        order.setStatus(0);
        order.setCreateDate(new Date());
        order.setMarkForDelete(false);
        order.setUpdateDate(new Date());

        order.setProductId(1);
        order.setProductName("test");
        order.setProductProps("test");
        order.setProductPrice(12D);

        order.setUserId(1);

        order.setStartTime(new Date());
        order.setDuration(6);
        order.setEndTime(new Date());
        order.setPrice(72D);

        // order.setId(0);
        // order.setAdminId(0);
        // order.setPayment("");

        orderService.save(order);
    }

    @Test
    public void testPage() {

        OrderCriteria criteria = new OrderCriteria();
        criteria.setAdminId(1);
        criteria.setRoleId(1);
        criteria.setPage(1);
        criteria.setPageSize(5);
        criteria.setName("123");

        PageInfo<Order> list = orderService.selectByPage(criteria);
        System.out.println(list);
    }

    @Test
    public void testPage1() {

        OrderCriteria criteria = new OrderCriteria();
        criteria.setAdminId(1);
        criteria.setRoleId(1);
        criteria.setPage(1);
        criteria.setPageSize(5);
        criteria.setName("123");
        criteria.setStartDate(new Date());
        criteria.setEndDate(new Date());

        PageInfo<Order> list = orderService.selectByPage(criteria);
        System.out.println(list);
    }

}
