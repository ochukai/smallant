package com.ochukai.smallant.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Error Controller. handles the calls for 404, 500 and 401 HTTP Status codes.
 */
@Controller
@RequestMapping(value = "/error")
public class ErrorController {

    /**
     * Page Not Found.
     */
    @RequestMapping(value = "/404")
    public String notFound() {
        return "error/404";
    }

}