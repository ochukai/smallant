package com.ochukai.smallant.web.controllers;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alipay.config.AlipayConfig;
import com.alipay.util.AlipayNotify;
import com.alipay.util.AlipaySubmit;
import com.alipay.util.StringUtil;
import com.alipay.util.UtilDate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ochukai.smallant.common.util.DateUtil;
import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.services.OrderService;
import com.ochukai.smallant.web.alipay.AlipayRequestInfo;
import com.ochukai.smallant.web.alipay.GoodsInfo;

@Controller
public class AlipayController {

    @Autowired
    private OrderService orderService;

    // @ResponseBody
    // @RequestMapping(value = "/getAlipayQR", method = RequestMethod.POST)
    // public JsonResult getAlipayQR(@RequestParam(value = "orderId") String
    // orderId) {
    //
    // String QRCodeUrl = "";
    //
    // if (orderId != null) {
    //
    // String WIDbiz_data = buildJsonByProID(Integer.parseInt(orderId));
    // String sHtmlText = setRequestAlipayInfo(WIDbiz_data);
    //
    // SAXReader sax = new SAXReader();
    // Document document;
    //
    // try {
    // document = sax.read(new
    // ByteArrayInputStream(sHtmlText.getBytes("UTF-8")));
    // Element root = document.getRootElement();
    //
    // // test
    // QRCodeUrl =
    // "http://www.qiyequna.com/alipay/qrcode.php?title=宝山区创业套餐&amp;price=1500&amp;phone=13188873073&amp;desc=注册资本50万以上，不限行业&amp;code=d6add4de849c10d1211c526b22faa504";
    // // root.elementText("qrcode_img_url");
    //
    // } catch (UnsupportedEncodingException | DocumentException e) {
    // e.printStackTrace();
    // }
    // }
    // return new JsonResult(false, QRCodeUrl);
    //
    // }

    public String setRequestAlipayInfo(String WIDbiz_data) {

        String sHtmlText = "";

        // 接口调用时间
        // 格式为：yyyy-MM-dd HH:mm:ss
        String timestamp = UtilDate.getDateFormatter();

        // 动作
        String method = "add";
        // 创建商品二维码
        // 业务类型
        String biz_type = "1";
        // 目前只支持1
        // 业务数据
        String biz_data = WIDbiz_data;// new

        //////////////////////////////////////////////////////////////////////////////////

        // 把请求参数打包成数组
        Map<String, String> sParaTemp = new HashMap<String, String>();
        sParaTemp.put("service", "alipay.mobile.qrcode.manage");
        sParaTemp.put("partner", AlipayConfig.partner);
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
        sParaTemp.put("timestamp", timestamp);
        sParaTemp.put("method", method);
        sParaTemp.put("biz_type", biz_type);
        sParaTemp.put("biz_data", biz_data);

        // 建立请求
        try {
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sHtmlText;
    }

    public String buildJsonByProID(int orderId) {

        ObjectMapper objectMapper = new ObjectMapper();
        GoodsInfo goodsInfo = new GoodsInfo();
        Order order = orderService.selectById(orderId);

        goodsInfo.setDesc(order.getProductProps());
        goodsInfo.setId(order.getOrderId());
        goodsInfo.setName(order.getProductName());
        goodsInfo.setPrice(order.getProductPrice().toString());

        AlipayRequestInfo aInfo = new AlipayRequestInfo();
        aInfo.setMemo("备注");
        aInfo.setNeed_address("F");
        aInfo.setNotify_url("http://");
        aInfo.setTrade_type("1");
        aInfo.setGoods_info(goodsInfo);

        StringWriter str = new StringWriter();

        try {

            System.out.println("ObjectMapper");
            objectMapper.writeValue(str, aInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return str.toString();
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST, produces = "application/json")
    public String deposit(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        // PrintWriter out = response.getWriter();
        String result = "";

        // 支付类型
        String payment_type = "1";
        // 必填，不能修改
        // 服务器异步通知页面路径
        String notify_url = "";
        // 需http://格式的完整路径，不能加?id=123这类自定义参数

        // 页面跳转同步通知页面路径
        String return_url = "http://www.antcw.com/async";
        // 需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        // 商户订单号
        String out_trade_no = request.getParameter("WIDout_trade_no");
        // 商户网站订单系统中唯一订单号，必填

        // 订单名称
        String subject = request.getParameter("WIDsubject");
        System.out.println(subject);

        // 付款金额
        String total_fee = request.getParameter("WIDtotal_fee");

        // 订单描述
        String body = request.getParameter("WIDbody");
        // 商品展示地址
        String show_url = request.getParameter("WIDshow_url");
        // 需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

        // 防钓鱼时间戳
        String anti_phishing_key = "";
        // 若要使用请调用类文件submit中的query_timestamp函数

        // 客户端的IP地址
        String exter_invoke_ip = "";

        Map<String, String> sParaTemp = new HashMap<String, String>();
        sParaTemp.put("service", "create_direct_pay_by_user");// 接口服务----即时到账
        sParaTemp.put("partner", AlipayConfig.partner);// 支付宝PID
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);// 统一编码
        sParaTemp.put("payment_type", payment_type);// 支付类型
        sParaTemp.put("notify_url", notify_url);// 异步通知页面
        sParaTemp.put("return_url", return_url);// 页面跳转同步通知页面
        sParaTemp.put("seller_email", AlipayConfig.seller_email);// 卖家支付宝账号
        sParaTemp.put("out_trade_no", out_trade_no);// 商品订单编号
        sParaTemp.put("subject", subject);// 商品名称
        sParaTemp.put("total_fee", total_fee);// 价格
        sParaTemp.put("body", body);
        sParaTemp.put("show_url", show_url);
        sParaTemp.put("anti_phishing_key", anti_phishing_key);
        sParaTemp.put("exter_invoke_ip", exter_invoke_ip);

        // 建立请求
        try {
            String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "post", "确认");

            result = "{\"success\":true,\"msg\":\"跳转成功\"}";
            StringUtil.writeToWeb(sHtmlText, "html", response);
        } catch (Exception e) {

            result = "{\"success\":false,\"msg\":\"跳转失败，请稍候再试！\"}";
            StringUtil.writeToWeb(result, "html", response);
        }

        return result;
    }

    /**
     * 同步通知的页面的 Controller
     */
    @RequestMapping(value = "/return_url")
    public String Return_url(HttpServletRequest request, HttpServletResponse response) {
        return "web/pay/success";
    }

    /**
     * 异步通知的页面的 Controller
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/async", method = RequestMethod.GET)
    public String return_url(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> params = new HashMap<String, String>();

        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }

        String orderId = request.getParameter("out_trade_no");
        String tradeStatus = request.getParameter("trade_status");
        if (AlipayNotify.verify(params)) {
            // 验证成功
            if (tradeStatus.equals("TRADE_FINISHED") || tradeStatus.equals("TRADE_SUCCESS")) {
                Order order = orderService.getOrderByOrderId(orderId);
                order.setStatus(1);
                order.setPayment("支付宝");

                Calendar cal = DateUtil.getCurrentYearAndMonth();
                int days = DateUtil.getCurrentMonthLastDay();
                int range = days - DateUtil.getCurrentDay();
                if (range < 5) {
                    cal.add(Calendar.MONTH, 1);
                }

                Calendar endCal = DateUtil.getCurrentYearAndMonth();
                endCal.add(Calendar.MONTH, order.getDuration());

                order.setStartTime(cal.getTime());
                order.setEndTime(endCal.getTime());

                orderService.updateOrder(order);
            }
            return "web/pay/success";
        } else {
            // 验证失败
            return "web/pay/fail";
        }

    }

}
