package com.ochukai.smallant.web.controllers.admin;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.User;
import com.ochukai.smallant.services.UserService;
import com.ochukai.smallant.web.vo.JsonResult;
import com.ochukai.smallant.web.vo.PageModel;

@Controller
@ResponseBody
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserService userService;

    // @RequestMapping(method = RequestMethod.POST)
    // public void create() {}

    @ResponseBody
    @RequestMapping
    public PageModel<User> query(@RequestParam(defaultValue = "1", required = false) Integer page,
            @RequestParam Integer pageSize, //
            @RequestParam(defaultValue = "", required = false) String name, //
            Model model, HttpSession session) {

        Admin admin = (Admin) session.getAttribute(Constants.ONLINE_ACCOUNT);

        User user = new User();
        user.setMarkForDelete(false);
        PageInfo<User> ps = userService.selectPage(page, pageSize, name, admin.getId(), admin.getRoleId());
        PageModel<User> pageModel = new PageModel<>();
        pageModel.setData(ps.getList());
        pageModel.setPage(ps.getPageNum());
        pageModel.setPageSize(ps.getPageSize());
        pageModel.setTotal(ps.getTotal());
        return pageModel;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User show(@PathVariable("id") int id) {
        User user = new User();
        user.setId(id);
        user = userService.selectOne(user);
        return user;
    }

    // @ResponseBody
    // @RequestMapping(value = "/update", method = RequestMethod.POST)
    // public JsonResult update(@Valid UserUpdateModel user, BindingResult
    // result, Model model) {
    //
    // if (result.hasErrors()) {
    // ErrorUtil.putInModel(result, model);
    // return new JsonResult(false);
    // }
    //
    // User dummy = new User();
    // dummy.setId(user.getId());
    // dummy.setMarkForDelete(false);
    // dummy = userService.selectOne(dummy);
    //
    // dummy.setUpdateDate(new Date());
    //
    // if (!StringUtils.isEmpty(user.getPassword())) {
    // dummy.setPassword(HashKit.md5(user.getPassword()));
    // }
    //
    // userService.update(dummy);
    // return new JsonResult(true);
    // }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public JsonResult destroy(@PathVariable("id") int id, Model model) {
        userService.deleteUser(id);
        return new JsonResult(true);
    }

}
