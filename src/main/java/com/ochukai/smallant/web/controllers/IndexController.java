package com.ochukai.smallant.web.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ochukai.smallant.models.Product;
import com.ochukai.smallant.models.ProductProp;
import com.ochukai.smallant.services.ProductService;
import com.ochukai.smallant.web.vo.ProductPropValue;
import com.ochukai.smallant.web.vo.ProductPropWithValues;

@Controller
@RequestMapping
public class IndexController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/")
    public String defalut(Model model) {
        List<Product> products = productService.selectIndexProducts();
        model.addAttribute("products", products);
        return "index";
    }

    @RequestMapping("/product/{id}")
    public String product(@PathVariable("id") int id, Model model) {
        Product product = productService.selectById(id);

        String[] features = StringUtils.split(product.getFeatures(), ";");
        String featureStr = StringUtils.join(features, " + ");

        List<ProductProp> props = product.getProps();
        List<ProductPropWithValues> propVos = new ArrayList<>();

        for (ProductProp prop : props) {
            boolean exist = false;
            ProductPropWithValues ppv = new ProductPropWithValues();
            ppv.setName(prop.getPropName());

            for (ProductPropWithValues productPropWithValue : propVos) {
                if (productPropWithValue.equals(ppv)) {
                    ppv = productPropWithValue;
                    exist = true;
                    // break;
                }
            }

            ProductPropValue value = new ProductPropValue();
            value.setId(prop.getId());
            value.setValue(prop.getPropValue());
            ppv.getValues().add(value);

            if (exist) {
                continue;
            }

            propVos.add(ppv);
        }

        model.addAttribute("product", product);
        model.addAttribute("features", featureStr);
        model.addAttribute("props", propVos);

        return "product/detail";
    }
    
    @RequestMapping("/about")
    public String aboutUs(){
        return "about";
    }

}
