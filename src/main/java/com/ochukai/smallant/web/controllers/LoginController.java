package com.ochukai.smallant.web.controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.common.util.HashKit;
import com.ochukai.smallant.models.User;
import com.ochukai.smallant.services.UserService;
import com.ochukai.smallant.web.jwt.JWT;
import com.ochukai.smallant.web.sms.SMS;
import com.ochukai.smallant.web.vo.JsonResult;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JsonResult login(@RequestParam(value = "phoneNum") String phoneNum,
            @RequestParam(value = "password") String password, HttpSession session) {
        String errorMessage = "";

        User dbUser = userService.selectByPhone(phoneNum);
        if (dbUser == null) {
            errorMessage = "NoSuchPhoneNum";
        } else {
            password = HashKit.md5(password);

            if (dbUser.getPassword().equals(password)) {
                session.setAttribute(Constants.ONLINE_USER, phoneNum);
                return new JsonResult(true, dbUser.getToken());
            }

            errorMessage = "WrongPassword";
        }

        return new JsonResult(false, errorMessage);
    }

    @ResponseBody
    @RequestMapping(value = "/loginWithToken", method = RequestMethod.POST)
    public JsonResult loginWithToken(@RequestParam(value = "token") String token, HttpSession session) {
        session.removeAttribute(Constants.ONLINE_USER);

        User user = userService.selectByToken(token);
        if (user != null && StringUtils.isNotEmpty(user.getPhone())) {
            session.setAttribute(Constants.ONLINE_USER, user.getPhone());
            return new JsonResult(true);
        }

        return new JsonResult(false);
    }

    @ResponseBody
    @RequestMapping(value = "/getCode", method = RequestMethod.POST)
    public JsonResult getCode(@RequestParam String phoneNum, HttpSession session) {
        String code = SMS.smsCode();
        session.setAttribute(Constants.ONLINE_CODE, code);

        new SMS(phoneNum, code).send();

        return new JsonResult(true);
    }

    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public JsonResult register(@RequestParam(value = "phoneNum") String phoneNum,
            @RequestParam(value = "password") String password, @RequestParam(value = "userCode") String userCode,
            HttpSession session) {
        boolean result = compareCode(session, userCode);
        if (!result) {
            return new JsonResult(false, "wrongCode");
        }

        if (userService.selectByPhone(phoneNum) != null) {
            return new JsonResult(false, "registeredPhone");
        }

        String token = JWT.sign(phoneNum);

        User user = new User();
        user.setPhone(phoneNum);
        user.setPassword(HashKit.md5(password));
        user.setToken(token);
        user.setCreateDate(new Date());
        user.setUpdateDate(new Date());
        user.setMarkForDelete(false);
        userService.save(user);

        session.setAttribute(Constants.ONLINE_USER, phoneNum);

        return new JsonResult(true, token);
    }

    @ResponseBody
    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public JsonResult resetPassword(@RequestParam(value = "phoneNum") String phoneNum,
            @RequestParam(value = "password") String newPassword, //
            @RequestParam(value = "userCode") String userCode, HttpSession session) {
        boolean result = compareCode(session, userCode);
        if (!result) {
            return new JsonResult(false, "wrongCode");
        }

        User user = userService.selectByPhone(phoneNum);
        if (user == null) {
            return new JsonResult(false, "noRegisteredPhone");
        }

        user.setPassword(HashKit.md5(newPassword));
        user.setUpdateDate(new Date());
        userService.update(user);

        session.setAttribute(Constants.ONLINE_USER, phoneNum);

        return new JsonResult(true, user.getToken());
    }

    private boolean compareCode(HttpSession session, String userCode) {
        Object code = session.getAttribute(Constants.ONLINE_CODE);
        if (code == null || !code.toString().equals(userCode)) {
            return false;
        }

        session.removeAttribute(Constants.ONLINE_CODE);
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public JsonResult logout(HttpSession session) {
        session.removeAttribute(Constants.ONLINE_USER);
        return new JsonResult(true);
    }

}
