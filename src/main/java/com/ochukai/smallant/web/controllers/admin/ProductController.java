package com.ochukai.smallant.web.controllers.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.util.StringKit;
import com.ochukai.smallant.exception.ErrorUtil;
import com.ochukai.smallant.models.Product;
import com.ochukai.smallant.models.ProductProp;
import com.ochukai.smallant.models.PropCombPrice;
import com.ochukai.smallant.services.ProductService;
import com.ochukai.smallant.web.vo.JsonResult;
import com.ochukai.smallant.web.vo.PageModel;
import com.ochukai.smallant.web.vo.ProductModel;
import com.ochukai.smallant.web.vo.ProductPropAttr;
import com.ochukai.smallant.web.vo.ProductPropVo;

@Controller
@ResponseBody
@RequestMapping("/admin/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.POST)
    public JsonResult create(@Valid ProductModel product, BindingResult result, Model model) {
        JsonResult jsonResult = new JsonResult(true);

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            jsonResult.setResult(false);
            return jsonResult;
        }

        Product prod = new Product();
        prod.setName(product.getName());
        prod.setDefaultPrice(product.getDefaultPrice());
        prod.setDescription(product.getDescription());
        prod.setPic(product.getPic());
        prod.setStatus(1);
        prod.setCreateDate(new Date());
        prod.setUpdateDate(new Date());
        prod.setMarkForDelete(false);
        prod.setFeatures(product.getFeatures());
        productService.save(prod);

        int rows = this.saveProps(product, prod);
        if (rows < 0) {
            jsonResult.setResult(false);
        }

        return jsonResult;
    }

    public int saveProps(ProductModel model, Product product) {
        int productId = product.getId();

        List<ProductPropVo> props = model.getPropsList();

        Set<ProductProp> ppSet = new HashSet<>();
        for (ProductPropVo prop : props) {
            List<ProductPropAttr> attrs = prop.getAttrs();
            for (ProductPropAttr attr : attrs) {
                ProductProp pp = new ProductProp();
                pp.setProductId(productId);
                pp.setPropName(attr.getName());
                pp.setPropValue(attr.getValue());
                pp.setMarkForDelete(false);
                pp.setCreateDate(new Date());
                pp.setUpdateDate(new Date());
                ppSet.add(pp);
            }
        }

        for (ProductProp pp : ppSet) {
            productService.saveProp(pp);
        }

        for (ProductPropVo prop : props) {
            PropCombPrice pcp = new PropCombPrice();
            pcp.setCreateDate(new Date());
            pcp.setMarkForDelete(false);
            pcp.setUpdateDate(new Date());
            pcp.setPrice(prop.getPrice() == null ? model.getDefaultPrice() : prop.getPrice());
            pcp.setProductId(productId);

            List<Integer> ids = new ArrayList<>();
            List<ProductPropAttr> attrs = prop.getAttrs();
            for (ProductPropAttr attr : attrs) {
                ProductProp pp = new ProductProp();
                pp.setProductId(productId);
                pp.setPropName(attr.getName());
                pp.setPropValue(attr.getValue());

                for (ProductProp pps : ppSet) {
                    if (pps.equals(pp)) {
                        pp = pps;
                        break;
                    }
                }
                
                ids.add(pp.getId());
            }

            String idsStr = StringKit.listToString(ids);
            pcp.setPropIds(idsStr);

            productService.savePropComb(pcp);
        }

        return productId;
    }

    @RequestMapping(method = RequestMethod.GET)
    public PageModel<Product> query(@RequestParam(defaultValue = "1", required = false, value = "page") Integer page,
            Model model) {
        Product product = new Product();
        product.setMarkForDelete(false);
        PageInfo<Product> ps = productService.selectPage(page, 6, product);
        PageModel<Product> pageModel = new PageModel<>();
        pageModel.setData(ps.getList());
        pageModel.setPage(ps.getPageNum());
        pageModel.setPageSize(ps.getPageSize());
        pageModel.setTotal(ps.getTotal());
        return pageModel;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Product show(@PathVariable("id") int id) {
        Product product = productService.selectById(id);
        return product;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public JsonResult update(@Valid ProductModel product, BindingResult result, Model model) {
        JsonResult jsonResult = new JsonResult(true);

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            return new JsonResult(false);
        }

        int productId = product.getId();

        productService.deletePropByProductId(productId);
        productService.deletePropCombByProductId(productId);

        Product prod = new Product();
        prod.setId(productId);
        prod = productService.selectOne(prod);
        prod.setName(product.getName());
        prod.setDefaultPrice(product.getDefaultPrice());
        prod.setDescription(product.getDescription());
        prod.setPic(product.getPic());
        prod.setStatus(product.getStatus());
        prod.setFeatures(product.getFeatures());
        prod.setUpdateDate(new Date());
        productService.update(prod);

        int rows = this.saveProps(product, prod);
        if (rows < 0) {
            jsonResult.setResult(false);
        }

        return new JsonResult(true);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public JsonResult destroy(@PathVariable("id") int id, Model model) {
        productService.deleteProduct(id);
        return new JsonResult(true);
    }
}
