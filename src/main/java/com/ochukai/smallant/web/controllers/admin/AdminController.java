package com.ochukai.smallant.web.controllers.admin;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.common.util.HashKit;
import com.ochukai.smallant.common.util.UploadPathUtil;
import com.ochukai.smallant.exception.ErrorUtil;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.Role;
import com.ochukai.smallant.services.AdminService;
import com.ochukai.smallant.services.RoleService;
import com.ochukai.smallant.web.vo.AdminLoginModel;
import com.ochukai.smallant.web.vo.AdminModel;
import com.ochukai.smallant.web.vo.AdminUpdateModel;
import com.ochukai.smallant.web.vo.JsonResult;
import com.ochukai.smallant.web.vo.PageModel;
import com.ochukai.smallant.web.vo.UploadResult;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private RoleService roleService;

    @RequestMapping
    public String index() {
        return "/admin/index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(@Valid AdminLoginModel admin, BindingResult result, Model model, HttpSession session) {

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            model.addAttribute("result", false);
            return;
        }

        String hashPwd = HashKit.md5(admin.getPassword());
        Admin adminFromDB = adminService.findByLoginNameAndPassword(admin.getLoginName(), hashPwd);

        if (adminFromDB != null) {
            // put in session.
            session.setAttribute(Constants.ONLINE_ACCOUNT, adminFromDB);
            model.addAttribute("result", true);
            model.addAttribute("adminLogin", null);
            // update online status
            adminService.updateLoginTime(adminFromDB);
        } else {
            model.addAttribute("result", false);
            model.addAttribute("message", "用户名不存在或密码错误！");
        }
    }

    @RequestMapping(value = "/mymenus", method = RequestMethod.POST)
    public void myMenus(Model model, HttpSession session) {
        Admin adminInSession = (Admin) session.getAttribute(Constants.ONLINE_ACCOUNT);

        Role role = roleService.findByIdWithMenus(adminInSession.getRoleId());
        model.addAttribute("result", true);
        model.addAttribute("menus", role.getMenus());
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout(Model model, HttpSession session) {
        session.removeAttribute(Constants.ONLINE_ACCOUNT);
    }

    @ResponseBody
    @RequestMapping(value = "/uploads", method = RequestMethod.POST)
    public UploadResult uploads(@RequestParam MultipartFile file,
            @RequestParam(required = false, defaultValue = "file") String dir, HttpServletRequest request,
            Model model) {

        String urlPath = null;

        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = UploadPathUtil.getSavePath(request, dir, fileName);
            String name = UploadPathUtil.getTimeFileName(fileName);
            File targetFile = new File(path, name);

            try {
                file.transferTo(targetFile);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            urlPath = UploadPathUtil.getUrlPath(dir, fileName) + "/" + name;
        }

        UploadResult result = new UploadResult();
        result.setError((urlPath == null) ? 1 : 0);
        result.setUrl(urlPath);

        return result;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public void create(@Valid AdminModel admin, BindingResult result, Model model) {

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            model.addAttribute("result", false);
            return;
        }

        Admin dummy = new Admin();
        dummy.setLoginName(admin.getLoginName());
        dummy.setName(admin.getName());
        dummy.setPassword(HashKit.md5(admin.getPassword()));
        dummy.setRoleId(admin.getRoleId());
        dummy.setLtdId(1);
        dummy.setCreateDate(new Date());
        dummy.setUpdateDate(new Date());
        dummy.setMarkForDelete(false);

        int id = adminService.save(dummy);
        model.addAttribute("result", (id > 0));
    }

    @ResponseBody
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public PageModel<Admin> query(@RequestParam(defaultValue = "1", required = false, value = "page") Integer page,
            @RequestParam Integer pageSize, //
            @RequestParam(defaultValue = "", required = false) String name,
            Model model) {
        PageInfo<Admin> ps = adminService.selectPage(page, pageSize, name);
        PageModel<Admin> pageModel = new PageModel<>();
        pageModel.setData(ps.getList());
        pageModel.setPage(ps.getPageNum());
        pageModel.setPageSize(ps.getPageSize());
        pageModel.setTotal(ps.getTotal());
        return pageModel;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/{id}", method = RequestMethod.GET)
    public Admin show(@PathVariable("id") int id) {
        Admin admin = new Admin();
        admin.setId(id);
        admin = adminService.selectOne(admin);
        return admin;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/update", method = RequestMethod.POST)
    public JsonResult update(@Valid AdminUpdateModel admin, BindingResult result, Model model) {

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            return new JsonResult(false);
        }

        Admin dummy = new Admin();
        dummy.setId(admin.getId());
        dummy = adminService.selectOne(dummy);

        dummy.setLoginName(admin.getLoginName());
        dummy.setName(admin.getName());
        dummy.setRoleId(admin.getRoleId());
        dummy.setUpdateDate(new Date());

        if (!StringUtils.isEmpty(admin.getNewPassword()) && !StringUtils.isEmpty(admin.getReNewPassword())
                && admin.getReNewPassword().equals(admin.getNewPassword())) {
            dummy.setPassword(HashKit.md5(admin.getNewPassword()));
        }

        adminService.update(dummy);
        return new JsonResult(true);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public JsonResult destroy(@PathVariable("id") int id, Model model) {
        adminService.deleteAdmin(id);
        return new JsonResult(true);
    }

    @RequestMapping(value = "/admin/allRoles", method = RequestMethod.POST)
    public void allRoles(Model model) {
        List<Role> roles = roleService.findAllRoles();
        model.addAttribute("roles", roles);
    }

}
