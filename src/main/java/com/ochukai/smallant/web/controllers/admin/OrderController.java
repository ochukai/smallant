package com.ochukai.smallant.web.controllers.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.exception.ErrorUtil;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.services.OrderService;
import com.ochukai.smallant.web.vo.JsonResult;
import com.ochukai.smallant.web.vo.OrderCriteria;
import com.ochukai.smallant.web.vo.PageModel;
import com.ochukai.smallant.web.vo.ProductModel;

@Controller("adminOrderController")
@ResponseBody
@RequestMapping("/admin/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.GET)
    public PageModel<Order> query(OrderCriteria criteria, HttpSession session, Model model) {
        Admin admin = (Admin) session.getAttribute(Constants.ONLINE_ACCOUNT);
        criteria.setAdminId(admin.getId());
        criteria.setRoleId(admin.getRoleId());

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (StringUtils.isNotEmpty(criteria.getStartDateStr())) {
                criteria.setStartDate(sf.parse(criteria.getStartDateStr()));
            }

            if (StringUtils.isNotEmpty(criteria.getEndDateStr())) {
                criteria.setEndDate(sf.parse(criteria.getEndDateStr()));
            }
        } catch (ParseException e) {
            // e.printStackTrace();
        }

        PageInfo<Order> ps = orderService.selectByPage(criteria);

        PageModel<Order> pageModel = new PageModel<>();
        pageModel.setData(ps.getList());
        pageModel.setPage(ps.getPageNum());
        pageModel.setPageSize(ps.getPageSize());
        pageModel.setTotal(ps.getTotal());
        return pageModel;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Order show(@PathVariable("id") int id) {
        Order order = orderService.selectById(id);
        return order;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public JsonResult update(@Valid ProductModel order, BindingResult result, Model model) {

        if (result.hasErrors()) {
            ErrorUtil.putInModel(result, model);
            return new JsonResult(false);
        }

        return new JsonResult(true);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public JsonResult destroy(@PathVariable("id") int id, Model model) {
        orderService.deleteOrder(id);
        return new JsonResult(true);
    }

    @RequestMapping(value = "pull", method = RequestMethod.POST)
    public JsonResult pull(HttpSession session) {
        JsonResult result = new JsonResult();

        Admin admin = (Admin) session.getAttribute(Constants.ONLINE_ACCOUNT);
        orderService.countNewOrderAndUpdateLastPullTime(admin.getId());

        result.setResult(true);
        return result;
    }

}
