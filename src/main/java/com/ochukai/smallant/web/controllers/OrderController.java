package com.ochukai.smallant.web.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.common.util.IdWorker;
import com.ochukai.smallant.models.CompanyInfo;
import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.models.OrderProgress;
import com.ochukai.smallant.models.Product;
import com.ochukai.smallant.models.ProductProp;
import com.ochukai.smallant.models.User;
import com.ochukai.smallant.services.CompanyInfoService;
import com.ochukai.smallant.services.OrderService;
import com.ochukai.smallant.services.ProductService;
import com.ochukai.smallant.services.UserService;
import com.ochukai.smallant.web.vo.CompanyInfoVo;
import com.ochukai.smallant.web.vo.JsonResult;
import com.ochukai.smallant.web.vo.PreOrder;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private CompanyInfoService companyInfoService;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    @ResponseBody
    @RequestMapping(value = "/pre", method = RequestMethod.POST)
    public JsonResult buy(PreOrder preOrder, Model model, HttpSession session) {
        Order order = new Order();
        order.setOrderId("" + new IdWorker(13L).nextId());
        order.setStatus(0);
        order.setCreateDate(new Date());
        order.setMarkForDelete(false);
        order.setUpdateDate(new Date());

        Product prod = productService.selectById(preOrder.getProduct());
        order.setProductId(prod.getId());
        order.setProductName(prod.getName());
        order.setProductPic(prod.getPic());
        String propStrs = buildProps(preOrder.getIds(), prod.getProps());
        order.setProductProps(propStrs);

        Double productPrice = preOrder.getPrice() / preOrder.getDuration();
        order.setProductPrice(productPrice);

        String phone = session.getAttribute(Constants.ONLINE_USER).toString();
        User user = userService.selectByPhone(phone);
        order.setUserId(user.getId());
        order.setDuration(preOrder.getDuration());
        order.setPrice(preOrder.getPrice());

        int rows = orderService.save(order);

        JsonResult result = new JsonResult();
        result.setResult(rows == 1);

        result.setMessage("/order/topay/" + order.getId());
        return result;
    }
    
    @RequestMapping("/orderToBuy/{orderId}")
    public String OrderToBuy(@PathVariable String orderId, Model model) {
        Order order = orderService.getOrderByOrderId(orderId);
        String props = order.getProductProps();
        String beautyProps = buildBeatutyProps(props);
        model.addAttribute("order", order);
        model.addAttribute("props", beautyProps);
        return "order/pay";
    }

    private String buildProps(String ids, List<ProductProp> props) {
        List<String> propStrList = new ArrayList<>();

        String[] idStringArray = ids.split(",");
        for (String idStr : idStringArray) {
            for (ProductProp prop : props) {
                if (prop.getId().toString().equals(idStr)) {
                    String propStr = String.format("%s-%s", prop.getPropName(), prop.getPropValue());
                    propStrList.add(propStr);
                }
            }
        }

        return StringUtils.join(propStrList, ",");
    }

    @RequestMapping(value = "/topay/{id}", method = RequestMethod.GET)
    public String toPay(@PathVariable Integer id, Model model) {
        Order order = orderService.selectById(id);
        String props = order.getProductProps();
        String beautyProps = buildBeatutyProps(props);
        model.addAttribute("order", order);
        model.addAttribute("props", beautyProps);
        return "order/pay";
    }

    private String buildBeatutyProps(String props) {
        List<String> retProps = new ArrayList<>();

        String[] propArray = props.split(",");
        for (String prop : propArray) {
            String[] attrs = prop.split("-");
            retProps.add(String.format("%s (%s)", attrs[0], attrs[1]));
        }

        String[] arr = (String[]) retProps.toArray(new String[retProps.size()]);
        return StringUtils.join(arr, " & ");
    }

    @RequestMapping("/{id}/result")
    public String result(@PathVariable String id, Model model) {
        model.addAttribute("orderId", id);
        return "order/success";
    }

    /*
     * orderId:2485141563438080 name:sdfasdf taxCode:s1234234 manager:sdfsdf
     * phone:021-55462551 addressPrefix:上海-黄浦区 receiverAddress:sdffdghsd
     * receiver:sdadsfasdf receiverPhone:021-12345789
     */
    @RequestMapping(value = "/companyinfo", method = RequestMethod.POST)
    public JsonResult addCompanyInfo(@Valid CompanyInfoVo infoVo, BindingResult bindResult, Model model) {
        JsonResult result = new JsonResult();

        // if (bindResult.hasErrors()) {
        // ErrorUtil.putInModel(bindResult, model);
        // result.setResult(false);
        // return result;
        // }

        CompanyInfo info = new CompanyInfo();
        info.setCreateDate(new Date());
        info.setUpdateDate(new Date());
        info.setMarkForDelete(false);
        info.setUserId(1);

        info.setName(infoVo.getName());
        info.setTaxCode(infoVo.getTaxCode());
        info.setManager(infoVo.getManager());
        info.setPhone(infoVo.getPhone());
        info.setReceiver(infoVo.getReceiver());
        info.setReceiveAddress(infoVo.getAddressPrefix() + "-" + infoVo.getReceiverAddress());
        info.setReceiverPhone(infoVo.getReceiverPhone());

        int rows = companyInfoService.save(info);
        if (rows == 1) {
            Order order = orderService.getOrderByOrderId(infoVo.getOrderId());
            order.setCompanyInfoId(info.getId());
            order.setStatus(2);
            orderService.update(order);
            result.setResult(true);
        }

        return result;
    }

    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public String orderList(Model model, HttpSession session) {
        String phone = session.getAttribute(Constants.ONLINE_USER).toString();
        User user = new User();
        user.setPhone(phone);
        user = userService.selectOne(user);

        List<Order> orders = orderService.getOrderByUserId(user.getId());
        CompanyInfo c = new CompanyInfo();
        c.setUserId(user.getId());
        List<CompanyInfo> companyInfos = companyInfoService.getCompanyInfoList(c);
        HashMap<Integer, CompanyInfo> CompanyInfoKeyDict = new HashMap<Integer, CompanyInfo>();

        for (int i = 0; i < companyInfos.size(); i++) {
            if (!CompanyInfoKeyDict.containsKey(companyInfos.get(i).getId())) {
                CompanyInfoKeyDict.put(companyInfos.get(i).getId(), companyInfos.get(i));
            }
        }

        Integer length = orders.size();
        for (int i = 0; i < length; i++) {
            if (orders.get(i) != null) {
                Integer id = orders.get(i).getCompanyInfoId();
                orders.get(i).setCompanyInfo(CompanyInfoKeyDict.get(id));
            }
        }

        model.addAttribute("orders", orders);
        return "order/list";
    }

    @RequestMapping("/detail/{orderId}")
    public String detail(@PathVariable String orderId, Model model, HttpSession session) {
        Order order = orderService.getOrderByOrderId(orderId);

        List<OrderProgress> progressList = orderService.getOrderProgressByOrderId(order.getId());

        order.setCompanyInfo(companyInfoService.selectById(order.getCompanyInfoId()));
        model.addAttribute("order", order);
        model.addAttribute("progressList", progressList);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");// 设置日期格式
        String month = df.format(new Date());
        model.addAttribute("month", month);

        return "order/detail";
    }

    @ResponseBody
    @RequestMapping(value = "/orderCancel/{orderId}", method = RequestMethod.POST)
    public JsonResult orderCancel(@PathVariable String orderId, HttpSession session) {
        Order order = orderService.getOrderByOrderId(orderId);
        order.setMarkForDelete(true);
        int result = 0;
        result = orderService.save(order);
        if (result == 0) {
            return new JsonResult(false);
        }
        return new JsonResult(true);

    }

    @RequestMapping("/upload")
    public String upload(Model model, HttpSession session) {
        return "order/upload";
    }

    @RequestMapping(value = "/UploadFile1", method = RequestMethod.POST)
    public JsonResult uploadFile1(@Valid CompanyInfoVo infoVo, BindingResult bindResult, Model model) {
        JsonResult result = new JsonResult();
        return result;
    }

    @RequestMapping(value = "/UploadFile", method = RequestMethod.POST)
    public String iconUpload(MultipartFile iconImg, HttpServletRequest request, String aas)
            throws OSSException, ClientException, IOException {
        String path = request.getSession().getServletContext().getRealPath("upload");

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

        MultipartFile file = multipartRequest.getFile("iconImg");

        String bucketName = "smallant";
        String key = "blp3jfACs7nNuyFE";
        String secret = "cPEmMtl6KI2c0AtGAOz9rrgFKfnxjE";
        String endpoint = "http://oss-cn-shanghai.aliyuncs.com";

        OSSClient client = new OSSClient(endpoint, key, secret);
        String phone = request.getSession().getAttribute(Constants.ONLINE_USER).toString();
        String fileName = phone + "-" + file.getOriginalFilename();
        File targetFile = new File(path, fileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        file.transferTo(targetFile);
        InputStream content = new FileInputStream(targetFile);
        ObjectMetadata meta = new ObjectMetadata();
        Long length = targetFile.length();
        meta.setContentLength(length);

        // 上传Object.
        PutObjectResult result = client.putObject(bucketName, fileName, content, meta);

        // 判断是否为文件
        if (targetFile.isFile()) { // 为文件时调用删除文件方法
            deleteFile(targetFile);
        }
        return "order/upload-success";
    }

    /**
     * 删除单个文件
     * 
     * @param sPath
     *            被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public boolean deleteFile(File file) {
        boolean flag = false;
        // File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }
}
