package com.ochukai.smallant.web.interceptors;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UrlPathHelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.web.vo.JsonResult;

public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static List<String> ignoreUrls = new ArrayList<>();

    static {
        ignoreUrls.add("/admin/login");
        ignoreUrls.add("/admin/logout");
    }

    private UrlPathHelper urlPathHelper = new UrlPathHelper();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String lookupPath = this.urlPathHelper.getLookupPathForRequest(request);
        System.out.println("pre handler: " + lookupPath);

        if (ignoreUrls.contains(lookupPath)) {
            return true;
        }

        HttpSession session = request.getSession();
        Object obj = session.getAttribute(Constants.ONLINE_ACCOUNT);
        if (obj != null) {
            return true;
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        JsonResult result = new JsonResult(false);
        result.setMessage("对不起，您不能访问~");
        result.setError("还没有登陆！");

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getWriter(), result);
        response.flushBuffer();
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        // modelAndView.getModel();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}
