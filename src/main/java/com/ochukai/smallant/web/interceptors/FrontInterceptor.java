package com.ochukai.smallant.web.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UrlPathHelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ochukai.smallant.common.Constants;
import com.ochukai.smallant.exception.ResourceNotFoundException;
import com.ochukai.smallant.web.vo.JsonResult;

public class FrontInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = Logger.getLogger(getClass());

    private UrlPathHelper urlPathHelper = new UrlPathHelper();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String lookupPath = this.urlPathHelper.getLookupPathForRequest(request);

        HttpSession session = request.getSession();
        Object phone = session.getAttribute(Constants.ONLINE_USER);

        if (logger.isDebugEnabled()) {
            logger.debug("curren: " + lookupPath + ", phone: " + phone);
        }

        if (phone != null) {
            return true;
        }

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Class<?> returnType = handlerMethod.getMethod().getReturnType();
        if (returnType == JsonResult.class) {
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");

            JsonResult result = new JsonResult(false);
            result.setMessage("对不起，您不能访问~");

            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getWriter(), result);
            response.flushBuffer();
            return false;
        }

        throw new ResourceNotFoundException("");
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        logger.debug("postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        logger.debug("afterCompletion");
    }

}
