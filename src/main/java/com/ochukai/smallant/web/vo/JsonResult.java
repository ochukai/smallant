package com.ochukai.smallant.web.vo;

public class JsonResult {

    private Boolean result;
    private String  message;
    private String  error;

    public JsonResult() {
        this.result = false;
    }

    public JsonResult(Boolean result) {
        super();
        this.result = result;
    }

    public JsonResult(Boolean result, String message) {
        super();
        this.result = result;
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
