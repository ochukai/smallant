package com.ochukai.smallant.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AdminUpdateModel {

    private int id;

    @NotNull
    @Size(min = 5, max = 16)
    private String loginName;

    @NotNull
    @Size(max = 16)
    private String name;

    @NotNull
    private int roleId;

    @Size(min = 6, max = 16)
    private String newPassword;

    @Size(min = 6, max = 16)
    private String reNewPassword;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReNewPassword() {
        return reNewPassword;
    }

    public void setReNewPassword(String reNewPassword) {
        this.reNewPassword = reNewPassword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
