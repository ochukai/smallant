package com.ochukai.smallant.web.vo;

import java.util.ArrayList;
import java.util.List;

public class ProductPropWithValues {

    private String                 name;
    private List<ProductPropValue> values = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProductPropWithValues other = (ProductPropWithValues) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public List<ProductPropValue> getValues() {
        return values;
    }

    public void setValues(List<ProductPropValue> values) {
        this.values = values;
    }

}
