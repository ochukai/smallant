package com.ochukai.smallant.web.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductPropVo {

    private String ids;
    private Double price;

    @JsonIgnore
    private String $$hashKey;
    private String title;

    private List<ProductPropAttr> attrs;

    public List<ProductPropAttr> getAttrs() {
        return attrs;
    }

    public Double getPrice() {
        return price;
    }

    public void setAttrs(List<ProductPropAttr> attrs) {
        this.attrs = attrs;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String get$$hashKey() {
        return $$hashKey;
    }

    public void set$$hashKey(String $$hashKey) {
        this.$$hashKey = $$hashKey;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "ProductProp [ids=" + ids + ", price=" + price + ", $$hashKey=" + $$hashKey + ", title=" + title
                + ", attrs=" + attrs + "]";
    }

}
