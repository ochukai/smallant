package com.ochukai.smallant.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AdminLoginModel {

    @NotNull
    @Size(min = 5, max = 16)
    private String loginName;

    @NotNull
    @Size(min = 6, max = 16)
    private String password;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
