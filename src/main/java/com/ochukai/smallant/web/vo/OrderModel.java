package com.ochukai.smallant.web.vo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ochukai.smallant.common.util.JsonConvertUtil;

public class OrderModel {

    private String name;
    private String pic;
    private double defaultPrice;
    private String description;
    private String props;

    private List<ProductPropVo> propsList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProps() {
        return props;
    }

    public void setProps(String props) {
        this.props = props;
    }

    public List<ProductPropVo> getPropsList() {
        if (propsList == null) {
            ObjectMapper mapper = new ObjectMapper();
            JavaType javaType = JsonConvertUtil.getCollectionType(mapper, ArrayList.class, ProductPropVo.class);
            try {
                propsList = mapper.readValue(this.props, javaType);
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return propsList;
    }

    public void setPropsList(List<ProductPropVo> propsList) {
        this.propsList = propsList;
    }

}
