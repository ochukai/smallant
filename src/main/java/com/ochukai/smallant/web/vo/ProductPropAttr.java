package com.ochukai.smallant.web.vo;

public class ProductPropAttr {

    private Integer id;
    private String  name;
    private String  value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ProductPropAttr [name=" + name + ", value=" + value + "]";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
