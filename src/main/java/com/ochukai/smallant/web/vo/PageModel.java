package com.ochukai.smallant.web.vo;

import java.util.List;

/**
 * 
 * $scope.products = page.data; $scope.pageModel = { page : page.page, pageSize
 * : page.pageSize, total : page.total };
 * 
 * @author xiaoyee
 *
 */
public class PageModel<T> {

    private List<T> data;

    private int  page;
    private int  pageSize;
    private long total;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

}
