package com.ochukai.smallant.web.jwt;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

public class JWT {
    private static Logger logger = Logger.getLogger(JWT.class);

    public final static String TOKEN_KEY_STRING = "this is a long and hard to guess token string of smallant.";

    private static SecretKey getKey() {
        SecureRandom random = null;
        try {
            random = new SecureRandom(TOKEN_KEY_STRING.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return MacProvider.generateKey(SignatureAlgorithm.HS512, random);
    }

    public static String sign(String subject) {
        return Jwts.builder().setSubject(subject).signWith(SignatureAlgorithm.HS512, getKey()).compact();
    }

    public static String parse(String token) {
        Jws<Claims> claims = null;
        String subject = null;

        try {
            claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token);
        } catch (Exception e) {
            logger.error("token parse error.");
            return null;
        }

        if (claims != null) {
            subject = claims.getBody().getSubject();
        }

        return subject;
    }

    public static void main(String[] args) {
        String token = sign("13188873073");
        System.out.println(token);
        String subject = parse(token + "sss");
        System.out.println(subject);
    }

}
