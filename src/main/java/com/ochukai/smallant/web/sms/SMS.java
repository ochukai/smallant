package com.ochukai.smallant.web.sms;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

/**
 * <pre>
 * String code = SMS.smsCode();
 * </pre>
 * 
 * // save code in session and send this message.
 * 
 * <pre>
 * SMS sms = new SMS("13188888888", code);
 * sms.send();
 * </pre>
 * 
 * @author Oliver
 *
 */
public class SMS {

    private Logger logger = Logger.getLogger(getClass());

    private static String PROPERTIES_PATH = "sms.properties";

    private String url = "http://web.duanxinwang.cc/asmx/smsservice.aspx?";

    private String name;
    private String pwd;
    private String sign;
    private String content;
    private String mobile;
    private String stime = "";
    private String type  = "pt";
    private String extno = "";

    public SMS(String mobile, String code) {
        init();
        this.mobile = mobile;
        this.content = String.format(this.content, code);

        if (logger.isDebugEnabled()) {
            logger.debug("sms content: " + content);
        }
    }

    private void init() {
        Properties prop = readPropertiesFile();
        this.name = prop.getProperty("name", "");
        this.pwd = prop.getProperty("password", "");
        this.sign = prop.getProperty("sign", "");
        this.content = prop.getProperty("content", "");
    }

    private Properties readPropertiesFile() {
        Properties properties = new Properties();

        try {
            // InputStream inputStream =
            // ClassLoader.getSystemResourceAsStream(PROPERTIES_PATH);
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_PATH);
            properties.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return properties;
    }

    public boolean send() {
        try {
            String urlString = this.toUrl();

            if (logger.isDebugEnabled()) {
                logger.debug("sms url: " + urlString);
            }

            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");

            InputStream is = url.openStream();

            // code, sendid, invalidcount, successcount, blackcount, msg
            // 0, 20130821110353234137876543,0,500,0,提交成功
            String returnStr = this.convertStreamToString(is);

            if (logger.isDebugEnabled()) {
                logger.debug("sms response: " + returnStr);
            }

            String[] results = StringUtils.split(returnStr, ",");
            return "0".equals(results[0]);

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    private String toUrl() throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();
        builder.append(url)//
                .append("name=").append(name)//
                .append("&pwd=").append(pwd)//
                .append("&mobile=").append(mobile)//
                .append("&content=").append(URLEncoder.encode(content, "UTF-8"))//
                .append("&stime=").append(stime)//
                .append("&sign=").append(URLEncoder.encode(sign, "UTF-8"))//
                .append("&type=").append(type)//
                .append("&extno=").append(extno);
        return builder.toString();
    }

    private String convertStreamToString(InputStream is) {
        StringBuilder sb1 = new StringBuilder();
        byte[] bytes = new byte[4096];
        int size = 0;

        try {
            while ((size = is.read(bytes)) > 0) {
                String str = new String(bytes, 0, size, "UTF-8");
                sb1.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb1.toString();
    }

    /**
     * 生成六位数字验证码
     * 
     * @return 六位数字验证码
     */
    public static String smsCode() {
        return createRandom(true, 6);
    }

    /**
     * 创建指定数量的随机字符串
     * 
     * @param numberFlag
     *            是否是数字
     * @param length
     * @return
     */
    private static String createRandom(boolean numberFlag, int length) {
        String retStr = "";
        String strTable = numberFlag ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
        int len = strTable.length();
        boolean bDone = true;
        do {
            retStr = "";
            int count = 0;
            for (int i = 0; i < length; i++) {
                double dblR = Math.random() * len;
                int intR = (int) Math.floor(dblR);
                char c = strTable.charAt(intR);
                if (('0' <= c) && (c <= '9')) {
                    count++;
                }
                retStr += strTable.charAt(intR);
            }
            if (count >= 2) {
                bDone = false;
            }
        } while (bDone);

        return retStr;
    }

    public static void main(String[] args) {
        String code = smsCode();
//        SMS sms = new SMS("18516559920", code);
         SMS sms = new SMS("13188873073", code);
        sms.send();
    }
}
