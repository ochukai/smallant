package com.ochukai.smallant.web.alipay;

public class AlipayRequestInfo {

	private String memo;
	private GoodsInfo goods_info;
	private String need_address;
	private String trade_type;
	private String notify_url;

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public GoodsInfo getGoods_info() {
		return goods_info;
	}

	public void setGoods_info(GoodsInfo goods_info) {
		this.goods_info = goods_info;
	}

	public String getNeed_address() {
		return need_address;
	}

	public void setNeed_address(String need_address) {
		this.need_address = need_address;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

}
