package com.ochukai.smallant.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ochukai.smallant.mappers.ProductMapper;
import com.ochukai.smallant.mappers.ProductPropMapper;
import com.ochukai.smallant.mappers.PropCombPriceMapper;
import com.ochukai.smallant.models.Product;
import com.ochukai.smallant.models.ProductProp;
import com.ochukai.smallant.models.PropCombPrice;
import com.ochukai.smallant.po.PoProduct;

@Service
@Transactional
public class ProductService extends CommonService<Product> {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductPropMapper productPropMapper;

    @Autowired
    private PropCombPriceMapper propCombPriceMapper;

    public List<Product> getProductList() {
        List<Product> Products = productMapper.select(new Product());
        return Products;
    }

    public Product getOrderById(int id) {
        Product p = new Product();
        p.setId(id);
        Product product = productMapper.selectOne(p);
        return product;
    }

    public PoProduct getPOProductById(int id) {

        PoProduct result = new PoProduct();

        Product pInfo = new Product();
        pInfo.setId(id);
        Product productInfo = productMapper.selectOne(pInfo);

        ProductProp pProp = new ProductProp();
        pProp.setProductId(id);
        List<ProductProp> productPropList = productPropMapper.select(pProp);

        result.setProductInfo(productInfo);
        result.setProductPropList(productPropList);

        return result;
    }

    /**
     * @param ids
     * @return
     */
    public double getPriceBySelectedProp(String ids) {
        String arrayids[] = ids.split(",");
        int array[] = new int[arrayids.length];

        for (int i = 0; i < arrayids.length; i++) {
            array[i] = Integer.parseInt(arrayids[i]);
        }

        Arrays.sort(array);
        String arrString = Arrays.toString(array);

        PropCombPrice propCombPrice = new PropCombPrice();
        propCombPrice.setPropIds(arrString);
        PropCombPrice pCP = propCombPriceMapper.selectOne(propCombPrice);

        return pCP.getPrice();
    }

    public int saveProp(ProductProp prop) {
        return productPropMapper.insert(prop);
    }

    public int savePropComb(PropCombPrice propComb) {
        return propCombPriceMapper.insert(propComb);
    }

    public int deletePropByProductId(Integer pid) {
        ProductProp pp = new ProductProp();
        pp.setProductId(pid);
        return productPropMapper.delete(pp);
    }

    public int deletePropCombByProductId(Integer pid) {
        PropCombPrice pcp = new PropCombPrice();
        pcp.setProductId(pid);
        return propCombPriceMapper.delete(pcp);
    }

    public Product selectById(Integer id) {
        return productMapper.selectById(id);
    }

    public List<Product> selectIndexProducts() {
        Product product = new Product();
        product.setMarkForDelete(false);
        product.setStatus(1);
        List<Product> products = productMapper.select(product);
        return products;
    }

    public void deleteProduct(int id) {
        Product p = productMapper.selectByPrimaryKey(id);
        p.setId(id);
        p.setMarkForDelete(true);
        p.setStatus(0);
        productMapper.updateByPrimaryKey(p);
    }

}
