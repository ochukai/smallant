package com.ochukai.smallant.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.mappers.AdminMapper;
import com.ochukai.smallant.mappers.OrderMapper;
import com.ochukai.smallant.mappers.OrderProgressMapper;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.models.OrderProgress;
import com.ochukai.smallant.po.PoOrder;
import com.ochukai.smallant.web.vo.OrderCriteria;

@Service
@Transactional
public class OrderService extends CommonService<Order> {

    @Autowired
    private OrderProgressMapper orderProgressMapper;
    @Autowired
    private AdminMapper         adminMapper;

    private OrderMapper getOrderMapper() {
        return (OrderMapper) this.mapper;
    }

    public List<Order> getOrderList() {
        Order o = new Order();
        o.setMarkForDelete(false);
        List<Order> orders = getOrderMapper().select(o);
        return orders;
    }

    public Order getOrderById(int id) {
        Order o = new Order();
        o.setId(id);
        o.setMarkForDelete(false);
        Order order = getOrderMapper().selectOne(o);
        return order;
    }

    public List<Order> getOrderByUserId(int userId) {
        Order o = new Order();
        o.setUserId(userId);
        o.setMarkForDelete(false);
        List<Order> orders = getOrderMapper().select(o);
        return orders;
    }

    public List<Order> getOrderByAdminId(int adminId) {
        Order o = new Order();
        o.setAdminId(adminId);
        o.setMarkForDelete(false);
        List<Order> orders = getOrderMapper().select(o);
        return orders;
    }

    public Order getOrderByOrderId(String orderId) {
        Order o = new Order();
        o.setOrderId(orderId);
        o.setMarkForDelete(false);
        Order order = getOrderMapper().selectOne(o);
        return order;
    }

    public void addOrder(Order order) {
        getOrderMapper().insert(order);
    }

    public void updateOrder(Order order) {
        getOrderMapper().updateByPrimaryKey(order);
    }

    public void deleteOrder(int id) {
        Order order = getOrderMapper().selectByPrimaryKey(id);
        order.setMarkForDelete(true);
        getOrderMapper().updateByPrimaryKey(order);
    }

    public List<OrderProgress> getOrderProgressByOrderId(int orderId) {
        OrderProgress op = new OrderProgress();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMM");// 设置日期格式
        Integer month = Integer.parseInt(df.format(new Date()));
        op.setMonth(month);
        op.setOrderId(orderId);
        return orderProgressMapper.select(op);
    }

    /**
     * @param id
     * @param month
     *            查询所有月份请输入0
     * @return
     */
    public PoOrder getPoOrderById(int id, int month) {
        PoOrder result = new PoOrder();

        Order o = new Order();
        o.setId(id);
        o.setMarkForDelete(false);
        Order order = getOrderMapper().selectOne(o);

        OrderProgress op = new OrderProgress();
        op.setOrderId(o.getId());
        op.setMarkForDelete(false);

        if (month != 0) {
            op.setMonth(month);
        }

        List<OrderProgress> orderProgressList = orderProgressMapper.select(op);

        result.setOrderInfo(order);
        result.setOrderProgressList(orderProgressList);

        return result;
    }

    public int countNewOrderAndUpdateLastPullTime(Integer id) {
        Admin admin = new Admin();
        admin.setId(id);
        admin = adminMapper.selectOne(admin);

        Date lastPull = admin.getLastPullTime();
        if (lastPull == null) {
            Calendar cal = Calendar.getInstance();
            cal.set(2015, 8, 16);
            lastPull = cal.getTime();
        }

        int count = getOrderMapper().countNewOrder(id, lastPull);

        admin.setLastPullTime(new Date());
        adminMapper.updateByPrimaryKey(admin);

        return count;
    }

    public PageInfo<Order> selectByPage(OrderCriteria criteria) {
        PageHelper.startPage(criteria.getPage(), criteria.getPageSize());
        
        List<Order> list = getOrderMapper().selectByPage(criteria);
        return new PageInfo<>(list);
    }
}
