package com.ochukai.smallant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import tk.mybatis.mapper.common.Mapper;

@Service
public abstract class CommonService<T> {

    @Autowired
    protected Mapper<T> mapper;
    
    public T selectOne(T entity) {
        return mapper.selectOne(entity);
    }
    
    public T selectById(Object key) {
        return mapper.selectByPrimaryKey(key);
    }

    public int save(T entity) {
        return mapper.insertSelective(entity);
    }

    /**
     * 调用时请将传入对象的 MarkForDelete 置为 true。
     * 
     * @param entity
     * @return
     */
    public int delete(T entity) {
        return mapper.updateByPrimaryKey(entity);
    }

    public int update(T entity) {
        return mapper.updateByPrimaryKey(entity);
    }

    /**
     * 单表分页查询 调用时请将 参数对象的MarkForDelete 置为 False；
     * 
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<T> selectPage(int pageNum, int pageSize, T entity) {
        PageHelper.startPage(pageNum, pageSize);
        List<T> list = mapper.select(entity);
        return new PageInfo<>(list);
    }
}
