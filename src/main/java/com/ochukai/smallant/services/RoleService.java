package com.ochukai.smallant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ochukai.smallant.mappers.RoleMapper;
import com.ochukai.smallant.models.Role;

@Service
@Transactional
public class RoleService extends CommonService<Role> {

    @Autowired
    private RoleMapper roleMapper;

    public List<Role> findAllRoles() {
        return roleMapper.findAllRoles();
    }

    public Role findByIdWithMenus(int id) {
        return roleMapper.findByIdWithMenus(id);
    }

}
