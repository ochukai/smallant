package com.ochukai.smallant.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.mappers.UserMapper;
import com.ochukai.smallant.models.Admin;
import com.ochukai.smallant.models.User;

@Service
@Transactional
public class UserService extends CommonService<User> {

    private UserMapper getUserMapper() {
        return (UserMapper) this.mapper;
    }

    public void deleteUser(int id) {
        User user = mapper.selectByPrimaryKey(id);
        user.setMarkForDelete(true);
        mapper.updateByPrimaryKey(user);
    }

    public List<User> findAllUsers() {
        User a = new User();
        a.setMarkForDelete(true);
        return mapper.select(a);
    }

    public String checkLogin(String phone, String password) {
        User a = new User();
        a.setMarkForDelete(false);
        a.setPhone(phone);
        User user = mapper.selectOne(a);
        String resultInfo = "";
        if (user == null) {
            resultInfo = "注册手机号不存在，请重新输入。";
        } else if (user.getPassword() == password) {
            resultInfo = "密码错误，请重新输入。";
        }

        return resultInfo;
    }

    public User selectByPhone(String phone) {
        User user = new User();
        user.setPhone(phone);
        return mapper.selectOne(user);
    }

    public User selectByToken(String token) {
        User user = new User();
        user.setToken(token);
        return mapper.selectOne(user);
    }

    public PageInfo<User> selectPage(int pageNum, int pageSize, String name, Integer adminId, Integer adminRole) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> list = getUserMapper().selectPage(name, adminId, adminRole);
        return new PageInfo<>(list);
    }

}