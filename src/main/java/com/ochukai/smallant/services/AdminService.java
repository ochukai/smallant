package com.ochukai.smallant.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ochukai.smallant.mappers.AdminMapper;
import com.ochukai.smallant.models.Admin;

@Service
@Transactional
public class AdminService extends CommonService<Admin> {

    @Autowired
    private AdminMapper adminMapper;

    public void deleteAdmin(int id) {
        Admin admin = adminMapper.selectByPrimaryKey(id);
        admin.setMarkForDelete(true);
        adminMapper.updateByPrimaryKey(admin);
    }

    public Admin findByLoginNameAndPassword(String loginName, String password) {
        Admin admin = new Admin();
        admin.setLoginName(loginName);
        admin.setPassword(password);
        admin.setMarkForDelete(false);
        return adminMapper.selectOne(admin);
    }

    public PageInfo<Admin> selectPage(int pageNum, int pageSize, String name) {
        PageHelper.startPage(pageNum, pageSize);
        List<Admin> list = adminMapper.selectPage(name);
        return new PageInfo<>(list);
    }

    public void updateLoginTime(Admin admin) {
        admin.setLastLoginTime(new Date());
        adminMapper.updateByPrimaryKey(admin);
    }

}
