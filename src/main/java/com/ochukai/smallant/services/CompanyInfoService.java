package com.ochukai.smallant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ochukai.smallant.mappers.CompanyInfoMapper;
import com.ochukai.smallant.models.CompanyInfo;

@Service
@Transactional
public class CompanyInfoService extends CommonService<CompanyInfo> {

	@Autowired
	private CompanyInfoMapper companyInfoMapper;

	public List<CompanyInfo> getCompanyInfoList(CompanyInfo c) {
		List<CompanyInfo> CompanyInfos = companyInfoMapper.select(c);
		return CompanyInfos;
	}

}
