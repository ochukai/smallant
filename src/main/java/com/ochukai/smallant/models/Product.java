package com.ochukai.smallant.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String pic;

    private String description;

    @Column(name = "default_price")
    private Double defaultPrice;

    private Integer status;

    private String features;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    @Transient
    private List<ProductProp> props;

    @Transient
    private List<PropCombPrice> prices;

    @Transient
    private String[] featureList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public List<ProductProp> getProps() {
        return props;
    }

    public void setProps(List<ProductProp> props) {
        this.props = props;
    }

    public List<PropCombPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<PropCombPrice> prices) {
        this.prices = prices;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
        if (this.features != null) {
            this.featureList = this.features.split(";");
        }
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", name=" + name + ", pic=" + pic + ", description=" + description
                + ", defaultPrice=" + defaultPrice + ", status=" + status + ", features=" + features + ", createDate="
                + createDate + ", markForDelete=" + markForDelete + ", updateDate=" + updateDate + ", props=" + props
                + ", prices=" + prices + "]";
    }

    public String[] getFeatureList() {
        return featureList;
    }

    public void setFeatureList(String[] featureList) {
        this.featureList = featureList;
    }

}