package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "work_flow")
public class WorkFlow {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "root_id")
    private Integer rootId;

    private String name;

    private String description;

    @Column(name = "expect_duration")
    private Integer expectDuration;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return root_id
     */
    public Integer getRootId() {
        return rootId;
    }

    /**
     * @param rootId
     */
    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return expect_duration
     */
    public Integer getExpectDuration() {
        return expectDuration;
    }

    /**
     * @param expectDuration
     */
    public void setExpectDuration(Integer expectDuration) {
        this.expectDuration = expectDuration;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return mark_for_delete
     */
    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}