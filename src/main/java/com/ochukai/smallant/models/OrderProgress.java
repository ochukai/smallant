package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "order_progress")
public class OrderProgress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "work_flow_id")
    private Integer workFlowId;

    /**
     * 0 未开始 
     * 1 正在进行 
     * 2 整理票据
     * 3 会计整理
     * 4 会计记账
     * 5 已完成
     */
    private Integer status;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "expect_end_date")
    private Date expectEndDate;

    private Integer month;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return order_id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * @return user_id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return work_flow_id
     */
    public Integer getWorkFlowId() {
        return workFlowId;
    }

    /**
     * @param workFlowId
     */
    public void setWorkFlowId(Integer workFlowId) {
        this.workFlowId = workFlowId;
    }

    /**
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return start_date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return expect_end_date
     */
    public Date getExpectEndDate() {
        return expectEndDate;
    }

    /**
     * @param expectEndDate
     */
    public void setExpectEndDate(Date expectEndDate) {
        this.expectEndDate = expectEndDate;
    }

    /**
     * @return month
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * @param month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return mark_for_delete
     */
    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}