package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "product_prop")
public class ProductProp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "prop_name")
    private String propName;

    @Column(name = "prop_value")
    private String propValue;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return product_id
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return prop_name
     */
    public String getPropName() {
        return propName;
    }

    /**
     * @param propName
     */
    public void setPropName(String propName) {
        this.propName = propName;
    }

    /**
     * @return prop_value
     */
    public String getPropValue() {
        return propValue;
    }

    /**
     * @param propValue
     */
    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return mark_for_delete
     */
    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "ProductProp [id=" + id + ", productId=" + productId + ", propName=" + propName + ", propValue="
                + propValue + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((productId == null) ? 0 : productId.hashCode());
        result = prime * result + ((propName == null) ? 0 : propName.hashCode());
        result = prime * result + ((propValue == null) ? 0 : propValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProductProp other = (ProductProp) obj;
        if (productId == null) {
            if (other.productId != null)
                return false;
        } else if (!productId.equals(other.productId))
            return false;
        if (propName == null) {
            if (other.propName != null)
                return false;
        } else if (!propName.equals(other.propName))
            return false;
        if (propValue == null) {
            if (other.propValue != null)
                return false;
        } else if (!propValue.equals(other.propValue))
            return false;
        return true;
    }

}