package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "admin")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "ltd_id")
    private Integer ltdId;

    private String password;

    @Column(name = "login_name")
    private String loginName;

    private String name;

    private String phone;

    @Column(name = "last_login_time")
    private Date lastLoginTime;

    @Column(name = "last_pull_time")
    private Date lastPullTime;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return login_name
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * @return ltd_id
     */
    public Integer getLtdId() {
        return ltdId;
    }

    /**
     * @return mark_for_delete
     */
    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return role_id
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param loginName
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * @param ltdId
     */
    public void setLtdId(Integer ltdId) {
        this.ltdId = ltdId;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param roleId
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getLastPullTime() {
        return lastPullTime;
    }

    public void setLastPullTime(Date lastPullTime) {
        this.lastPullTime = lastPullTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}