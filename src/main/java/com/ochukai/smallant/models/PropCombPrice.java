package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "prop_comb_price")
public class PropCombPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "prop_ids")
    private String propIds;

    private Double price;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return product_id
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return prop_ids
     */
    public String getPropIds() {
        return propIds;
    }

    /**
     * @param propIds
     */
    public void setPropIds(String propIds) {
        this.propIds = propIds;
    }

    /**
     * @return price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return mark_for_delete
     */
    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "PropCombPrice [id=" + id + ", productId=" + productId + ", propIds=" + propIds + ", price=" + price
                + "]";
    }
}