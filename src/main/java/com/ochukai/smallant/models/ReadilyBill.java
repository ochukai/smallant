package com.ochukai.smallant.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "readily_bill")
public class ReadilyBill {

    private Integer id;
    private String  pic;
    private String  comment;
    @Column(name = "upload_time")
    private Date    uploadTime;
    @Column(name = "mark_for_delete")
    private Boolean markForDelete;
    @Column(name = "order_progress_id")
    private Integer orderProgressId;

    @Transient
    private OrderProgress orderProgress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    public Integer getOrderProgressId() {
        return orderProgressId;
    }

    public void setOrderProgressId(Integer orderProgressId) {
        this.orderProgressId = orderProgressId;
    }

    public OrderProgress getOrderProgress() {
        return orderProgress;
    }

    public void setOrderProgress(OrderProgress orderProgress) {
        this.orderProgress = orderProgress;
    }

}
