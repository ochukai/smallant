package com.ochukai.smallant.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "company_info")
public class CompanyInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(name = "tax_code")
    private String taxCode;

    private String manager;

    private String phone;

    private String receiver;

    @Column(name = "receiver_phone")
    private String receiverPhone;

    @Column(name = "receiver_address")
    private String receiverAddress;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Boolean markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "user_id")
    private Integer userId;

    @Transient
    private User user;

    public Date getCreateDate() {
        return createDate;
    }

    public Integer getId() {
        return id;
    }

    public String getManager() {
        return manager;
    }

    public Boolean getMarkForDelete() {
        return markForDelete;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public User getUser() {
        return user;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public void setMarkForDelete(Boolean markForDelete) {
        this.markForDelete = markForDelete;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setReceiveAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
