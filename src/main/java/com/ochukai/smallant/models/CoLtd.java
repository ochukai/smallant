package com.ochukai.smallant.models;

import java.util.Date;
import javax.persistence.*;

@Table(name = "co_ltd")
public class CoLtd {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String decription;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mark_for_delete")
    private Byte markForDelete;

    @Column(name = "update_date")
    private Date updateDate;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return decription
     */
    public String getDecription() {
        return decription;
    }

    /**
     * @param decription
     */
    public void setDecription(String decription) {
        this.decription = decription;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return mark_for_delete
     */
    public Byte getMarkForDelete() {
        return markForDelete;
    }

    /**
     * @param markForDelete
     */
    public void setMarkForDelete(Byte markForDelete) {
        this.markForDelete = markForDelete;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}