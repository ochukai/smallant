package com.ochukai.smallant.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ErrorUtil {

    /**
     * 把错误信息存入model 有助于在前台显示
     * 
     * @param result
     * @param model
     */
    public static void putInModel(BindingResult result, Model model) {
        List<ObjectError> errors = result.getAllErrors();
        Map<String, String> ers = new HashMap<String, String>();
        for (ObjectError objectError : errors) {
            if (objectError instanceof FieldError) {
                FieldError fe = (FieldError) objectError;
                ers.put(fe.getField(), fe.getDefaultMessage());
            } else {
                ers.put(objectError.getCode(), objectError.getDefaultMessage());
            }
        }
        model.addAttribute("errors", ers);
    }

}
