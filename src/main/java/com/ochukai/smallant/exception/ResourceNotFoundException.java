package com.ochukai.smallant.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ResourceNotFoundException(String arg0) {
        super(arg0);
    }

    public ResourceNotFoundException(Throwable arg0) {
        super(arg0);
    }

}