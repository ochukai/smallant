package com.ochukai.smallant.exception;

public class UserCodeException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UserCodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserCodeException(String message) {
        super(message);
    }

    public UserCodeException(Throwable cause) {
        super(cause);
    }

}