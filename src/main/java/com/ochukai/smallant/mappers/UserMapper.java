package com.ochukai.smallant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ochukai.smallant.models.User;

import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

    List<User> selectPage(@Param("name") String name, @Param("adminId") Integer adminId,
            @Param("roleId") Integer adminRole);
}