package com.ochukai.smallant.mappers;

import java.util.List;

import com.ochukai.smallant.models.Menu;

import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<Menu> {

    public List<Menu> selectMenusByRoleId(int roleId);

}