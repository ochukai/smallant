package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.Product;
import tk.mybatis.mapper.common.Mapper;

public interface ProductMapper extends Mapper<Product> {

    Product selectById(Integer id);

}