package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.CoLtd;
import tk.mybatis.mapper.common.Mapper;

public interface CoLtdMapper extends Mapper<CoLtd> {
}