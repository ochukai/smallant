package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.OrderProgress;
import tk.mybatis.mapper.common.Mapper;

public interface OrderProgressMapper extends Mapper<OrderProgress> {
}