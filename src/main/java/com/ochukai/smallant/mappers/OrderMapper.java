package com.ochukai.smallant.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.web.vo.OrderCriteria;

import tk.mybatis.mapper.common.Mapper;

public interface OrderMapper extends Mapper<Order> {

    int countNewOrder(@Param(value = "id") int id, @Param(value = "lastPullTime") Date lastPullTime);

    List<Order> selectByPage(@Param(value = "criteria") OrderCriteria criteria);

}