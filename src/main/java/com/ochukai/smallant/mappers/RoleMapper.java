package com.ochukai.smallant.mappers;

import java.util.List;

import com.ochukai.smallant.models.Role;

import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {

    Role findByIdWithMenus(int id);
    
    List<Role> findAllRoles();

}