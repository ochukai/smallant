package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.PropCombPrice;
import tk.mybatis.mapper.common.Mapper;

public interface PropCombPriceMapper extends Mapper<PropCombPrice> {
}