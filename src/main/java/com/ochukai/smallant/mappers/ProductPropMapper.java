package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.ProductProp;
import tk.mybatis.mapper.common.Mapper;

public interface ProductPropMapper extends Mapper<ProductProp> {
}