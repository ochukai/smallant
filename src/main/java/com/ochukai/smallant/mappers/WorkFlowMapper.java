package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.WorkFlow;
import tk.mybatis.mapper.common.Mapper;

public interface WorkFlowMapper extends Mapper<WorkFlow> {
}