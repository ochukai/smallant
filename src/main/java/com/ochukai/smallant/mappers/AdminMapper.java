package com.ochukai.smallant.mappers;

import java.util.List;

import com.ochukai.smallant.models.Admin;
import tk.mybatis.mapper.common.Mapper;

public interface AdminMapper extends Mapper<Admin> {
    
    void updateOnlineStatus(Admin admin);
    
    List<Admin> selectPage(String name);
    
}