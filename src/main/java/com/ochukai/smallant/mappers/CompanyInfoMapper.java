package com.ochukai.smallant.mappers;

import com.ochukai.smallant.models.CompanyInfo;

import tk.mybatis.mapper.common.Mapper;

public interface CompanyInfoMapper extends Mapper<CompanyInfo> {

}