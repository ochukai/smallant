package com.ochukai.smallant.common;

public class Constants {

    /**
     * 当前在线用户
     */
    public final static String ONLINE_ACCOUNT = "onlineAccount";

    public final static String ONLINE_USER = "onlineUser";

    public final static String ONLINE_CODE = "currentUserCode";

    public static final String SELLER_EMAIL = null;

}
