package com.ochukai.smallant.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author HuifengWang 静态化方法
 **/
public class FreeMarkerUtil {
    /**
     * 
     * 生成HTML静态页面的公公方法
     * 
     * @param fmc
     * @param templateName
     *            模板的名称
     * @param request
     * @param map
     *            生成模板需要的数据
     * @param filePath
     *            相对于web容器的路径
     * @param fileName
     *            要生成的文件的名称，带扩展名
     * @author HuifengWang
     * 
     */
    public static void createHtml(FreeMarkerConfig fmc, String templateName, HttpServletRequest request, Map<?, ?> map,
            String filePath, String fileName) {
        Writer out = null;
        try {
            Template template = fmc.getConfiguration().getTemplate(templateName);
            String htmlPath = request.getSession().getServletContext().getRealPath(filePath) + "/" + fileName;
            File htmlFile = new File(htmlPath);
            if (!htmlFile.getParentFile().exists()) {
                htmlFile.getParentFile().mkdirs();
            }
            if (!htmlFile.exists()) {
                htmlFile.createNewFile();
            }
            out = new OutputStreamWriter(new FileOutputStream(htmlPath), "UTF-8");
            template.process(map, out);
            out.flush();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
                out = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param request
     * @param filePath
     *            文件存放的路径
     * @param fileName
     *            文件的名称，需要扩展名
     * @author HuifengWang
     * @return
     */
    public static Map<String, Object> htmlFileHasExist(HttpServletRequest request, String filePath, String fileName) {
        Map<String, Object> map = new HashMap<String, Object>();
        String htmlPath = request.getSession().getServletContext().getRealPath(filePath) + "/" + fileName;
        File htmlFile = new File(htmlPath);
        if (htmlFile.exists()) {
            map.put("exist", true);
        } else {
            map.put("exist", false);
        }
        return map;
    }

//    @Autowired
//    private FreeMarkerConfig freeMarkerConfig;// 获取FreemarkerConfig的实例
//
//    @RequestMapping("/ttt")
//    public String ttt(HttpServletRequest request, HttpServletResponse response, ModelMap mv)
//            throws IOException, TemplateException, ServletException {
//        String fileName = "ttt.html";
//        Boolean flag = (Boolean) FreeMarkerUtil.htmlFileHasExist(request, FREEMARKER_PATH, fileName).get("exist");
//        if (!flag) {// 如何静态文件不存在，重新生成
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("user", "xiaowang小王");// 这里包含业务逻辑请求等
//            mv.addAllAttributes(map);
//            FreeMarkerUtil.createHtml(freeMarkerConfig, "demo.ftl", request, map, FREEMARKER_PATH, fileName);// 根据模板生成静态页面
//        }
//        return FREEMARKER_PATH + "/" + fileName;// 始终返回生成的HTML页面
//    }

}