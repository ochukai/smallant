package com.ochukai.smallant.common.util;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class UploadPathUtil {

    /**
     * 得到请求的根目录
     * 
     * @param request
     * @return
     */
    public static String getBasePath(HttpServletRequest request) {
        String path = request.getContextPath();
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
    }

    /**
     * 获取文件扩展名
     * 
     * @return string etc .jpg
     */
    public static String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    /**
     * 得到请求的根目录
     * 
     * @param request
     * @return
     */
    public static String getRealPath(HttpServletRequest request, String suffix) {
        return getRealRootPath(request) + suffix;
    }

    /**
     * 得到请求的根目录
     * 
     * @param request
     * @return
     */
    public static String getRealRootPath(HttpServletRequest request) {
        return request.getSession().getServletContext().getRealPath("/");
    }

    /**
     * 生成文件保存的路径
     * 
     * @param request
     * @param dir
     * @param originalFilename
     * @return 保存文件的位置, 会保存到数据库中.
     */
    public static File getToBeSaveFile(HttpServletRequest request, String dir, String fileName) {
        Date d = new Date();
        StringBuffer path = new StringBuffer(getRealPath(request, ENCLOSURE_BASE_PATH)).append(File.separator);

        if (dir.equals("image")) {
            path.append("images");
        } else if (dir.equals("file")) {
            path.append("files");
        }

        String timeDir = TimeFormat.Y_M_D.format(d);
        path.append(File.separator).append(timeDir);

        String timeFileName = TimeFormat.YMDHMSZ.format(d) + getFileExtension(fileName);

        File saveFile = new File(path.toString(), timeFileName);
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }

        return saveFile;
    }

    /**
     * 
     * 生成文件保存的路径
     * 
     * 
     * 
     * @param request
     * 
     * @param dir
     * 
     * @param originalFilename
     * 
     * @return 保存文件的位置, 会保存到数据库中.
     * 
     */
    public static String getSavePath(HttpServletRequest request, String dir, String fileName) {
        Date d = new Date();
        StringBuffer path = new StringBuffer(getRealPath(request, ENCLOSURE_BASE_PATH)).append(File.separator);

        if (dir.equals("image")) {
            path.append("images");
        } else if (dir.equals("file")) {
            path.append("files");
        }

        String timeDir = TimeFormat.Y_M_D.format(d);
        path.append(File.separator).append(timeDir);

        File saveFile = new File(path.toString());
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }

        return path.toString();
    }

    public static String getUrlPath(String dir, String fileName) {
        StringBuffer path = new StringBuffer("/uploads/");

        if (dir.equals("image")) {
            path.append("images");
        } else if (dir.equals("file")) {
            path.append("files");
        }

        String timeDir = TimeFormat.Y_M_D.format(new Date());
        path.append("/").append(timeDir);
        return path.toString();
    }

    public static String getTimeFileName(String fileName) {
        return TimeFormat.YMDHMSZ.format(new Date()) + getFileExtension(fileName);
    }

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(getFileExtension("123.jpg"));
    }

    /**
     * 所有的附件都保存在这个文件夹下面.
     * 
     * base_path/dir/date/file.***
     * 
     */
    private final static String ENCLOSURE_BASE_PATH = "uploads";
}
