package com.ochukai.smallant.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashKit {
	private static final char[] LETTERS = "0123456789abcdef".toCharArray();

	/**
	 * md5
	 * 
	 * @param value
	 * @return
	 */
	public static String md5(String value) {
		try {
			return toHexString(MessageDigest.getInstance("md5").digest(value.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * sha1
	 * 
	 * @param value
	 * @return
	 */
	public static String sha1(String value) {
		try {
			return toHexString(MessageDigest.getInstance("SHA1").digest(value.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String toHexString(byte[] bytes) {
		char[] values = new char[bytes.length * 2];
		int i = 0;
		for (byte b : bytes) {
			values[i++] = LETTERS[((b & 0xF0) >>> 4)];
			values[i++] = LETTERS[b & 0xF];
		}
		return String.valueOf(values);
	}
}
