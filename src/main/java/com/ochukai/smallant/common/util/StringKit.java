package com.ochukai.smallant.common.util;

import java.util.List;

public class StringKit {

    public static <T> String listToString(List<T> list) {
        if (list == null) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        boolean flag = false;
        for (T t : list) {
            if (flag) {
                result.append(",");
            } else {
                flag = true;
            }
            result.append(t);
        }
        return result.toString();
    }

}
