package com.ochukai.smallant.po;

import java.util.List;

import com.ochukai.smallant.models.Product;
import com.ochukai.smallant.models.ProductProp;

public class PoProduct {

    private Product productInfo;

    private List<ProductProp> productPropList;

    public List<ProductProp> getProductPropList() {
        return productPropList;
    }

    public void setProductPropList(List<ProductProp> productPropList) {
        this.productPropList = productPropList;
    }

    public Product getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(Product productInfo) {
        this.productInfo = productInfo;
    }

}
