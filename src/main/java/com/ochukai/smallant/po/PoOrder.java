package com.ochukai.smallant.po;

import java.util.List;

import com.ochukai.smallant.models.Order;
import com.ochukai.smallant.models.OrderProgress;

public class PoOrder {
	private Order OrderInfo;
	private List<OrderProgress> OrderProgressList;

	public List<OrderProgress> getOrderProgressList() {
		return OrderProgressList;
	}

	public void setOrderProgressList(List<OrderProgress> orderProgressList) {
		OrderProgressList = orderProgressList;
	}

	public Order getOrderInfo() {
		return OrderInfo;
	}

	public void setOrderInfo(Order orderInfo) {
		OrderInfo = orderInfo;
	}

}
