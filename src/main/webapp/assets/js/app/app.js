'use strict';

// in order to make $routeProvider works well, it's necessary to load ngRoute plugin.
var app = angular.module('myApp', 
  [
    'ngAnimate', 
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    'angular-loading-bar',
    'angularFileUpload',
    'ngKeditor'
  ]);

// config the $routeProvider.
app.config([
  '$routeProvider', 
  '$httpProvider', 
  '$locationProvider',
  function ($routeProvider, $httpProvider, $locationProvider) {

    $routeProvider
      .when('/admin', { 
        templateUrl: '/assets/tpl/admin/me.html',
        controller: 'MainController'
      })
    
      // Login, Logout, Register
      .when('/admin/login', {
        templateUrl: '/assets/tpl/admin/login.html',
        controller: 'LoginController'
      })
      .when('/admin/logout', {
        templateUrl: '/assets/tpl/admin/logout.html',
        controller: 'LoginController'
      }) 
      
      // admin
      .when("/admin/admin", {
        templateUrl: "/assets/tpl/admin/admin/list.html",
        controller: "AdminIndexController"
      })
      .when("/admin/admin/new", {
        templateUrl: "/assets/tpl/admin/admin/new.html",
        controller: "AdminEditController"
      })
      .when("/admin/admin/:id", {
        templateUrl: "/assets/tpl/admin/admin/show.html",
        controller: "AdminShowController"
      })
      .when("/admin/admin/:id/edit", {
        templateUrl: "/assets/tpl/admin/admin/edit.html",
        controller: "AdminEditController"
      })
      
      // product
      .when("/admin/product", {
        templateUrl: "/assets/tpl/admin/product/list.html",
        controller: "ProductIndexController"
      })
      .when("/admin/product/new", {
        templateUrl: "/assets/tpl/admin/product/new.html",
        controller: "ProductEditController"
      })
      .when("/admin/product/:id", {
        templateUrl: "/assets/tpl/admin/product/show.html",
        controller: "ProductShowController"
      })
      .when("/admin/product/:id/edit", {
        templateUrl: "/assets/tpl/admin/product/edit.html",
        controller: "ProductEditController"
      })
      
      // order
      .when("/admin/new-order", {
        templateUrl: "/assets/tpl/admin/order/new-list.html",
        controller: "NewOrderIndexController"
      })
      .when("/admin/order", {
        templateUrl: "/assets/tpl/admin/order/list.html",
        controller: "OrderIndexController"
      })
//      .when("/admin/order/new", {
//        templateUrl: "/assets/tpl/admin/order/edit.html",
//        controller: "OrderEditController"
//      })
      .when("/admin/order/:id", {
        templateUrl: "/assets/tpl/admin/order/show.html",
        controller: "OrderShowController"
      })
      .when("/admin/order/:id/edit", {
        templateUrl: "/assets/tpl/admin/product/edit.html",
        controller: "OrderEditController"
      })
      
       // user
      .when("/admin/user", {
        templateUrl: "/assets/tpl/admin/user/list.html",
        controller: "UserIndexController"
      })
      .when("/admin/user/new", {
        templateUrl: "/assets/tpl/admin/user/new.html",
        controller: "UserEditController"
      })
      .when("/admin/user/:id", {
        templateUrl: "/assets/tpl/admin/user/show.html",
        controller: "UserShowController"
      })
      .when("/admin/user/:id/edit", {
        templateUrl: "/assets/tpl/admin/user/edit.html",
        controller: "UserEditController"
      })
      
      // company-info
      .when("/admin/company-info", {
        templateUrl: "/assets/tpl/admin/company-info/list.html",
        controller: "CompanyInfoIndexController"
      })
//      .when("/admin/company-info/new", {
//        templateUrl: "/assets/tpl/admin/company-info/new.html",
//        controller: "CompanyInfoEditController"
//      })
      .when("/admin/company-info/:id", {
        templateUrl: "/assets/tpl/admin/company-info/show.html",
        controller: "CompanyInfoShowController"
      })
      .when("/admin/company-info/:id/edit", {
        templateUrl: "/assets/tpl/admin/company-info/edit.html",
        controller: "CompanyInfoEditController"
      })
      
       // readily
      .when("/admin/company-info", {
        templateUrl: "/assets/tpl/admin/company-info/list.html",
        controller: "CompanyInfoIndexController"
      })
//      .when("/admin/company-info/new", {
//        templateUrl: "/assets/tpl/admin/company-info/new.html",
//        controller: "CompanyInfoEditController"
//      })
      .when("/admin/company-info/:id", {
        templateUrl: "/assets/tpl/admin/company-info/show.html",
        controller: "CompanyInfoShowController"
      })
      .when("/admin/company-info/:id/edit", {
        templateUrl: "/assets/tpl/admin/company-info/edit.html",
        controller: "CompanyInfoEditController"
      })
      
      // else
      .otherwise({
        redirectTo: '/admin'
      });
    
    $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";
    $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
      if (typeof data === "string") {
        return data;
      }

      var key, result = [];
      for (key in data) {
        if (data.hasOwnProperty(key)) {
          result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
        }
      }

      return result.join("&");
    });

  }]);