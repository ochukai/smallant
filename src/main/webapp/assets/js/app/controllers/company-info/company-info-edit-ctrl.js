app.controller('CompanyInfoEditController', 
    [ '$scope', '$routeParams', '$location', 'CompanyInfo', 'AlertService',
    function($scope, $routeParams, $location, CompanyInfo, alertService) {

      $scope.isUpdate = ($routeParams.id) ? true : false;

      var result = CompanyInfo.allRoles(function() {
        $scope.roles = result.roles;
      });

      $scope.admin = $scope.isUpdate ? CompanyInfo.show({ id : $routeParams.id }) : new CompanyInfo();

      $scope.submit = function() {
        console.log('submit');

        var success = function(res) {
              if (res.result) {
                alertService.addSuccess('ok.');
                console.log('uc success');
      
                $location.path('/admin/admin');
                return;
              }
              
              // error happened.

            },
            
            failure = function() {
              alertService.addDanger('error.');
              console.log('uc failure');
            };

        if ($scope.isUpdate) {
          CompanyInfo.update({ id : $scope.admin.id }, $scope.admin, success, failure);
        } else {
          CompanyInfo.create($scope.admin, success, failure);
        }
      };

    }
]);