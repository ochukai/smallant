app.controller('AdminEditController', 
    [ '$scope', '$routeParams', '$location', 'Admin', 'AlertService',
    function($scope, $routeParams, $location, Admin, alertService) {

      $scope.isUpdate = ($routeParams.id) ? true : false;

      var result = Admin.allRoles(function() {
        $scope.roles = result.roles;
      });

      $scope.admin = $scope.isUpdate ? Admin.show({ id : $routeParams.id }) : new Admin();

      $scope.submit = function() {
        console.log('submit');

        var success = function(res) {
              if (res.result) {
                alertService.addSuccess('ok.');
                console.log('uc success');
      
                $location.path('/admin/admin');
                return;
              }
              
              // error happened.

            },
            
            failure = function() {
              alertService.addDanger('error.');
              console.log('uc failure');
            };

        if ($scope.isUpdate) {
          Admin.update({ id : $scope.admin.id }, $scope.admin, success, failure);
        } else {
          Admin.create($scope.admin, success, failure);
        }
      };

    }
]);