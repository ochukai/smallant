app.controller('AdminIndexController',
  ['$scope', 'Admin', '$modal', 'AlertService', '$location',
  function ($scope, Admin, $modal, alertService, $location) {

    $scope.queryByCondition = function () {
      $scope.toPage(1);
    };

    $scope.toPage = function (pageNumber) {

      var pageRequest = {
        page     : pageNumber,
        pageSize : 8,
        name     : $scope.queryAdminName || ''
      };

      console.log('toPage(pageRequest): ', pageRequest);

      var page = Admin.query(pageRequest, function () {
        console.log('query', page);
        
        $scope.admins  = page.data;
        console.log('admins', $scope.admins);
        
        $scope.pageModel = {
          page     : page.page,
          pageSize : page.pageSize,
          total    : page.total
        };
        
        console.log('query finish.', $scope.pageModel);
      });
    };

    // show page one by default.
    $scope.toPage(1);

    function remove(id) {
      var success = function () {
            // give some info
            alertService.addSuccess('删除了：' + id);
            console.log('ok(delete): ' + id);
  
            // refresh list.
            var currentPage = $scope.pageModel.page;
            $scope.toPage(currentPage);
          },
  
          // error happens during the remove process.
          failure = function() {
            alertService.addDanger('删除的时候出了一些问题!');
          };

      Admin.destroy({id : id}, success, failure);
    }
    
    $scope.add = function() {
      console.log('add');
      $location.path('/admin/admin/new');
    };
    
    $scope.edit = function (id) {
      console.log('edit.');
      $location.path('/admin/admin/' + id + '/edit');
    };

    $scope.remove = function (id) {
      $scope.modalInstance = $modal.open({
        templateUrl: 'myModalContent.html',
        controller: 'AdminIndexController',
        scope: $scope
      });

      $scope
        .modalInstance
        .result
        .then(
          function () {
            remove(id);
          },
          function () {
            alertService.addInfo('什么都没有发生。');
            console.log('cancel');
          }
        );
    };

    $scope.ok = function () {
      $scope.modalInstance.close('delete');
    };

    $scope.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
    };

  }]);