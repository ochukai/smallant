app.controller('MainController', 
  ['$rootScope', '$scope', '$location', 'LoginService',
  function ($rootScope, $scope, $location, loginService) {

    if ($rootScope.menus) {
      return;
    }
    
    loginService
    .myMenus()
    .success(function (res) {
      
      if (!res.result) {
        $location.path('/admin/login');
        return;
      } else {
        var menus = [];
        if (res.menus) {
          menus = res.menus;
        }

        menus.push({ "name": "注销", "url": "/logout" });
        $rootScope.menus = menus;
      }
      
    });


  }]);