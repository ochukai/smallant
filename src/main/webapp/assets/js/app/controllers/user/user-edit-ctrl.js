app.controller('UserEditController', 
    [ '$scope', '$routeParams', '$location', 'User', 'AlertService',
    function($scope, $routeParams, $location, User, alertService) {

      $scope.isUpdate = ($routeParams.id) ? true : false;

      $scope.user = $scope.isUpdate ? User.show({ id : $routeParams.id }) : new User();

      $scope.submit = function() {
        console.log('submit');

        var success = function(res) {
              if (res.result) {
                alertService.addSuccess('ok.');
                console.log('uc success');
      
                $location.path('/admin/user');
                return;
              }
              
              // error happened.

            },
            
            failure = function() {
              alertService.addDanger('error.');
              console.log('uc failure');
            };

        if ($scope.isUpdate) {
          User.update({ id : $scope.user.id }, $scope.user, success, failure);
        } else {
          User.create($scope.user, success, failure);
        }
      };

    }
]);