app.controller('ReadilyEditController', 
    [ '$scope', '$routeParams', '$location', 'Readily', 'AlertService',
    function($scope, $routeParams, $location, Readily, alertService) {

      $scope.isUpdate = ($routeParams.id) ? true : false;

      var result = Readily.allRoles(function() {
        $scope.roles = result.roles;
      });

      $scope.admin = $scope.isUpdate ? Readily.show({ id : $routeParams.id }) : new Readily();

      $scope.submit = function() {
        console.log('submit');

        var success = function(res) {
              if (res.result) {
                alertService.addSuccess('ok.');
                console.log('uc success');
      
                $location.path('/admin/admin');
                return;
              }
              
              // error happened.

            },
            
            failure = function() {
              alertService.addDanger('error.');
              console.log('uc failure');
            };

        if ($scope.isUpdate) {
          Readily.update({ id : $scope.admin.id }, $scope.admin, success, failure);
        } else {
          Readily.create($scope.admin, success, failure);
        }
      };

    }
]);