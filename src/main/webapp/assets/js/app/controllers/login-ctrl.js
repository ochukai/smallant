app.controller('LoginController', 
  ['$rootScope', '$scope', '$location', 'LoginService', 'AlertService',
  function ($rootScope, $scope, $location, loginService, alertService) {

    $scope.login = function () {
      var formData = {
        loginName: $scope.username,
        password: $scope.password
      };

      loginService
        .login(formData)
        .success(function (res) {
          if (res.result) {
            // redirect to /me.
            $location.path('/admin');
            alertService.addSuccess('登陆成功');
            return;
          }
          
          // login error
          alertService.addDanger('用户名或密码错误');
        });
    };
    
    $scope.logout = function () {
        
      delete $rootScope.menus;
      
      loginService
        .logout()
        .success(function(res) {
          $location.path('/admin');
          alertService.addSuccess('已注销');
        });
    };
    
}]);