app.controller('OrderIndexController', 
    [ '$scope', 'Order', '$modal', 'AlertService', '$location',
    function($scope, Order, $modal, alertService, $location) {
        
      $scope.queryStartTime = null;
      $scope.queryEndTime = null;
      
      $scope.datePickerStart = { opened: false };
      $scope.datePickerEnd = { opened: false };
      
      $scope.openCalendar = function($event, type) {
        $event.preventDefault();
        $event.stopPropagation();
        
        if (type === 0) {
            $scope.datePickerStart.opened = !$scope.datePickerStart.opened;
        } else {
            $scope.datePickerEnd.opened = !$scope.datePickerEnd.opened;
        }
      };

      $scope.clear = function () {
        $scope.queryStartTime = null;
        $scope.queryEndTime = null;
      };

      // Disable weekend selection
      $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
      };

      $scope.dateOptions = {
          formatDay: 'dd',
          formatMonth: 'MM',
          formatYear: 'yyyy',
          formatDayHeader: 'EEE',
          formatDayTitle: 'MMMM yyyy',
          formatMonthTitle: 'yyyy',
          datepickerMode: 'day',
          minMode: 'day',
          maxMode: 'year',
          showWeeks: true,
          startingDay: 1,
          yearRange: 20,
          minDate: null,
          maxDate: null
      };

      $scope.format = 'yyyy-MM-dd';

//////////////////////////////
   // 对Date的扩展，将 Date 转化为指定格式的String 
   // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
   // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
   // 例子： 
   // (new Date()).format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
   // (new Date()).format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
   Date.prototype.format = function(fmt) {
     var o = { 
       "M+" : this.getMonth()+1,                 //月份 
       "d+" : this.getDate(),                    //日 
       "h+" : this.getHours(),                   //小时 
       "m+" : this.getMinutes(),                 //分 
       "s+" : this.getSeconds(),                 //秒 
       "q+" : Math.floor((this.getMonth()+3)/3), //季度 
       "S"  : this.getMilliseconds()             //毫秒 
     }; 
     if(/(y+)/.test(fmt)) 
       fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
     for(var k in o) 
       if(new RegExp("("+ k +")").test(fmt)) 
     fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
     return fmt; 
   }

      $scope.queryByCondition = function() {
        if ($scope.queryStartTime
                && $scope.queryEndTime
                && $scope.queryStartTime > $scope.queryEndTime) {
          alertService.addDanger('结束时间要大于开始时间');
          $scope.queryEndTime = null;
        }
        
        $scope.toPage(1);
      };

      $scope.toPage = function(pageNumber) {

        var pageRequest = {
          page : pageNumber,
          pageSize : 6,
          name : $scope.queryName,
          startDateStr: $scope.queryStartTime ? $scope.queryStartTime.format('yyyy-MM-dd 00:00:00') : null,
          endDateStr: $scope.queryEndTime ? $scope.queryEndTime.format('yyyy-MM-dd 23:59:59') : null
        };

        console.log('toPage(pageRequest): ', pageRequest);

        var page = Order.query(pageRequest, function() {
          console.log('query', page);

          $scope.orders = page.data;
          console.log('orders', $scope.orders);

          $scope.pageModel = {
            page : page.page,
            pageSize : page.pageSize,
            total : page.total
          };

          console.log('query finish.', $scope.pageModel);
        });
      };

      // show page one by default.
      $scope.toPage(1);

      function remove(id) {
        var success = function() {
          // give some info
          alertService.addDanger('删除了：' + id);
          console.log('ok(delete): ' + id);

          // refresh list.
          var currentPage = $scope.pageModel.page;
          $scope.toPage(currentPage);
        },

        // error happens during the remove process.
        failure = function() {
          alertService.addDanger('删除的时候出了一些问题!');
        };

        Order.destroy({
          id : id
        }, success, failure);
      }

      $scope.add = function() {
        console.log('add');
        $location.path('/admin/order/new');
      };

      $scope.edit = function(id) {
        console.log('edit.');
        $location.path('/admin/order/' + id + '/edit');
      };

      $scope.remove = function(id) {
        $scope.modalInstance = $modal.open({
          templateUrl : 'myModalContent.html',
          controller : 'ProductIndexController',
          scope : $scope
        });

        $scope.modalInstance.result.then(function() {
          remove(id);
        }, function() {
          alertService.addInfo('什么都没有发生。');
          console.log('cancel');
        });
      };

      $scope.ok = function() {
        $scope.modalInstance.close('delete');
      };

      $scope.cancel = function() {
        $scope.modalInstance.dismiss('cancel');
      };

    } ]);