app.controller('ProductShowController', ['$scope', 'Product', '$routeParams',
    function ($scope, Product, $routeParams) {
  
      $scope.product = Product.show({ id : $routeParams.id });
      
    }]);