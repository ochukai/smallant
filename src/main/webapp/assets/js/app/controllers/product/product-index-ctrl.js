app.controller('ProductIndexController', 
    [ '$scope', 'Product', '$modal', 'AlertService', '$location',
    function($scope, Product, $modal, alertService, $location) {

      $scope.queryByCondition = function() {
        $scope.toPage(1);
      };

      $scope.toPage = function(pageNumber) {

        var pageRequest = {
          page : pageNumber,
          pageSize : 6,
          name : $scope.queryName
        };

        console.log('toPage(pageRequest): ', pageRequest);

        var page = Product.query(pageRequest, function() {
          console.log('query', page);

          $scope.products = page.data;
          console.log('products', $scope.products);

          $scope.pageModel = {
            page : page.page,
            pageSize : page.pageSize,
            total : page.total
          };

          console.log('query finish.', $scope.pageModel);
        });
      };

      // show page one by default.
      $scope.toPage(1);

      function remove(id) {
        var success = function() {
          // give some info
          alertService.addDanger('删除了：' + id);
          console.log('ok(delete): ' + id);

          // refresh list.
          var currentPage = $scope.pageModel.page;
          $scope.toPage(currentPage);
        },

        // error happens during the remove process.
        failure = function() {
          alertService.addDanger('删除的时候出了一些问题!');
        };

        Product.destroy({
          id : id
        }, success, failure);
      }

      $scope.add = function() {
        console.log('add');
        $location.path('/admin/product/new');
      };

      $scope.edit = function(id) {
        console.log('edit.');
        $location.path('/admin/product/' + id + '/edit');
      };
      
      $scope.show = function(id) {
        console.log('show.');
        $location.path('/admin/product/' + id);
      };

      $scope.remove = function(id) {
        $scope.modalInstance = $modal.open({
          templateUrl : 'myModalContent.html',
          controller : 'ProductIndexController',
          scope : $scope
        });

        $scope.modalInstance.result.then(function() {
          remove(id);
        }, function() {
          alertService.addInfo('什么都没有发生。');
          console.log('cancel');
        });
      };

      $scope.ok = function() {
        $scope.modalInstance.close('delete');
      };

      $scope.cancel = function() {
        $scope.modalInstance.dismiss('cancel');
      };

    } ]);