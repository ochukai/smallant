app.controller('ProductEditController',
    [ '$scope', 'FileUploader', '$routeParams', '$location', 'Product', 'AlertService', 'oTransformer',
    function($scope, FileUploader, $routeParams, $location, Product, alertService, oTransformer) {
      console.log("$routeParams.id:", $routeParams.id);

      $scope.isUpdate = ($routeParams.id) ? true : false;

      // [ { value, index} ]
      $scope.features = [];
      
      // [ { name, values } ]
      $scope.attrs = [];
      
      // [ ids, title, attrs(name, value), price ]
      // $scope.prices = oTransformer.process($scope.attrs, $scope.priceMappings);
      $scope.prices = [];
      
      // console.log('$scope.prices:', $scope.prices);
      
      $scope.product = !$scope.isUpdate ? 
          new Product() :
          Product.show({ id : $routeParams.id }, function() {
            var ggIndex = 999;
            
            var features = ($scope.product.features || '').split(';') || [];
            console.log('featues in show:', features );
            $scope.features = features.map(function(item) {
              return { value: item, index: ggIndex++ };
            });
            
            var attrs = [];
            
            // [ { id, productId, propName, propPrice } ]
            var props = $scope.product.props || [];
            props.forEach(function(prop) {
              var id = prop.id
              var pid = prop.productId;
              var name = prop.propName;
              var value = prop.propValue;
              
              if (attrs.some(function(attr) { return attr.name === name; })) {
                var attr = attrs.filter(function(attr) { return attr.name === name; })[0];
                attr.values.push({ index: ggIndex++, id: id, value: value });
              } else {
                var attr = { index: ggIndex++, pid: pid, name: name, values: [ { id: id, value: value } ] };
                attrs.push(attr);
              } 
            });

            // [ {id, productId, propIds, price } ]
            var prices = $scope.product.prices || [];
            $scope.priceMappings = prices.map(function(item, index) {
              return { ids: item.propIds, price: item.price };
            });
            
            $scope.attrs = attrs;
            
            // [ ids, title, attrs(name, value), price ]
            $scope.prices = oTransformer.process($scope.attrs, $scope.priceMappings);
            
          });

      $scope.$watch("attrs", function( newValue, oldValue ) {
        $scope.prices = oTransformer.process(newValue, $scope.priceMappings);
        console.log('in watch:', $scope.prices);
      }, true);

      var uploader = $scope.uploader = new FileUploader({ url: '/admin/uploads.json' });
      uploader
        .filters
        .push({
          name: 'imageFilter',
          fn: function (item /*{File|FileLikeObject}*/) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
        });

      uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
        $scope.product.pic = response.url;
      };

      uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
      };

      uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
        submitOrUpdate();
      };

      $scope.submit = function () {
        console.log('in product submit:', $scope.product);

        $scope.product.props = JSON.stringify($scope.prices);
        $scope.product.features = $scope.features.map(function(item) { return item.value; }).join(';');

        // if an image is selected, it should be uploaded firstly.
        if (uploader.queue.length > 0) {
          console.log('exist file to upload.');
          uploader.uploadAll();
        } else {
          submitOrUpdate();
        }
      };

      function submitOrUpdate() {
        console.log('submit'); 

        var success = function() {
              alertService.addSuccess('ok.');
              $location.path('/admin/product');
            },
            failure = function() {
              alertService.addDanger('error.');
            };

        if ($scope.isUpdate) {
          Product.update({ id : $scope.product.id }, $scope.product, success, failure);
        } else {
          $scope.product.id = 0;
          Product.create($scope.product, success, failure);
        }
      };

      $scope.config = {
          minHeight       : 250,
          width           : '100%',
          resizeType      : 1,
          themeType       : 'simple',
          allowUpload     : true,
          uploadJson      : '/admin/uploads.json',
          filePostName    : 'file',
          langType        : 'zh_CN',
          urlType         : 'relative',
          fillDescAfterUploadImage : true,
          items : [
              'source', '|',
              'undo', 'redo', '|',
              'preview', 'print', 'template', 'cut', 'copy', 'paste', 'plainpaste', '|',
              'justifyleft', 'justifycenter', 'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'clearhtml', 'quickformat', '|',
              'fullscreen', '/', 'formatblock', 'fontname', 'fontsize', '|',
              'forecolor', 'hilitecolor', 'bold', 'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|',
              'image', 'multiimage', 'insertfile', 'table', 'hr', 'emoticons', 'pagebreak', 'link', 'unlink'
          ]
      };

    }
  ]);