app.directive('productAttrs', ['$timeout', function($timeout) {

  var linkFn = function(scope, element, attrs) {
    console.log('productAttrs link.');
    
    var globalIndex = 1;
    angular.forEach(scope.attrs, function(attr) {
      attr.index = globalIndex++;
      angular.forEach(attr.values, function(val) {
        val.index = globalIndex++;
      });
    });
    
    scope.addAttr = function() {
      console.log('add attr：', globalIndex, scope.attrs);
      
      scope.attrs.push({ index: globalIndex++, values: [ { index: globalIndex++ } ]});
    };

    scope.addValue = function(index) {
      console.log('add value:', globalIndex, scope.attrs);
      
      angular.forEach(scope.attrs, function(attr) {
        if (attr.index === index) {
          attr.values.push({ index: globalIndex++ });
        }
      });
    };
    
    scope.removeValue = function(index, attrIndex) {
      console.log('remove value', index, attrIndex, scope.attrs);
      
      angular.forEach(scope.attrs, function(attr, inx) {
        if (attr.index === attrIndex) {
          for(var i = 0; i < attr.values.length; i++) {
            var item = attr.values[i];
            if (item.index === index) {
              attr.values.splice(i, 1);
              break;
            }
          }
          
          // remove this attr when it's values is empty.
          if(attr.values.length === 0) {
            scope.attrs.splice(inx, 1);
          }
        }
      });
    };
  };

  return {
    restrict : 'E',
    replace : true,
    transclude : true,
    templateUrl : '/assets/tpl/directives/product-attrs.html',
    link : linkFn
  };

}]);