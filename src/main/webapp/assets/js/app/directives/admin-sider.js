app.directive("adminSider", 
    [ '$rootScope', 'LoginService', 'Order', '$location', '$interval',
    function($rootScope, loginService, Order, $location, $interval) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/assets/tpl/directives/sider.html',
            link: function($scope, $elem, $attrs) {
                
//                if (!$rootScope.findNewOrderTimer) {
//                    $rootScope.findNewOrderTimer = $interval(function () {
//                        Order.pull(function(data) {
//                            console.log(data);
//                            if (data.result) {
//                                $rootScope.menus.forEach(function(item, index) {
//                                    if (item.id === 1) {
//                                        item.name = '新订单 （√）';
//                                    }
//                                });
//                            }
//                        })
//                    }, 20 * 1000);
//                }

                if ($rootScope.menus) { return; }
    
                loginService.myMenus().success(function(res) {
    
                    if (!res.result) {
                        $location.path('/admin/login');
                        return;
                    } else {
                        var menus = [];
                        if (res.menus) {
                            menus = res.menus;
                        }
    
                        menus.push({
                            "name": "注销",
                            "url": "/logout"
                        });
                        
                        $rootScope.menus = menus;
                    }
    
                });
            }
        };
    } ]);
