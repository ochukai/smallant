app.directive('productFeatures', ['$timeout', function($timeout) {

  var linkFn = function(scope, element, attrs) {
    console.log('productFeatures link.');

    var globalIndex = 1;
    var max = 6;

    angular.forEach(scope.features, function(val) {
      val.index = globalIndex++;
    });

    scope.addFeature = function(index) {
      console.log('add feature:', globalIndex, scope.features);
      if (scope.features.length < 6) {
        scope.features.push({ index: globalIndex++ });
      }
    };

    scope.removeFeature = function(index) {
      console.log('remove feature:', index, scope.features);
      for (var i = 0; i < scope.features.length; i++) {
        var item = scope.features[i];
        if (item.index === index) {
          scope.features.splice(i, 1);
          break;
        }
      }
    };

  };

  return {
    restrict : 'E',
    replace : true,
    transclude : true,
    templateUrl : '/assets/tpl/directives/product-features.html',
    link : linkFn
  };

}]);