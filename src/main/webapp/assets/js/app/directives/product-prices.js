app.directive('productPrices', ['$timeout', function($timeout) {

  var linkFn = function(scope, element, attrs) {
    console.log('productPrices link.');
  };

  return {
    restrict : 'E',
    replace : true,
    transclude : true,
    templateUrl : '/assets/tpl/directives/product-prices.html',
    link : linkFn
  };

}]);