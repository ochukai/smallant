app.factory('Readily', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/readily/:id',
        { id: '@_id' },
        {
            'create'  : { method: 'POST' },
            'query'   : { method: 'GET', isArray: false },
            'show'    : { method: 'GET', isArray: false },
            'update'  : { method: 'POST', url: '/admin/readily/update', isArray: false },
            'destroy' : { method: 'DELETE' }
        }
    );
    return resource;
}]);