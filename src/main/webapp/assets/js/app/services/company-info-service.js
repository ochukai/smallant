app.factory('CompanyInfo', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/compnay-info/:id',
        { id: '@_id' },
        {
            'create'  : { method: 'POST' },
            'query'   : { method: 'GET', isArray: false },
            'show'    : { method: 'GET', isArray: false },
            'update'  : { method: 'POST', url: '/admin/compnay-info/update', isArray: false },
            'destroy' : { method: 'DELETE' }
        }
    );
    return resource;
}]);