app.service('oTransformer', function() {

  var oTransformer = function() {
  };

  oTransformer.prototype.parseSrc = function() {
    var _this = this;
    this.data.forEach(function(item) {
      var arr = [];

      if (!item.name || item.name === '') {
        return;
      }

      item.values.forEach(function(val) {
        if (!val.value || val.value === '') {
          return;
        }
        var id = val.id || 0;
        arr.push(id + ':' + item.name + ':' + val.value);
      });

      if (arr.length === 0) {
        return;
      }

      _this.dataArr.push(arr);
    });
  };

  oTransformer.prototype.transform = function() {
    var point = {}, pIndex = null, tempCount = 0, temp = [], list = this.dataArr;

    for ( var index in list) {
      if (typeof list[index] == 'object') {
        point[index] = {
          'parent': pIndex,
          'count': 0
        };
        pIndex = index;
      }
    }

    if (pIndex == null) {
      this.resultArr = list;
      return;
    }

    while (true) {
      for ( var index in list) {
        tempCount = point[index]['count'];
        temp.push(list[index][tempCount]);
      }

      this.resultArr.push(temp);
      temp = [];

      while (true) {
        if (point[index]['count'] + 1 >= list[index].length) {
          point[index]['count'] = 0;
          pIndex = point[index]['parent'];
          if (pIndex == null) {
            return;
          }

          index = pIndex;
        } else {
          point[index]['count']++;
          break;
        }
      }
    }
  };

  oTransformer.prototype.toObjArr = function() {
    var _this = this;

    this.result = this.resultArr.map(function(item) {

      var attrArr = item.map(function(child) {
        if (!child)
          return;
        return {
          id: child.split(':')[0],
          name: child.split(':')[1],
          value: child.split(':')[2]
        };
      });

      var textArr = item.map(function(child) {
        if (child) {
          return child.split(':')[2] || '';
        }
      });

      var idArr = item.map(function(child) {
        if (child) {
          return child.split(':')[0];
        }
      });

      var prices = _this.priceMappings.filter(function(item) {
        return idArr.join(',') === item.ids;
      });

      var price = (prices && prices.length > 0) ? prices[0].price : null;

      return {
        ids: idArr.join(','),
        title: textArr.join(' - '),
        attrs: attrArr,
        price: price
      };
    });

    console.log('oTransformer result:', this.result);
  };

  oTransformer.prototype.process = function(arr, mappings) {
    this.data = arr || [];
    this.dataArr = [];
    this.resultArr = [];
    this.result = [];
    this.priceMappings = mappings || [];

    this.parseSrc();
    this.transform();
    this.toObjArr();

    return this.result;
  };

  return new oTransformer();

});