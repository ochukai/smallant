app.factory('LoginService', ['$http',
  function ($http) {
    
    var adminBaseUrl = "/admin";
    
    return {

      login : function(data) {
        return $http.post(adminBaseUrl + '/login', data);
      },

//      profile : function() {
//        return $http.post(adminBaseUrl + '/profile');
//      },

      myMenus : function() {
        return $http.post(adminBaseUrl + '/mymenus');
      },

      logout: function() {
        return $http.post(adminBaseUrl + '/logout');
      }

    };
  }]);
