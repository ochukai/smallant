app.factory('Order', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/order/:id',
        { id: '@_id' },
        {
            'create' : { method: 'POST' },
            'query'  : { method: 'GET', isArray: false },
            'show'   : { method: 'GET', isArray: false },
            'update' : { method: 'POST', url: '/admin/order/update', isArray: false },
            'pull'   : { method: 'POST', url: '/admin/order/pull', isArray: false },
            'destroy': { method: 'DELETE' }
        }
    );
    return resource;
}]);