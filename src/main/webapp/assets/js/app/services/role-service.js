app.factory('Role', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/role/:id',
        { id: '@_id' },
        {
          'allRoles'  : { method: 'POST', url: '/admin/role/allRoles', isArray: true  }
        }
    );
    return resource;
}]);