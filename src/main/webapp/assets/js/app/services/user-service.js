app.factory('User', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/user/:id',
        { id: '@_id' },
        {
            'create'  : { method: 'POST' },
            'query'   : { method: 'GET', isArray: false },
            'show'    : { method: 'GET', isArray: false },
            'update'  : { method: 'POST', url: '/admin/user/update', isArray: false },
            'destroy' : { method: 'DELETE' }
        }
    );
    return resource;
}]);