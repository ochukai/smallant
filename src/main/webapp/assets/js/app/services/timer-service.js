app.service('FindNewOrderTimer', [ "$timeout", function timeFunctions($timeout) {
    var _intervals = {},
    _intervalUID = 1;

    return {

        $setInterval: function(operation, interval, $scope) {
            var _internalId = _intervalUID++;

            _intervals[_internalId] = $timeout(function intervalOperation() {
                operation($scope || undefined);
            }, interval);

            return _internalId;
        },

        $clearInterval: function(id) {
            return $timeout.cancel(_intervals[id]);
        }
    }
} ]);
