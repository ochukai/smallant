app.factory('Admin', ['$resource', function($resource) {
    var resource = $resource(
        '/admin/admin/:id',
        { id: '@_id' },
        {
            'create'  : { method: 'POST' },
            'query'   : { method: 'GET', isArray: false },
            'show'    : { method: 'GET', isArray: false },
            'update'  : { method: 'POST', url: '/admin/admin/update', isArray: false },
            'destroy' : { method: 'DELETE' },
            'allRoles': { method: 'POST', url: '/admin/admin/allRoles', isArray: false  }
        }
    );
    return resource;
}]);