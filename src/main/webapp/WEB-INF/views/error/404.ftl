<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="出错啦">
    <div class="container order-info error-info">
        <div class="order-info-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-md-3 text-center">
                        <span class="mark"><i class="glyphicon glyphicon-exclamation-sign"></i></span>
                    </div>
                    <div class="col-md-9 order-detail">
                        <h2>出错啦~~</h2>
                        <p>页面不存在或您无权限访问~</p>
                        <p>
                            <a href="/">前往首页</a>
                            <span>或</span>
                            <a href="javascript:history.go(-1)" target="_self">返回</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@com.page>

</#escape>