/*
 * =============================================== 
 * product-detail - Oliver 
 * ===============================================
 * 
 */
require([ 'jquery', 'bootstrap' ], function($) {

    $(function() {

        // to-pay
        $('#btn-go-to-pay').on('click', function() {
            $('#modal-pay')
                .modal({ backdrop: 'static' })
                .modal('show');
            $('#payment_form').submit();
        });

    });

});
