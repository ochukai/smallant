/*
 * =============================================== 
 * product-detail - Oliver 
 * ===============================================
 * 
 */
require([ 'jquery', 'bootstrap', '$storage' ], function($) {

    $(function() {
        var detailNames = $('.type').toArray().map(function(item) {
            return $(item).attr('id');
        });

        var priceMappings = $('[data-group="price"]').toArray().map(function(item) {
            var name = $(item).attr('name');
            var value = $(item).val();
            return {
                ids: name,
                price: value
            };
        });

        $('[data-group="price"]').remove();

        var detailParams = {};
        
        var onTokenValid = function() {
            var productId = $('input[name="id"]').val();
            detailParams['product'] = productId;

            console.log(detailParams);

            // submit
            $.ajax({
                type: "post",
                url: "/order/pre",
                data: detailParams,
                dataType: "json",
                success: function(data) {
                    if (data.result) {
                        window.location.href = data.message;
                    }
                }
            });
        };

        var onClickBuy = function() {

            // check if an user login.
            var token = $.localStorage('userToken');
            if (token && $.localStorage('userPhone')) {
                // make current user online.
                $.ajax({
                    type: "post",
                    url: "/loginWithToken",
                    data: { token: token },
                    dataType: "json",
                    success: function(data) {
                        if (!data.result) {
                            $('#modal-login').modal('show');
                        } else {
                            onTokenValid();
                        }
                    }
                });
            } else {
                // if no user has logged in, let he to login.
                $('#modal-login').modal('show');
            }
        };

        $(".type span").on('click', function() {
            // add or remove class
            var $this = $(this);
            var isRemove = false;
            if ($this.hasClass('active')) {
                isRemove = true;
                $this.removeClass('active');
            } else {
                $this.addClass('active').siblings().removeClass('active');
            }

            // add / remove {name, value}
            var name = $(this).parent().attr('id');
            var value = $(this).attr('data-value');

            if (!isRemove) {
                detailParams[name] = value;
            } else {
                delete detailParams[name];
            }

            // enable buy button
            var enable = detailNames.every(function(name) {
                return detailParams.hasOwnProperty(name);
            });

            if (enable) {
                $('.btn-commodity-select')
                    .removeClass('disabled')
                    .unbind("click.buy")
                    .bind('click.buy', onClickBuy);
            } else {
                $('.btn-commodity-select').addClass('disabled').unbind('click.buy');
            }

            // change price
            var ids = [];
            for ( var key in detailParams) {
                if (key !== 'duration' && key !== 'price' && key != 'ids') {
                    ids.push(detailParams[key]);
                }
            }

            var price = 0;
            priceMappings.forEach(function(item) {
                var itemIds = item.ids.split(',');
                // '42,41'.split(',').sort().toString() === '41,42'.split(',').sort().toString()
                if (ids.sort().toString() === itemIds.sort().toString()) {
                    price = item.price;
                    detailParams['ids'] = ids.join(',');
                    return;
                }
            });

            var totalPrice = 0;
            var month = detailParams['duration'] || 0;
            totalPrice = (price !== 0 && month !== 0) ? month * price : '--';
            detailParams['price'] = totalPrice;

            $('.commodity-price p').text(totalPrice);
        });
    });
});
