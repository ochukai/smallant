requirejs([ 'jquery', 'bootstrap', '$storage' ], function($) {

    $(function() {

        /*
         * 频率控制 返回函数连续调用时，fn 执行频率限定为每多少时间执行一次
         */
        var throttle = function(fn, delay, immediate, debounce) {
            var curr = +new Date(), //当前时间
                last_call = 0, 
                last_exec = 0, 
                timer = null, 
                diff, //时间差
                context, //上下文
                args, 
                exec = function() {
                    last_exec = curr;
                    fn.apply(context, args);
                };

            return function() {
                curr = +new Date();
                context = this, 
                args = arguments, 
                diff = curr - (debounce ? last_call : last_exec) - delay;

                clearTimeout(timer);

                if (debounce) {
                    if (immediate) {
                        timer = setTimeout(exec, delay);
                    } else if (diff >= 0) {
                        exec();
                    }
                } else {
                    if (diff >= 0) {
                        exec();
                    } else if (immediate) {
                        timer = setTimeout(exec, -diff);
                    }
                }

                last_call = curr;
            }
        };

        var onResize = function() {
            var currentBodyHeight = $(document.body).outerHeight(true);
            var windowHeight = $(window).height();

            if ($('.footer').hasClass('navbar-fixed-bottom') 
                    && windowHeight < (currentBodyHeight + $('.footer').outerHeight(true))) {
                $('.footer').removeClass('navbar-fixed-bottom');
                return;
            } else if (windowHeight > currentBodyHeight) {
                $('.footer').addClass('navbar-fixed-bottom');
            }
        };

        // run when current window is loaded.
        onResize();

        // resize 时间执行太频繁 使用throttle 的方式会限制它的执行速度
        // 当窗口被改变大小时，我这里设置的是250毫秒执行一次resize事件
        $(window).on('resize', throttle(onResize, 250));

        // scroll to top
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $('.scroll-to-top').show();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        $('.scroll-to-top').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 500);
            return false;
        });

        $('.modal').on('show.bs.modal', function(event) {
            var nav = $('div.navbar-fixed-top.navigation .container');
            var left = nav.offset().left;
            nav.css({
                position: 'absolute',
                left: left
            });
        });

        $('.modal').on('hidden.bs.modal', function(event) {
            $('div.navbar-fixed-top.navigation .container').css({
                position: 'none'
            });
        });

        // in index
        $('#btn-login').on('click', function(event) {
            $('#modal-login').modal('show');
            event.preventDefault();
        });

        $('#btn-register').on('click', function(event) {
            $('#modal-register').modal('show');
            event.preventDefault();
        });

        // in login dialog
        $('#btn-forget').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-forget').modal('show');
            }, 500);
        });

        $('#btn-log-register').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-register').modal('show');
            }, 500);
        });

        // in forget
        $('#btn-forget-login').on('click', function() {
            $('#modal-forget').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });

        // in register
        $('#btn-reg-login').on('click', function() {
            $('#modal-register').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });

        /*
         * =====================================================
         * Login-Register-Forget - Johnny
         * =====================================================
         */
        if ($.localStorage('userToken') && $.localStorage('userPhone')) {
            console.log("user exists in localStorage");

            $("#logined-user").html($.localStorage('userPhone'));
            
            // make current user online.
            $.ajax({
                type: "post",
                url: "/loginWithToken",
                data: { token: $.localStorage('userToken') },
                dataType: "json",
                success: function(data) {
                    if (data.result) {
                        $("#no-login").css({ display: "none" });
                        $("#logined").css({ display: "inline-block" });
                    }
                }
            });
        }

        // type=1 register
        // type=2 resetPassword
        function setButton(num, type) {
            var obj = new Object();
            if (type == 1) {
                obj = $("#get-code");
            } else if (type == 2) {
                obj = $("#get-code-forget");
            }
            obj.unbind();
            var clearTimer = setInterval(changeTime, 1000);
            obj.attr({
                "disabled": true
            });
            obj.html(+num + "秒后重新发送");

            /** 初期值设置 */
            function changeTime() {
                if (num > 0) {
                    num = num - 1;
                    curnum = num;
                    obj.html(+curnum + "");
                } else {
                    if (num == 0) {
                        obj.bind("click", function() {
                            sentMsg();
                        });
                        obj.html("重新获取");
                        /** 到时间后的操作 */
                        obj.removeAttr("disabled");
                        clearInterval(clearTimer);
                        num = 60;
                    }
                    num = num - 1;
                }
            }
            clearTimer;
        }

        // type=1 register
        // type=2 resetPassword
        function getCode(mobile, type) {
            var isGet;
            $.ajax({ // 一个Ajax过程
                type: "post", // 以post方式与后台沟通
                url: "/getCode",
                data: {
                    phoneNum: mobile
                },
                success: function(rr) {
                    isGet = rr.result;
                    if (isGet == true) {
                        setButton(5, type);
                    }
                }
            });
        }

        function sentMsg() {
            var mobile = $('#input-phone-register').val();
            var myreg = /(1[3-9]\d{9}$)/;

            if (!myreg.test(mobile) || mobile.length != 11) {
                $("#register_phone").addClass("has-error");
                $("#register_phone").addClass("has-feedback");
                $('#inputError2StatusRegister').css({
                    display: "inline-flex"
                });
                $('#registerSpan').css({
                    display: "inline-flex"
                });

                return;
            }

            $('#inputError2StatusRegister').css({
                display: "none"
            });
            $('#registerSpan').css({
                display: "none"
            });

            $("#register_phone").removeClass("has-error");
            $("#register_phone").removeClass("has-feedback");
            $('#inputError2StatusRegister-re').css({
                display: "none"
            });
            getCode(mobile, 1);
        }

        $('#get-code').on('click', sentMsg);
        $('#btn-register-zc').on('click', register)
        // $('#input-password-register').on('change',validatPsw)
        $('#input-password-register').bind('input propertychange', validatPsw);

        function register() {
            var psw = $('#input-password-register').val();
            var mobile = $('#input-phone-register').val();
            var code = $('#input-code-register').val();
            var obj = $('#btn-register-zc');
            if (true) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/register",
                    data: {
                        phoneNum: mobile,
                        password: psw,
                        userCode: code
                    },
                    success: function(data) {

                        if (data.result == false && data.message == "wrongCode") {
                            $("#register_code").addClass("has-error");
                            $("#register_code").addClass("has-feedback");
                            $('#inputError2StatusRegisterCode').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == false && data.message == "registeredPhone") {
                            $("#register_phone").addClass("has-error");
                            $("#register_phone").addClass("has-feedback");
                            $('#inputError2StatusRegister-re').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);

                            $("#no-login").css({
                                display: "none"
                            });

                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-register').modal('hide');
                        }
                    }
                });
            }
        }

        function validatPsw() {
            var obj = $('#btn-register-zc');
            var psw = $('#input-password-register').val();
            var mmReg = /^[0-9a-zA-Z_]{6,15}$/;
            if (!mmReg.test(psw)) {
                $("#register_psw").addClass("has-error");
                $("#register_psw").addClass("has-feedback");
                $('#inputError2StatusRegisterPsw').css({
                    display: "inline-flex"
                });
                $('#registerPswSpan').css({
                    display: "inline-flex"
                });

                obj.attr({
                    "disabled": true
                });

                return false;
            }

            // obj.removeAttr("disabled");
            obj.attr({
                "disabled": false
            });

            $("#register_psw").removeClass("has-error");
            $("#register_psw").removeClass("has-feedback");
            $('#inputError2StatusRegisterPsw').css({
                display: "none"
            });

            $('#registerPswSpan').css({
                display: "none"
            });

            return true;
        }

        function validatePhonePsw(mobile, password) {

            var myreg = /(1[3-9]\d{9}$)/;

            // phone
            if (mobile.length != 11 || !myreg.test(mobile)) {
                $("#login_phone").addClass("has-error");
                $("#login_phone").addClass("has-feedback");
                $('#inputError2StatusLogin').css({
                    display: "inline-flex"
                });
                $('#loginSpan').css({
                    display: "inline-flex"
                });

                return false;
            }

            $('#inputError2StatusLogin').css({
                display: "none"
            });
            $('#loginSpan').css({
                display: "none"
            });
            $("#login_phone").removeClass("has-error");
            $("#login_phone").removeClass("has-feedback");
            $("#inputError2StatusLogin-no-register").css({
                display: "none"
            });

            // password
            if (password.length < 6) {
                $("#login_psw").addClass("has-error");
                $("#login_psw").addClass("has-feedback");
                $('#inputError2StatusLoginPsw').css({
                    display: "inline-flex"
                });
                $('#loginSpan').css({
                    display: "inline-flex"
                });
                return false;
            }

            $("#login_psw").removeClass("has-error");
            $("#login_psw").removeClass("has-feedback");
            $('#inputError2StatusLoginPsw').css({
                display: "none"
            });
            $('#loginSpan').css({
                display: "none"
            });

            return true;
        }

        function login() {
            var mobile = $('#input-phone-login').val();
            var password = $('#input-password-login').val();
            if (validatePhonePsw(mobile, password)) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/login",
                    data: {
                        phoneNum: mobile,
                        password: password
                    },
                    success: function(data) {
                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);

                            $("#no-login").css({
                                display: "none"
                            });
                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-login').modal('hide');
                        }

                        if (data.result == false && data.message == "NoSuchPhoneNum") {

                            $("#login_phone").removeClass("has-error");
                            $("#login_phone").removeClass("has-feedback");
                            $("#inputError2StatusLogin-no-register").css({
                                display: "inline-flex"
                            });
                            $('#loginSpan').css({
                                display: "inline-flex"
                            });
                        } else if (data.result == false && data.message == "WrongPassword") {
                            $("#login_psw").addClass("has-error");
                            $("#login_psw").addClass("has-feedback");
                            $('#inputError2StatusLoginPsw').css({
                                display: "inline-flex"
                            });

                        }
                    }
                });
            }
        }

        $('#btn-login-dl').on('click', login);

        // ------------------------------------------------------------
        // forget

        function sentMsgForget() {

            var mobile = $('#input-phone-forget').val();

            var myreg = /(1[3-9]\d{9}$)/;
            if (!myreg.test(mobile) || mobile.length != 11) {
                $("#forget_phone").addClass("has-error");
                $("#forget_phone").addClass("has-feedback");
                $('#inputError2StatusForget').css({
                    display: "inline-flex"
                });
                $('#forgetSpan').css({
                    display: "inline-flex"
                });

                return;
            }

            $('#inputError2StatusForget').css({
                display: "none"
            });
            $('#forgetSpan').css({
                display: "none"
            });
            $("#forget_phone").removeClass("has-error");
            $("#forget_phone").removeClass("has-feedback");
            $('#inputError2Statusforget-re').css({
                display: "none"
            });
            getCode(mobile, 2);
        }

        function validatePsw_f() {
            var obj = $('#btn-resetpassword-xg');
            var psw = $('#input-password-forget').val();
            var mmReg = /^[0-9a-zA-Z_]{6,15}$/;
            if (!mmReg.test(psw)) {
                $("#forget_psw").addClass("has-error");
                $("#forget_psw").addClass("has-feedback");
                $('#inputError2StatusForgetPsw').css({
                    display: "inline-flex"
                });
                $('#forgetPswSpan').css({
                    display: "inline-flex"
                });

                obj.attr({
                    "disabled": true
                });

                return false;
            }

            // obj.removeAttr("disabled");
            obj.attr({
                "disabled": false
            });

            $("#forget_psw").removeClass("has-error");
            $("#forget_psw").removeClass("has-feedback");
            $('#inputError2StatusForgetPsw').css({
                display: "none"
            });
            $('#forgetPswSpan').css({
                display: "none"
            });

            return true;
        }

        function resetPassword() {
            var psw = $('#input-password-forget').val();
            var mobile = $('#input-phone-forget').val();
            var code = $('#input-code-forget').val();
            var obj = $('#btn-forget-zc');
            if (true) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/resetPassword",
                    data: {
                        phoneNum: mobile,
                        password: psw,
                        userCode: code
                    },
                    success: function(data) {

                        if (data.result == false && data.message == "wrongCode") {
                            $("#forget_code").addClass("has-error");
                            $("#forget_code").addClass("has-feedback");
                            $('#inputError2StForgetsterCode').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == false && data.message == "noRegisteredPhone") {
                            $("#forget_phone").addClass("has-error");
                            $("#forget_phone").addClass("has-feedback");
                            $('#inputError2StatusForget-re').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);

                            $("#no-login").css({
                                display: "none"
                            });
                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-forget').modal('hide');
                        }
                    }
                });
            }
        }

        $('#get-code-forget').on('click', sentMsgForget);
        $('#btn-resetpassword-xg').on('click', resetPassword)
        $('#input-password-forget').bind('input propertychange', validatePsw_f);

    });

});
