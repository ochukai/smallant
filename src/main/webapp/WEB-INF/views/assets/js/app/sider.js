/**
 * =========================
 * Johnny
 * =========================
 * 
 */
require([ 'jquery', 'bootstrap' ], function($) {

    $('#li-order').click(function(){
        $("#li-order").addClass("active");
        $("#li-search").removeClass("active");
        $("#li-uploadPic").removeClass("active");
        $("#li-logout").removeClass("active");
    });
    
    $('#li-search').click(function() {
        $("#li-search").addClass("active");
        $("#li-order").removeClass("active");
        $("#li-uploadPic").removeClass("active");
        $("#li-logout").removeClass("active");
    });
    
    $('#li-uploadPic').click(function() {
        $("#li-uploadPic").addClass("active");
        $("#li-search").removeClass("active");
        $("#li-order").removeClass("active");
        $("#li-logout").removeClass("active");
    });
    
    $('#li-logout').click( function() {
        $("#li-logout").addClass("active");
        $("#li-search").removeClass("active");
        $("#li-uploadPic").removeClass("active");
        $("#li-order").removeClass("active");
    });
});