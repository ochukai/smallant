/*
 * =============================================== 
 * pay-success - Oliver 
 * ===============================================
 * 
 */
require([ 'jquery', 'bootstrap' ], function($) {

    $(function() {

        // company-info modal
        $('#modal-company-info').on('show.bs.modal', function(event) {
            var orderId = $(this).data('order');
            $(this).find('#orderId').val(orderId);
        });

        $('#btn-fill-compnay-info').on('click', function() {
            $('#modal-company-info')
                .data("order", $(this).data('order'))
                .modal({ backdrop: 'static' })
                .modal('show');
        });

        //        $('.order-detail').on('click', function() {
        //            $('#modal-company-info')
        //                .data("order", $(this).data('order'))
        //                .modal({ backdrop: 'static' })
        //                .modal('show');
        //        });

    });

});
