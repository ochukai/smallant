require([ 'jquery', 'bootstrap', '$validate', '$storage', '$citySelect' ], function($) {

    // override jquery validate plugin defaults
    $.extend($.validator.messages, {
        required: "请填写该字段",
        remote: "请修正该字段",
        email: "请输入正确格式的电子邮件",
        url: "请输入合法的网址",
        date: "请输入合法的日期",
        dateISO: "请输入合法的日期 (ISO).",
        number: "请输入合法的数字",
        digits: "只能输入整数",
        creditcard: "请输入合法的信用卡号",
        equalTo: "请再次输入相同的值",
        accept: "请输入拥有合法后缀名的字符串",
        maxlength: jQuery.validator.format("请输入一个 长度最多是 {0} 的字符串"),
        minlength: jQuery.validator.format("请输入一个 长度最少是 {0} 的字符串"),
        rangelength: jQuery.validator.format("请输入 一个长度介于 {0} 和 {1} 之间的字符串"),
        range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
        max: jQuery.validator.format("请输入一个最大为{0} 的值"),
        min: jQuery.validator.format("请输入一个最小为{0} 的值")
    });

    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error').removeClass('has-success');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').addClass('has-success').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $.validator.addMethod("isPhone", function(value, element) {
        var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
        var tel = /^\d{3,4}-?\d{7,9}$/;
        return this.optional(element) || (tel.test(value) || mobile.test(value));
    });

    $(function() {

        // company-info province-city-combo-select
        $('#re-address').citySelect({
            prov: "上海",
            nodata: "none"
        });

        $('#label-address-prefix').text('上海-黄浦区');
        $('#addressPrefix').val('上海-黄浦区');

        /*add event here*/
        $('#re-address select').on('change', function() {
            setTimeout(function() {
                var prefix = $('#re-address select').toArray().map(function(item) {
                    return $(item).val();
                });

                for (var i = 0; i < prefix.length; i++) {
                    var value = prefix[i];
                    if (!value || value === '') {
                        prefix.splice(i, 1);
                    }
                }

                var prefixStr = prefix.join('-')
                $('#label-address-prefix').text(prefixStr);
                $('#addressPrefix').val(prefixStr);
            }, 250);
        });

        $("#form-company-info").validate({
            rules: {
                'name': {
                    required: true
                },
                'taxCode': {
                    required: true
                },
                'manager': {
                    required: true
                },
                'phone': {
                    required: true,
                    isPhone: true
                },
                'receiverAddress': {
                    required: true
                },
                'receiver': {
                    required: true
                },
                'receiverPhone': {
                    required: true,
                    isPhone: true
                }
            },
            messages: {
                'name': {
                    required: '请输入公司名称'
                },
                'taxCode': {
                    required: '请输入纳税人识别号'
                },
                'manager': {
                    required: '请输入公司负责人姓名 '
                },
                'phone': {
                    required: '请输入公司联系电话',
                    isPhone: '请输入正确的公司联系电话'
                },
                'receiverAddress': {
                    required: '请输入有效收件地址'
                },
                'receiver': {
                    required: '请输入收件联系人姓名'
                },
                'receiverPhone': {
                    required: '请输入收件人电话',
                    isPhone: '请输入正确的公司联系电话'
                }
            // , password: {
            //     required: "请输入密码",
            //     minlength: jQuery.format("密码不能小于{0}个字 符")
            // },
            // confirm_password: {
            //     required: "请输入确认密码",
            //     minlength: "确认密码不能小于5个字符",
            //     equalTo: "两次输入密码不一致不一致"
            // }
            }
        });

        $('#btn-company-info-submit').on('click', function() {
            $('#form-company-info').submit();
        });
    });

});
