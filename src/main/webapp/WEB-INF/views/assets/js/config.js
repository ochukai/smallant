var requirejs = {
    baseUrl: '/public/js',
    paths: {
        main: 'app/main',
        jquery: 'lib/jquery',
        bootstrap: 'lib/bootstrap.min',
        $validate: 'lib/jquery.validate.min',
        $storage: 'lib/jquery.storage',
        $citySelect: 'lib/jquery.cityselect'
    },
    shim: {
        jquery: {
            exports: "$"
        },
        bootstrap: [ "jquery" ],
        $validate: [ 'jquery' ],
        $storage: [ 'jquery' ],
        $citySelect: [ 'jquery' ]

    //        livequery: {
    //            deps: [ "jquery" ],
    //            exports: "livequery"
    //        },
    }
};
