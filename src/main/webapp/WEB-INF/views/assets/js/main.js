+function($) {

    $(function() {

        
        /*
         * 频率控制 返回函数连续调用时，fn 执行频率限定为每多少时间执行一次
         */
        var throttle = function (fn, delay, immediate, debounce) {
            var curr = +new Date(),//当前时间
                last_call = 0,
                last_exec = 0,
                timer = null,
                diff, //时间差
                context,//上下文
                args,
                exec = function () {
                    last_exec = curr;
                    fn.apply(context, args);
                };

            return function () {
                curr= +new Date();
                context = this,
                args = arguments,
                diff = curr - (debounce ? last_call : last_exec) - delay;
                
                clearTimeout(timer);

                if (debounce) {
                    if (immediate) {
                        timer = setTimeout(exec, delay);
                    } else if (diff >= 0) {
                        exec();
                    }
                } else {
                    if (diff >= 0) {
                        exec();
                    } else if (immediate) {
                        timer = setTimeout(exec, -diff);
                    }
                }

                last_call = curr;
            }
        };

        var onResize = function() {
            var currentBodyHeight = $(document.body).outerHeight(true);
            var windowHeight = $(window).height();

            $('.footer').toggleClass(
                'navbar-fixed-bottom',
                    // 没有加'navbar-fixed-bottom' 的时候 body 的高度包括footer 的高度
                (windowHeight > currentBodyHeight)
                    // 加了'navbar-fixed-bottom'之后，body的高度不包括footer的高度 所以手动加上，
                    // 以保证浏览器窗口高度减小时，body 与footer 不重叠在一起
                || (windowHeight > currentBodyHeight + 227)
            );
        };

        // run when current window is loaded.
        onResize();

        // resize 时间执行太频繁 使用throttle 的方式会限制它的执行速度
        // 当窗口被改变大小时，我这里设置的是250毫秒执行一次resize事件
        $(window).on('resize', throttle(onResize, 250));

        // scroll to top
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $('.scroll-to-top').show();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        $('.scroll-to-top').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 500);
            return false;
        });

        $('.modal').on('show.bs.modal', function (event) {
            var nav  = $('div.navbar-fixed-top.navigation .container');
            var left = nav.offset().left;
            nav.css({
                position: 'absolute',
                left: left
            });
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $('div.navbar-fixed-top.navigation .container').css({ position: 'none' });
        });

        // in index
        $('#btn-login').on('click', function(event) {
            $('#modal-login').modal('show');
            event.preventDefault();
        });

        $('#btn-register').on('click', function(event) {
            $('#modal-register').modal('show');
            event.preventDefault();
        });

        // in login dialog
        $('#btn-forget').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-forget').modal('show');
            }, 500);
        });

        $('#btn-log-register').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-register').modal('show');
            }, 500);
        });

        // in forget
        $('#btn-forget-login').on('click', function() {
            $('#modal-forget').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });

        // in register
        $('#btn-reg-login').on('click', function() {
            $('#modal-register').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });
        
        /*
         * =====================================================
         * Login-Register-Forget - Johnny
         * =====================================================
         */
        if ($.localStorage('userToken') && $.localStorage('userPhone')) {
            console.log("user exists in localStorage");

            $("#logined-user").html($.localStorage('userPhone'));
            
            $("#no-login").css({ display: "none" });
            $("#logined").css({ display: "inline-block" });
        }

        // type=1 register
        // type=2 resetPassword
        function setButton(num, type) {
            var obj = new Object();
            if (type == 1) {
                obj = $("#get-code");
            } else if (type == 2) {
                obj = $("#get-code-forget");
            }
            obj.unbind();
            var clearTimer = setInterval(changeTime, 1000);
            obj.attr({ "disabled": true });
            obj.html(+num + "秒后重新发送");
            
            /** 初期值设置 */
            function changeTime() {
                if (num > 0) {
                    num = num - 1;
                    curnum = num;
                    obj.html(+curnum + "");
                } else {
                    if (num == 0) {
                        obj.bind("click", function() {
                            sentMsg();
                        });
                        obj.html("重新获取");
                        /** 到时间后的操作 */
                        obj.removeAttr("disabled");
                        clearInterval(clearTimer);
                        num = 60;
                    }
                    num = num - 1;
                }
            }
            clearTimer;
        }

        // type=1 register
        // type=2 resetPassword
        function getCode(mobile, type) {
            var isGet;
            $.ajax({ // 一个Ajax过程
                type: "post", // 以post方式与后台沟通
                url: "/getCode",
                data: {
                    phoneNum: mobile
                },
                success: function(rr) {
                    isGet = rr.result;
                    if (isGet == true) {
                        setButton(5, type);
                    }
                }
            });
        }

        function sentMsg() {
            var mobile = $('#input-phone-register').val();
            var myreg = /(1[3-9]\d{9}$)/;
            
            if (!myreg.test(mobile) || mobile.length != 11) {
                $("#register_phone").addClass("has-error");
                $("#register_phone").addClass("has-feedback");
                $('#inputError2StatusRegister').css({
                    display: "inline-flex"
                });
                $('#registerSpan').css({
                    display: "inline-flex"
                });

                return;
            }

            $('#inputError2StatusRegister').css({ display: "none" });
            $('#registerSpan').css({ display: "none" });
            
            $("#register_phone").removeClass("has-error");
            $("#register_phone").removeClass("has-feedback");
            $('#inputError2StatusRegister-re').css({
                display: "none"
            });
            getCode(mobile, 1);
        }

        $('#get-code').on('click', sentMsg);
        $('#btn-register-zc').on('click', register)
        // $('#input-password-register').on('change',validatPsw)
        $('#input-password-register').bind('input propertychange', validatPsw);

        function register() {
            var psw = $('#input-password-register').val();
            var mobile = $('#input-phone-register').val();
            var code = $('#input-code-register').val();
            var obj = $('#btn-register-zc');
            if (true) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/register",
                    data: {
                        phoneNum: mobile,
                        password: psw,
                        userCode: code
                    },
                    success: function(data) {

                        if (data.result == false && data.message == "wrongCode") {
                            $("#register_code").addClass("has-error");
                            $("#register_code").addClass("has-feedback");
                            $('#inputError2StatusRegisterCode').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == false && data.message == "registeredPhone") {
                            $("#register_phone").addClass("has-error");
                            $("#register_phone").addClass("has-feedback");
                            $('#inputError2StatusRegister-re').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);
                            
                            $("#no-login").css({
                                display: "none"
                            });
                            
                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-register').modal('hide');
                        }
                    }
                });
            }
        }

        function validatPsw() {
            var obj = $('#btn-register-zc');
            var psw = $('#input-password-register').val();
            var mmReg = /^[0-9a-zA-Z_]{6,15}$/;
            if (!mmReg.test(psw)) {
                $("#register_psw").addClass("has-error");
                $("#register_psw").addClass("has-feedback");
                $('#inputError2StatusRegisterPsw').css({
                    display: "inline-flex"
                });
                $('#registerPswSpan').css({
                    display: "inline-flex"
                });

                obj.attr({
                    "disabled": true
                });

                return false;
            }

            // obj.removeAttr("disabled");
            obj.attr({
                "disabled": false
            });

            $("#register_psw").removeClass("has-error");
            $("#register_psw").removeClass("has-feedback");
            $('#inputError2StatusRegisterPsw').css({
                display: "none"
            });
            
            $('#registerPswSpan').css({
                display: "none"
            });

            return true;
        }

        function validatePhonePsw(mobile, password) {

            var myreg = /(1[3-9]\d{9}$)/;

            // phone
            if (mobile.length != 11 || !myreg.test(mobile)) {
                $("#login_phone").addClass("has-error");
                $("#login_phone").addClass("has-feedback");
                $('#inputError2StatusLogin').css({
                    display: "inline-flex"
                });
                $('#loginSpan').css({
                    display: "inline-flex"
                });

                return false;
            }

            $('#inputError2StatusLogin').css({
                display: "none"
            });
            $('#loginSpan').css({
                display: "none"
            });
            $("#login_phone").removeClass("has-error");
            $("#login_phone").removeClass("has-feedback");
            $("#inputError2StatusLogin-no-register").css({
                display: "none"
            });

            // password
            if (password.length < 6) {
                $("#login_psw").addClass("has-error");
                $("#login_psw").addClass("has-feedback");
                $('#inputError2StatusLoginPsw').css({
                    display: "inline-flex"
                });
                $('#loginSpan').css({
                    display: "inline-flex"
                });
                return false;
            }

            $("#login_psw").removeClass("has-error");
            $("#login_psw").removeClass("has-feedback");
            $('#inputError2StatusLoginPsw').css({
                display: "none"
            });
            $('#loginSpan').css({
                display: "none"
            });

            return true;
        }

        function login() {
            var mobile = $('#input-phone-login').val();
            var password = $('#input-password-login').val();
            if (validatePhonePsw(mobile, password)) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/login",
                    data: {
                        phoneNum: mobile,
                        password: password
                    },
                    success: function(data) {
                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);
                            
                            $("#no-login").css({
                                display: "none"
                            });
                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-login').modal('hide');
                        }
                        
                        if (data.result == false && data.message == "NoSuchPhoneNum") {

                            $("#login_phone").removeClass("has-error");
                            $("#login_phone").removeClass("has-feedback");
                            $("#inputError2StatusLogin-no-register").css({
                                display: "inline-flex"
                            });
                            $('#loginSpan').css({
                                display: "inline-flex"
                            });
                        } else if (data.result == false && data.message == "WrongPassword") {
                            $("#login_psw").addClass("has-error");
                            $("#login_psw").addClass("has-feedback");
                            $('#inputError2StatusLoginPsw').css({
                                display: "inline-flex"
                            });

                        }
                    }
                });
            }
        }

        $('#btn-login-dl').on('click', login);

        // ------------------------------------------------------------
        // forget

        function sentMsgForget() {

            var mobile = $('#input-phone-forget').val();

            var myreg = /(1[3-9]\d{9}$)/;
            if (!myreg.test(mobile) || mobile.length != 11) {
                $("#forget_phone").addClass("has-error");
                $("#forget_phone").addClass("has-feedback");
                $('#inputError2StatusForget').css({
                    display: "inline-flex"
                });
                $('#forgetSpan').css({
                    display: "inline-flex"
                });

                return;
            }

            $('#inputError2StatusForget').css({
                display: "none"
            });
            $('#forgetSpan').css({
                display: "none"
            });
            $("#forget_phone").removeClass("has-error");
            $("#forget_phone").removeClass("has-feedback");
            $('#inputError2Statusforget-re').css({
                display: "none"
            });
            getCode(mobile, 2);
        }

        function validatePsw_f() {
            var obj = $('#btn-resetpassword-xg');
            var psw = $('#input-password-forget').val();
            var mmReg = /^[0-9a-zA-Z_]{6,15}$/;
            if (!mmReg.test(psw)) {
                $("#forget_psw").addClass("has-error");
                $("#forget_psw").addClass("has-feedback");
                $('#inputError2StatusForgetPsw').css({
                    display: "inline-flex"
                });
                $('#forgetPswSpan').css({
                    display: "inline-flex"
                });

                obj.attr({
                    "disabled": true
                });

                return false;
            }

            // obj.removeAttr("disabled");
            obj.attr({
                "disabled": false
            });

            $("#forget_psw").removeClass("has-error");
            $("#forget_psw").removeClass("has-feedback");
            $('#inputError2StatusForgetPsw').css({
                display: "none"
            });
            $('#forgetPswSpan').css({
                display: "none"
            });

            return true;
        }

        function resetPassword() {
            var psw = $('#input-password-forget').val();
            var mobile = $('#input-phone-forget').val();
            var code = $('#input-code-forget').val();
            var obj = $('#btn-forget-zc');
            if (true) {
                $.ajax({ // 一个Ajax过程
                    type: "post", // 以post方式与后台沟通
                    url: "/resetPassword",
                    data: {
                        phoneNum: mobile,
                        password: psw,
                        userCode: code
                    },
                    success: function(data) {

                        if (data.result == false && data.message == "wrongCode") {
                            $("#forget_code").addClass("has-error");
                            $("#forget_code").addClass("has-feedback");
                            $('#inputError2StForgetsterCode').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == false && data.message == "noRegisteredPhone") {
                            $("#forget_phone").addClass("has-error");
                            $("#forget_phone").addClass("has-feedback");
                            $('#inputError2StatusForget-re').css({
                                display: "inline-flex"
                            });
                        }

                        if (data.result == true) {
                            $("#logined-user").html(mobile);
                            $.localStorage('userToken', data.message);
                            $.localStorage('userPhone', mobile);

                            $("#no-login").css({
                                display: "none"
                            });
                            $("#logined").css({
                                display: "inline-block"
                            });

                            $('#modal-forget').modal('hide');
                        }
                    }
                });
            }
        }

        $('#get-code-forget').on('click', sentMsgForget);
        $('#btn-resetpassword-xg').on('click', resetPassword)
        $('#input-password-forget').bind('input propertychange', validatePsw_f);

        /*
         * =============================================== 
         * product-detail - Oliver 
         * ===============================================
         * 
         */
        var detailNames = $('.type').toArray().map(function(item) {
            return $(item).attr('id');
        });

        var priceMappings = $('[data-group="price"]').toArray().map(function(item) {
            var name = $(item).attr('name');
            var value = $(item).val();
            return {
                ids: name,
                price: value
            };
        });

        $('[data-group="price"]').remove();

        var detailParams = {};

        var onClickBuy = function() {
            
            // check if an user login.
            
            var productId = $('input[name="id"]').val();
            detailParams['product'] = productId;
            
            console.log(detailParams);

            // submit
            $.ajax({
                type: "post",
                url: "/order/pre",
                data: detailParams,
                dataType: "json",
                success: function(data) {
                    if (data.result) {
                        window.location.href = data.message;
                    }
                }
            });
        };

        $(".type span").on('click', function() {
            // add or remove class
            var $this = $(this);
            var isRemove = false;
            if ($this.hasClass('active')) {
                isRemove = true;
                $this.removeClass('active');
            } else {
                $this.addClass('active').siblings().removeClass('active');
            }

            // add / remove {name, value}
            var name = $(this).parent().attr('id');
            var value = $(this).attr('data-value');

            if (!isRemove) {
                detailParams[name] = value;
            } else {
                delete detailParams[name];
            }

            // enable buy button
            var enable = detailNames.every(function(name) {
                return detailParams.hasOwnProperty(name);
            });

            if (enable) {
                $('.btn-commodity-select')
                    .removeClass('disabled')
                    .unbind("click.buy")
                    .bind('click.buy', function() {
                        onClickBuy(this);
                    });
            } else {
                $('.btn-commodity-select').addClass('disabled').unbind('click.buy');
            }

            // change price
            var ids = [];
            for (var key in detailParams) {
                if (key !== 'duration' && key !== 'price' && key!= 'ids') {
                    ids.push(detailParams[key]);
                }
            }

            var price = 0;
            priceMappings.forEach(function(item) {
                var itemIds = item.ids.split(',');
                // '42,41'.split(',').sort().toString() === '41,42'.split(',').sort().toString()
                if (ids.sort().toString() === itemIds.sort().toString()) {
                    price = item.price;
                    detailParams['ids'] = ids.join(',');
                    return;
                }
            });

            var totalPrice = 0;
            var month = detailParams['duration'] || 0;
            totalPrice = (price !== 0 && month !== 0) ? month * price : '--';
            detailParams['price'] = totalPrice;

            $('.commodity-price p').text(totalPrice);
        });
        
        
        /*
         * =================================
         * =================================
         */
        function requestAlipay() {

        	var WIDout_trade_no = $("#WIDout_trade_no").val();
        	var WIDsubject = $("#WIDsubject").val();
        	var WIDtotal_fee = $("#WIDtotal_fee").val();
        	var WIDbody = $("#WIDbody").val();
        	var WIDshow_url = "";
        	
        	var paramStr="out_trade_no="+WIDout_trade_no+"&subject="+ WIDsubject+"&total_fee="+ WIDtotal_fee+"&body="+WIDbody+"&show_url="+ WIDshow_url;
        	
        	
        	var param="http://www.antcw.com/alipayapi.jsp?"+paramStr;
        	window.open(param);

        }
        
        $("#btn-pay").on('click',requestAlipay); 
        
        /*
         * =================================
         * =================================
         */
        
        
        /*
         * =================================
         * to-pay and pay-success form - Oliver
         * =================================
         */
        
        // to-pay
        $('#btn-go-to-pay').on('click', function() {
            $('#modal-pay')
                .modal({ backdrop: 'static' })
                .modal('show');
            $('#payment_form').submit();
        });

        // company-info modal
        $('#modal-company-info').on('show.bs.modal', function (event) {
            var orderId = $(this).data('order');
            $(this).find('#orderId').val(orderId);
        });

        $('#btn-fill-compnay-info').on('click', function() {
            $('#modal-company-info')
                .data("order", $(this).data('order'))
                .modal({ backdrop: 'static' })
                .modal('show');
        });
        
        $('.order-detail').on('click', function() {
            $('#modal-company-info')
                .data("order", $(this).data('order'))
                .modal({ backdrop: 'static' })
                .modal('show');
        });

        // company-info province-city-combo-select
        $('#re-address').citySelect({ prov:"上海", nodata:"none" });
        $('#label-address-prefix').text('上海-黄浦区');
        $('#addressPrefix').val('上海-黄浦区');
        /*add event here*/
        $('#re-address select').on('change', function() {
            setTimeout(function() {
                var prefix = $('#re-address select').toArray().map(function(item) {
                    return $(item).val();
                });
                
                for (var i = 0; i < prefix.length; i++) {
                    var value = prefix[i];
                    if (!value || value === '') {
                        prefix.splice(i, 1);
                    }
                }
                
                var prefixStr = prefix.join('-')
                $('#label-address-prefix').text(prefixStr);
                $('#addressPrefix').val(prefixStr);
            }, 250);
        });

        // override jquery validate plugin defaults
        $.extend($.validator.messages, {
            required: "请填写该字段",
            remote: "请修正该字段",
            email: "请输入正确格式的电子邮件",
            url: "请输入合法的网址",
            date: "请输入合法的日期",
            dateISO: "请输入合法的日期 (ISO).",
            number: "请输入合法的数字",
            digits: "只能输入整数",
            creditcard: "请输入合法的信用卡号",
            equalTo: "请再次输入相同的值",
            accept: "请输入拥有合法后缀名的字符串",
            maxlength: jQuery.validator.format("请输入一个 长度最多是 {0} 的字符串"),
            minlength: jQuery.validator.format("请输入一个 长度最少是 {0} 的字符串"),
            rangelength: jQuery.validator.format("请输入 一个长度介于 {0} 和 {1} 之间的字符串"),
            range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
            max: jQuery.validator.format("请输入一个最大为{0} 的值"),
            min: jQuery.validator.format("请输入一个最小为{0} 的值")
        });

        $.validator.setDefaults({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error').removeClass('has-success');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').addClass('has-success').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod(
            "isPhone",
            function (value, element) {
                var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
                var tel = /^\d{3,4}-?\d{7,9}$/;
                return this.optional(element) || (tel.test(value) || mobile.test(value));
            }
//            , "请正确填写您的联系电话"
        );

        $("#form-company-info").validate({
            rules: {
                'name': {
                    required: true
                },
                'taxCode': {
                    required: true
                },
                'manager': {
                    required: true
                },
                'phone': {
                    required: true,
                    isPhone: true
                },
                'receiverAddress': {
                    required: true
                },
                'receiver': {
                    required: true
                },
                'receiverPhone': {
                    required: true,
                    isPhone: true
                }
            }
            // add message later
            , messages: {
                'name': {
                    required: '请输入公司名称'
                },
                'taxCode': {
                    required: '请输入纳税人识别号'
                },
                'manager': {
                    required: '请输入公司负责人姓名 '
                },
                'phone': {
                    required: '请输入公司联系电话',
                    isPhone: '请输入正确的公司联系电话'
                },
                'receiverAddress': {
                    required: '请输入有效收件地址'
                },
                'receiver': {
                    required: '请输入收件联系人姓名'
                },
                'receiverPhone': {
                    required: '请输入收件人电话',
                    isPhone: '请输入正确的公司联系电话'
                }
//                , password: {
//                    required: "请输入密码",
//                    minlength: jQuery.format("密码不能小于{0}个字 符")
//                },
//                confirm_password: {
//                    required: "请输入确认密码",
//                    minlength: "确认密码不能小于5个字符",
//                    equalTo: "两次输入密码不一致不一致"
//                }
            }
        });

        $('#btn-company-info-submit').on('click', function() {
            $('#form-company-info').submit();
        });
        
    });

}(window.jQuery);
