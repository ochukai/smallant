<!DOCTYPE html>
<html lang="zh" ng-app="myApp">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Oliver">

<title>Smallant Admin.</title>

<!-- Bootstrap core CSS-->
<link href="/release/admin/css/bootstrap.min.css" rel="stylesheet">
<link href="/release/admin/css/loading-bar.min.css" rel="stylesheet">

<!-- editor styles-->
<link href="/assets/js/vendor/kindeditor/themes/default/default.css" rel="stylesheet">
<link href="/assets/js/vendor/kindeditor/themes/simple/simple.css" rel="stylesheet">

<!-- my -->
<link href="/assets/css/styles.css" rel="stylesheet">

</head>

<body>
  <!-- alerts-->
  <div class="alerts">
    <div ng-repeat="alert in alerts" class="alert alert-{{alert.type}} animation">
      <button type="button" ng-click="alert.close()" aria-hidden="true" class="close">×</button>
      {{ alert.msg }}
    </div>
  </div>

  <!-- sider menus-->
  <admin-sider></admin-sider>

  <!-- main-->
  <div role="main" class="main">
    <div class="container-fluid">
      <ng-view></ng-view>
    </div>
  </div>

  <script type="text/ng-template" id="myModalContent.html">
<div class="modal-header">
  <h4 class="modal-title">删除？</h4>
</div>
<div class="modal-body">
  <p>真的要删除这一条吗(<span>这是可以恢复的</span>)？</p>
</div>
<div class="modal-footer">
  <button class="btn btn-danger" ng-click="ok()"> 删除 </button>
  <button class="btn btn-default" ng-click="cancel()"> 不 </button>
</div>
  </script>

  <!-- Jquery-->
  <script src="/release/admin/js/jquery.min.js"></script>
  
  <!-- Angular-->
  <script src="/release/admin/js/angular.min.js"></script>
  <script src="/release/admin/js/angular-animate.min.js"></script>
  <script src="/release/admin/js/angular-route.min.js"></script>
  <script src="/release/admin/js/angular-resource.min.js"></script>
  <script src="/release/admin/js/ui-bootstrap-tpls.min.js"></script>
  <script src="/release/admin/js/loading-bar.min.js"></script>
  
  <!-- other libs-->
  <script src="/assets/js/vendor/kindeditor/kindeditor-min.js"></script>
  <script src="/assets/js/vendor/kindeditor/lang/zh_CN.js"></script>
  <script src="/assets/js/vendor/angular-kindeditor/angular-kindeditor.js"></script>
  
  <script src="/assets/js/vendor/angular-locale_zh-cn.js"></script>
  
  <!-- angular file upload-->
  <script src="/assets/bower-components/es5-shim/es5-shim.min.js"></script>
  <script src="/assets/bower-components/es5-shim/es5-sham.min.js"></script>
  <script src="/release/admin/js/angular-file-upload.min.js"></script>
  
  <!-- app-->
  <script src="/release/admin/js/app.min.js"></script>

</body>
</html>