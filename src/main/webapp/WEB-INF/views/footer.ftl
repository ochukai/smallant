
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <ul class="list-unstyled qr-code">
                        <li class="col-xs-6">
                            <img src="/public/images/qr-serve.jpg" class="img-responsive img-thumbnail" alt="微信服务号">
                            <span>微信服务号</span>
                        </li>
                        <li class="col-xs-6">
                            <img src="/public/images/qr-sub.jpg" class="img-responsive img-thumbnail" alt="微信订阅号">
                            <span>微信订阅号</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled list-inline contact-us-section text-right">
                    <li class="header">联系我们</li>
                    <li class="phone">4000-000-000</li>
                </ul>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled text-right">
                            <li class="header">我们公司</li>
                            <li><p class="copy">Copyright&copy;2015 小蚁记账 版权所有 那硕（上海）财务咨询有限公司旗下网站</p></li>
                            <li><p class="copy">沪ICP备14053360号-2</p></li>
                            <li class="copy-img img-thumbnail"><img src="/public/images/360.png"></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="scroll-to-top">
    <i class="glyphicon glyphicon-chevron-up"></i>
</div>
