<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="主页">


    <div class="banner-box">
        <div id="carousel-banner-box" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-banner-box" data-slide-to="0" class=""></li>
                <li data-target="#carousel-banner-box" data-slide-to="1" class=""></li>
                <li data-target="#carousel-banner-box" data-slide-to="2" class="active"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item">
                    <img class="img-responsive" src="public/images/banner-1.jpg" alt="s">

                </div>
                <div class="item">
                    <img class="img-responsive" alt="900x500" src="public/images/banner-2.jpg">
                    <div class="carousel-caption">
                        
                    </div>
                </div>
                <div class="item active">
                    <img class="img-responsive" alt="900x500" src="public/images/banner-4.jpg">
                    <div class="carousel-caption">
                       
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-banner-box" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-banner-box" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <#include "/index/product-list.ftl">
    
    <#include "/index/service.ftl">
    
    <#include "/index/feature.ftl">

</@com.page>

</#escape>