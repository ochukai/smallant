<div class="modal fade in" id="modal-company-info" tabindex="-1" role="dialog" aria-labelledby="payModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">请填写公司信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-company-info" class="form-horizontal" method="post" action="/order/companyinfo">
                        <input type="hidden" id="orderId" name="orderId" value="" />
                        <div class="form-group">
                            <label for="input-name" class="col-sm-3 control-label">公司名称</label>
                            <div class="col-sm-7">
                                <input name="name" type="text" class="form-control" id="input-name" placeholder="公司名称">
                                <span class="help-block"> 请输入公司名称 </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-code" class="col-sm-3 control-label">纳税人识别号</label>
                            <div class="col-sm-7">
                                <input name="taxCode" type="text" class="form-control" id="input-code" placeholder="纳税人识别号">
                                <span class="help-block"> 请输入纳税人识别号 </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-manager" class="col-sm-3 control-label">公司负责人姓名</label>
                            <div class="col-sm-7">
                                <input name="manager" type="text" class="form-control" id="input-manager" placeholder="公司负责人姓名">
                                <span class="help-block"> 请输入公司负责人姓名 </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-phone" class="col-sm-3 control-label">公司联系电话</label>
                            <div class="col-sm-7">
                                <input name="phone" type="tel" class="form-control" id="input-phone" placeholder="公司联系电话">
                                <span class="help-block"> 请输入公司联系电话 </span>
                            </div>
                        </div>

                        <hr/>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">请选择地址</label>
                            <div class="col-sm-8 receive-address">
                                <div id="re-address" class="row">
                                    <div class="col-sm-4">
                                        <select class="form-control prov"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control city" disabled="disabled"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control dist" disabled="disabled"></select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-address" class="col-sm-3 control-label">请输入详细地址</label>
                            <input type="hidden" id="addressPrefix" name="addressPrefix" value="" />
                            <div class="col-sm-7">
                                <label id="label-address-prefix" class="form-control-static"> </label>
                                <input name="receiverAddress" id="input-address" type="text" class="form-control" placeholder="详细地址">
                                <span class="help-block"> 请输入有效收件地址 </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-receiver" class="col-sm-3 control-label">收件联系人</label>
                            <div class="col-sm-7">
                                <input name="receiver" type="text" class="form-control" id="input-receiver" placeholder="收件联系人">
                                <span class="help-block"> 请输入收件联系人 </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-receiver-phone" class="col-sm-3 control-label">收件人电话</label>
                            <div class="col-sm-7">
                                <input name="receiverPhone" type="tel" class="form-control" id="input-receiver-phone" placeholder="收件人电话">
                                <span class="help-block"> 请输入收件人电话 </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-company-info-submit"> 提交 </button>
            </div>
        </div>
    </div>
</div>

<script src='/public/js/app/company-info.js'></script>