<div class="modal fade in" id="modal-pay" tabindex="-1" role="dialog" aria-labelledby="payModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">支付提示</h4>
            </div>
            <div class="modal-body">
                <p>请您在新打开的网上银行页面进行支付，支付完成前请不要关闭该窗口。</p>
            </div>
            <div class="modal-footer">
                <a id="btn-pay-success" class="btn btn-success" href="/order/${order.orderId}/result"> 支付成功 </a>
                <a id="btn-pay-failed" class="btn btn-default" target="_blank" href="#"> 支付失败 </a>
            </div>
        </div>
    </div>
</div>