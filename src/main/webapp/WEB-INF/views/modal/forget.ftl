
<!--forget modal-->
<div class="modal fade" id="modal-forget" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="registerModalLabel"> 忘记密码 </h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="input-phone" class="col-sm-4 control-label">手机</label>
                        <div  id="forget_phone" class="col-sm-6">
                            <input type="tel" class="form-control" id="input-phone-forget" placeholder="请输入手机号。">
                            <span id="forgetSpan" class="glyphicon glyphicon-remove form-control-feedback" style="display:none" aria-hidden="true"></span>
                            <span id="inputError2StatusForget" style="display:none" class="help-block"> 手机号码不正确。 </span>
                            <span id="inputError2StatusForget-re" style="display:none" class="help-block">该手机号没有注册过。 </span>
                        </div>
                    </div>
                    
                    <hr />
                    <div class="form-group">
                        <label for="input-code" class="col-sm-4 control-label">短信验证码</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="input-code-forget" type="text" class="form-control" placeholder="请输入验证码。">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="get-code-forget">获取验证码</button>
                                </span>
                            </div>
                            <span id="inputError2StatusForgetCode" style="display:none" class="help-block"> 验证码错误。 </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-password" class="col-sm-4 control-label">新密码</label>
                        <div id="forget_psw" class="col-sm-6">
                            <input type="password" class="form-control" id="input-password-forget" placeholder="请输入新密码。">
                            <span id="forgetPswSpan" class="glyphicon glyphicon-remove form-control-feedback" style="display:none" aria-hidden="true"></span>
                            <span id="inputError2StatusForgetPsw" style="display:none" class="help-block"> 要求由字母（大小写不限）、数字、下划线组成的6-15位字符。 </span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link pull-left" id="btn-forget-login"> 返回登陆 </button>
                <button type="button" class="btn btn-success" id="btn-resetpassword-xg" disabled="true"> 确认修改密码 </button>
            </div>
        </div>
    </div>
</div>

