
<div class="modal fade in" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="loginModalLabel"> 登陆 </h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="input-phone" class="col-sm-3 control-label">手机</label>
                        <div  id="login_phone" class="col-sm-7">
                            <input type="tel" class="form-control" id="input-phone-login" placeholder="手机号">
                            <span id="loginSpan" class="glyphicon glyphicon-remove form-control-feedback" style="display:none"  aria-hidden="true"></span>
                            <span id="inputError2StatusLogin" style="display:none" class="help-block"> 手机号码不正确。 </span>
                            <span id="inputError2StatusLogin-no-register" style="display:none" class="help-block"> 该手机号未注册。 </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-password" class="col-sm-3 control-label">密码</label>
                        <div id="login_psw" class="col-sm-7">
                            <input type="password" class="form-control" id="input-password-login" placeholder="密码">
                            <span id="loginSpanPsw" class="glyphicon glyphicon-remove form-control-feedback" style="display:none"  aria-hidden="true"></span>
                            <span id="inputError2StatusLoginPsw" style="display:none" class="help-block"> 密码不正确。 </span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link pull-left" id="btn-forget">忘记密码</button>
                <button type="button" class="btn btn-link pull-left" id="btn-log-register">注册新用户</button>
                <button type="button" class="btn btn-success" id="btn-login-dl">登陆</button>
            </div>
        </div>
    </div>
</div>