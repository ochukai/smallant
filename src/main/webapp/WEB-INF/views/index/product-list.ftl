<div class="clearfix price-section">
    <div class="container">
        <div class="row pricing-table pricing-variable-height">
          <#list products as product>
          <div class="plan">
              <div class="plan-name">
                  <h2><span class="icon-thumbs-up icon-2x"></span> ${product.name} </h2>
              </div>
              <ul>
                  <#list product.featureList as feature>
                  <li class="plan-feature"> ${feature} </li>
                  </#list>
                  <li class="plan-feature yen"> ${product.defaultPrice} </li>
                  <li class="plan-feature">
                      <a href="/product/${product.id}" class="btn btn-warning btn-plan-select">购买</a>
                  </li>
              </ul>
          </div>
          </#list>
        </div>
    </div>
</div>
