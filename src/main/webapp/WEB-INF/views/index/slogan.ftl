
<div class="slogan-section">
    <div class="container">
        <div class="row slogan-title">
            <div class="col-lg-12 text-center">
                <h2>小蚁记账</h2>
                <h5>您身边的财税专家</h5>
                <hr class="star-primary">
            </div>
        </div>
        <ul>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>成本更加节省</h3>

                        <p>中小企业聘用正规的公司代理记账，费用比雇佣一名普通会计的费用更节省，费用比雇佣一名普通会计的费用更节省。</p>
                    </li>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>服务更加专业</h3>

                        <p>代理记账公司可以为企业提供财会专家团队提供的专业化的服务，他们财务、会计、税收业务全面，解决复杂问题的能力强，最大程度的降低企业的财务风险。</p>
                    </li>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>渠道信息强大</h3>

                        <p>专业代理记账公司有强大的信息渠道，专人收集最新的各行业法律、法规，为企业提供高质量的参谋，帮助企业享受国家相关优惠政策。</p>
                    </li>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>信誉够高、质量保障</h3>

                        <p>正规记账公司是经财政部门严格审核成立的，信誉程度高，质量有保障。</p>
                    </li>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>工作水准更高</h3>

                        <p>高水准的代理记账公司能够承担起会计核算、会计管理的全部工作。将企业经营者从烦琐的非业务之外的应酬中解放出来，成为企业贴身的财务问题代理人。</p>
                    </li>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                    <li>
                        <i class="icon-star-empty"></i>

                        <h3>工作水准更高</h3>

                        <p>高水准的代理记账公司能够承担起会计核算、会计管理的全部工作。将企业经营者从烦琐的非业务之外的应酬中解放出来，成为企业贴身的财务问题代理人。</p>
                    </li>
                </div>
            </div>
        </ul>
    </div>
</div>