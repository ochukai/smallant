<div class="feature-section" id="features">
    <div class="container">
        <div class="row feature-title">
            <div class="col-lg-12 text-center">
                <h2>我们的特点</h2>
                <hr class="star-primary">
            </div>
        </div>

        <ul>
            <div class="row">
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-time"></i>
                        <h3>办理税务报到</h3>
                        <p>会计会与您约见，一起办理税务报到，会计会与您约见，一起办理税务报到</p>
                    </li>
                </div>
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-heart-empty"></i>
                        <h3>5分钟快速响应</h3>
                        <p>付款后5分钟内，立即与您联系，付款后5分钟内，立即与您联系</p>
                    </li>
                </div>
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-time"></i>
                        <h3>5分钟快速响应</h3>
                        <p>付款后5分钟内，立即与您联系，付款后5分钟内，立即与您联系</p>
                    </li>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-usd"></i>
                        <h3>代理记账</h3>
                        <p>之后每个月的税务，都会由会计为您报账</p>
                    </li>
                </div>
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-thumbs-up"></i>
                        <h3>会计一对一服务</h3>
                        <p>分配一名专业会计，一对一提供服务</p>
                    </li>
                </div>
                <div class="col-md-4 features">
                    <li>
                        <i class="glyphicon glyphicon-usd"></i>
                        <h3>代理记账</h3>
                        <p>之后每个月的税务，都会由会计为您报账</p>
                    </li>
                </div>
            </div>
        </ul>
    </div>
</div>