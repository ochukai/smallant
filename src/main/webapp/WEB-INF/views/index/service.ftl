<div class="service-process">
    <div class="container">
        <div class="row service-title">
            <div class="col-lg-12 text-center">
                <h2>我们的服务流程</h2>
                <hr class="star-primary">
            </div>
        </div>
        <div class="service-b">
            <div class="ser-pro-lnr">
                <span><i class="lnr lnr-pointer-up"></i></span>
                <p>1. 挑选套餐</p>
            </div>
            <span class="arrow-right pull-left">>>></span>

            <div class="ser-pro-lnr">
                <span><i class="lnr lnr-checkmark-circle"></i></span>
                <p>2. 付款</p>
            </div>
            <span class="arrow-right pull-left">>>></span>
            <div class="ser-pro-lnr">
                <span><i class="lnr lnr-phone"></i></span>
                <p>3. 客服回访</p>
            </div>
            <span class="arrow-right pull-left">>>></span>
            <div class="ser-pro-lnr">
                <span><i class="lnr lnr-file-empty"></i></span>
                <p>4. 填写资料</p>
            </div>
            <span class="arrow-right pull-left">>>></span>
            <div class="ser-pro-lnr">
                <span><i class="lnr lnr-smile"></i></span>
                <p>5. 开始服务</p>
            </div>
        </div>
    </div>
</div>
