<#macro page title>
<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Alina, Oliver">

    <title>${title?html} - 小蚁财务</title>

    <link href="/public/favicon.ico" type="image/x-icon" rel=icon>
    <link href="/public/favicon.ico" type="image/x-icon" rel="shortcut icon">

    <link rel="stylesheet" href="/public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/public/font-Awesome/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/public/css/base.css"/>
    <link rel="stylesheet" href="/public/css/main.css"/>
    
    <script src="/public/js/config.js"></script>
    <script src="/public/js/lib/require.js"></script>
    <script src="/public/js/app/main.js"></script>
    
<!--
    <script src="/public/js/modernizr-2.8.3.min.js"></script>
    <script src="/public/js/jquery-1.11.3.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
    <script src="/public/js/jquery.storage.js"></script>
    <script src="/public/js/jquery.validate.min.js"></script>
    <script src="/public/js/jquery.cityselect.js"></script>
    <script src="/public/js/main.js"></script>
-->
</head>

<body>

    <#include "/nav.ftl">

    <#nested>
    
    <#include "/footer.ftl">
    
    <div class="scroll-to-top">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </div>
    
    <#include "/modal/forget.ftl">
    <#include "/modal/register.ftl">
    <#include "/modal/login.ftl">
    
</body>
</html>
</#macro>