<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="关于我们">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>关于我们 </li>
        </ul>
    </div>
    
    <div class="about-us">
        <div class="container">
            <div class="row slogan-title">
                <div class="col-lg-12 text-center">
                    <h2>关于我们</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="about-b">
                <p>一个人做的梦，就只能是个梦，一群人怀着同一个梦想，就是真实。小蚁记账，让梦想成为真实。</p>
    
                <p> 小蚁记账创始于2015年，自创办之日起我们就秉承着“5分钟快速响应，会计一对一服务，及时办理税务报道，代理记账”的原则走到今天。“小蚁记账，梦想起航，创业助跑，我们最棒”是我们的口号，相信小蚁记账的梦想之路能够越走越宽广！</p>
                <p> 小蚁记账创始于2015年，自创办之日起我们就秉承着“5分钟快速响应，会计一对一服务，及时办理税务报道，代理记账”的原则走到今天。“小蚁记账，梦想起航，创业助跑，我们最棒”是我们的口号，相信小蚁记账的梦想之路能够越走越宽广！</p>
                <p> 小蚁记账创始于2015年，自创办之日起我们就秉承着“5分钟快速响应，会计一对一服务，及时办理税务报道，代理记账”的原则走到今天。“小蚁记账，梦想起航，创业助跑，我们最棒”是我们的口号，相信小蚁记账的梦想之路能够越走越宽广！</p>
            </div>
        </div>
    </div>
    
    <div class="slogan-section">
        <div class="container">
            <div class="row slogan-title">
                <div class="col-lg-12 text-center">
                    <h2>小蚁记账</h2>
                    <h5>您身边的财税专家</h5>
                    <hr class="star-primary">
                </div>
            </div>
            <ul>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>成本更加节省</h3>
    
                            <p>中小企业聘用正规的公司代理记账，费用比雇佣一名普通会计的费用更节省，费用比雇佣一名普通会计的费用更节省。</p>
                        </li>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>服务更加专业</h3>
    
                            <p>代理记账公司可以为企业提供财会专家团队提供的专业化的服务，他们财务、会计、税收业务全面，解决复杂问题的能力强，最大程度的降低企业的财务风险。</p>
                        </li>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>渠道信息强大</h3>
    
                            <p>专业代理记账公司有强大的信息渠道，专人收集最新的各行业法律、法规，为企业提供高质量的参谋，帮助企业享受国家相关优惠政策。</p>
                        </li>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>信誉够高、质量保障</h3>
    
                            <p>正规记账公司是经财政部门严格审核成立的，信誉程度高，质量有保障。</p>
                        </li>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>工作水准更高</h3>
    
                            <p>高水准的代理记账公司能够承担起会计核算、会计管理的全部工作。将企业经营者从烦琐的非业务之外的应酬中解放出来，成为企业贴身的财务问题代理人。</p>
                        </li>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 slogan-b">
                        <li>
                            <i class="icon-star-empty"></i>
    
                            <h3>工作水准更高</h3>
    
                            <p>高水准的代理记账公司能够承担起会计核算、会计管理的全部工作。将企业经营者从烦琐的非业务之外的应酬中解放出来，成为企业贴身的财务问题代理人。</p>
                        </li>
                    </div>
                </div>
            </ul>
        </div>
    </div>
    
</@com.page>

</#escape>