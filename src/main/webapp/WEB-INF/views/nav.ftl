<div class="navbar-fixed-top navigation navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#smallant-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/public/images/logo.png"/>
            </a>

            <span class="brand-text">掌上记账 创业无忧</span>
        </div>

        <div class="collapse navbar-collapse" id="smallant-navbar-collapse">
            
            <div class="nav navbar-nav navbar-right" style="display:inline-block" id="no-login">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-reg">
                        <a href="" id="btn-login"> 登录 </a>
                        |
                        <a href="" id="btn-register"> 注册 </a>
                    </button>
                </div>
            </div>

            <ul class="nav navbar-nav navbar-right after-login div-login-success" id="logined" style="display:none">
                <li><a href="/order/list"><span class="icon icon-user"></span> 13188873073</a></li>
            </ul>

            <div class="telphone navbar-right">
                <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 400-8000-1234
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">主页</a></li>
                <li><a href="/about">关于我们</a></li>
            </ul>
        </div>

    </div>
</div>