<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="${product.name}">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>商品详情</li>
        </ul>
    </div>

    <div class="container commodity">
        <div class="row">
            <div class="col-md-6">
                <img class="img-responsive commodity-img" src="${product.pic}" alt="${product.name}" />
            </div>
    
            <div class="col-md-6 commodity-detail">
                <h2> ${product.name} </h2>
                <p> ${features} </p>
    
                <hr />
    
                <form class="form-horizontal">
                    <input type="hidden" name="id" value="${product.id}" />
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">时间</label>
                        <div class="col-sm-9">
                            <div id="duration" class="type">
                                <span data-value="1">1个月</span>
                                <span data-value="3">3个月</span>
                                <span data-value="6">6个月</span>
                                <span data-value="12">12个月</span>
                            </div>
                        </div>
                    </div>
                    
                    <#list props as prop>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">${prop.name}</label>
                        <div class="col-sm-9">
                            <div id="${prop.name}" class="type">
                                <#list prop.values as value>
                                <span data-value="${value.id}">${value.value}</span>
                                </#list>
                            </div>
                        </div>
                    </div>
                    </#list>
    
                    <hr />
    
                    <div class="form-group commodity-price">
                        <label class="col-sm-2 control-label">价格</label>
                        <div class="col-sm-9">
                            <p class="form-control-static text-danger">--</p>
                        </div>
                    </div>
                    
                    <#list product.prices as price>
                    <input data-group="price" type="hidden" name="${price.propIds}" value="${price.price}" />
                    </#list>
    
                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="button" class="btn btn-reg btn-commodity-select disabled">立即购买</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12 commodity-description">
                <#noescape>${product.description}</#noescape>
            </div>
        </div>
    </div>
    
    <script src='/public/js/app/product-detail.js'></script>
</@com.page>

</#escape>
