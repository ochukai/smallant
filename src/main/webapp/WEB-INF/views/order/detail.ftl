<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="订单详情">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>订单详情</li>
        </ul>
    </div>
    
    <div class="container order-list-container">
        <div class="row">
            <div class="col-sm-3">
                <#include "/order/sider.ftl">
            </div>
    
            <div class="col-sm-9 order-list">
                <div class="order-detail-container">
                    <div class="order-list-item order-detail-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>单据编号：<span class="text-navy">${order.orderId}</span></h4>
                                <p><span>${order.productName}</span>，<span>${order.price}</span>元</p>
                                <p>
                                    <span><strong>套餐下单日期：</strong> ${order.createDate?datetime}</span>
                                </p>
                                <br/>
                                <p>
                                    <span>
                                        <strong>服务起始日期：</strong> 
                                        <#if order.startTime??>${order.startTime?datetime}<#else> 未付款 </#if>
                                    </span>
                                </p>
                                <p>
                                    <span>
                                        <strong>服务终止日期：</strong> 
                                        <#if order.endTime??>${order.endTime?datetime}<#else> 未付款 </#if>
                                    </span>
                                </p>
                                <p class="sp-indent">
                                    <span><strong>持续期限：</strong> <span>${order.duration}</span>个月</span>
                                </p>
                            </div>
                
                            <div class="col-sm-6 text-right">
                                <address class="addr">
                                    <strong>${order.companyInfo.name}</strong><br>
                                    ${order.companyInfo.receiverAddress}<br>
                                    <abbr title="Phone">Tel：</abbr> <span>(86) ${order.companyInfo.phone}</span>&nbsp;&nbsp;<span>${order.companyInfo.receiverPhone}</span></span>
                                </address>
                                <p>
                                    <span><strong>公司代表人：</strong> ${order.companyInfo.manager}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <p class="tip-text">
                        <span>本订单当前月份流程进度：</span>
                    </p>
                    <div class="order-list-item order-detail-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>
                                    <span><strong>套餐服务进度：</strong> 
                                    <#if value == data && order.status == 0>
                                    <span class="text-danger">订单未付款，服务未被受理</span>
                                    <#else>正在受理中...
                                    </#if>
                                    </span>
                                </p>
                                <div>
                                    <p><strong>套餐服务流程：</strong></p>
                                    <#list progressList as progress>

                                        <#if value == data && progress.status == 0 && progress.workFlowId == 1>
                                            <p>${progress.expectEndDate?date} <span>票据收集</span> <span class="text-success">进行中<span>&nbsp;</span><i class="icon-spinner"></i></span></p>
                                        <#elseif progress.workFlowId == 1>
                                            <p>${progress.expectEndDate?date} <span>票据收集</span> <span class="text-danger">已完成<span>&nbsp;</span><i class="icon-ok"></i></span></p>
                                        </#if>
                                        
                                        <#if value == data && progress.status == 0 && progress.workFlowId == 2>
                                            <p>${progress.expectEndDate?date} <span>会计受理</span> <span class="text-success">进行中<span>&nbsp;</span><i class="icon-spinner"></i></span></p>
                                        <#elseif progress.workFlowId == 2>
                                            <p>${progress.expectEndDate?date} <span>会计受理</span> <span class="text-danger">已完成<span>&nbsp;</span><i class="icon-ok"></i></span></p>
                                        </#if>
                                        
                                        <#if value == data && progress.status == 0 && progress.workFlowId == 3>
                                            <p>${progress.expectEndDate?date} <span>会计记账</span> <span class="text-success">进行中<span>&nbsp;</span><i class="icon-spinner"></i></span></p>
                                        <#elseif progress.workFlowId == 3>
                                            <p>${progress.expectEndDate?date} <span>会计记账</span> <span class="text-danger">已完成<span>&nbsp;</span><i class="icon-ok"></i></span></p>
                                        </#if>
                                        
                                        <#if value == data && progress.status == 0 && progress.workFlowId == 4>
                                            <p>${progress.expectEndDate?date} <span>纳税申报</span> <span class="text-success">进行中<span>&nbsp;</span><i class="icon-spinner"></i></span></p>
                                        <#elseif progress.workFlowId == 4>
                                            <p>${progress.expectEndDate?date} <span>纳税申报</span> <span class="text-danger">已完成<span>&nbsp;</span><i class="icon-ok"></i></span></p>
                                        </#if>
                                    </#list>
                                </div>
                            </div>
                
                            <div class="col-sm-6 text-right">
                                <p>
                                    <span><strong>查询月份：${month}</span>
                                </p>
                                <p><span>${order.productName}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <#include "modal/pay-code.ftl">
    <#include "/modal/company-info.ftl">
</@com.page>

</#escape>