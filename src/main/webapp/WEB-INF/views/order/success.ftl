<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="订单支付成功">
    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>订单支付成功 </li>
        </ul>
    </div>
    
    <div class="container order-info">
        <div class="order-info-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="col-md-3 text-center">
                        <span class="mark"><i class="glyphicon glyphicon-ok"></i></span>
                    </div>
                    <div class="col-md-9 order-detail">
                        <h2>恭喜您的支付已完成！</h2>
                        <p>订单号为：${orderId}</p>
                        <p>我们的客服人员稍后将会与您联系，请保持您的手机畅通，谢谢。</p>
                        <hr />
                        <p>为了我们更好的为您服务，接下来您需要 <strong> 填写公司信息 </strong>，也可以稍后再订单中心中填写。</p>
                        <hr />
                        <button id="btn-fill-compnay-info" type="button" 
                                class="btn btn-success btn-lg"
                                data-order="${orderId}">填写公司信息</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="/public/js/app/pay-success.js"></script>
    
    <#include "/modal/company-info.ftl">
</@com.page>

</#escape>


