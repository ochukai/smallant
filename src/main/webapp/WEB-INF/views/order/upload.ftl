<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="订单列表">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>上传图片</li>
        </ul>
    </div>
  
    <div class="container order-list-container">
        <div class="row">
            <div class="col-sm-3">
                <#include "/order/sider.ftl">
            </div>
    
    
    
    
    
            <div class="col-sm-9 order-list">
                <div id ="content"> 
                     <fieldset><legend>上传图片 </legend> 
                     <div class="order-list-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4><font color="#F00">注意事项</font></h4>
                                <p>
                                    <span><strong>1：</strong> 上传前请将图片的名字改为 年月-图片类型，<br> ----如 201510-交通发票.jpg</span>
                                    <br>
                                    <span><strong>2：</strong> 请上传清晰的票据扫描件，或者照片。</span>
                                    <br>
                                    <span><strong>3：</strong> 如果有 多张 相同类型 相同日期 的的票据图片，
                                        <br> ----请将图片命名为  年月-图片类型-编号，
                                        <br> ------如 201510-交通发票-1.jpg
                                        <br> ------如 201510-交通发票-2.jpg
                                        <br> ----否则会覆盖之前上传的同名图片。
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                     </fieldset > 
                     <form action="UploadFile" method="post" id="iconForm" enctype="multipart/form-data">
                        <table">
                            <tr>
                                <td>图片</td>
                                <td><input id="iconImg" name="iconImg" type="file" /></td>
                                <td><input type="submit" value="上传"/></td>
                            </tr>
                        </table>
                    </form>
            </div>
            </div>
        </div>
    </div>

    <#include "/modal/pay-code.ftl">
    <#include "/modal/company-info.ftl">
</@com.page>

</#escape>