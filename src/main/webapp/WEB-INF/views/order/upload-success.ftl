<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="订单列表">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>上传图片</li>
        </ul>
    </div>
  
    <div class="container order-list-container">
        <div class="row">
            <div class="col-sm-3">
                <#include "/order/sider.ftl">
            </div>
            <div class="col-sm-9 order-list">
                <div id ="content"> 
                     <fieldset><legend>上传成功 !   </legend> 
                     </fieldset > 
                     <form action="UploadFile" method="post" id="iconForm" enctype="multipart/form-data">
                        <table">
                            <tr>
                                <td>图片</td>
                                <td><input id="iconImg" name="iconImg" type="file" /></td>
                                <td><input type="submit" value="上传"/></td>
                            </tr>
                        </table>
                    </form>
            </div>
            </div>
        </div>
    </div>

    <#include "/modal/pay-code.ftl">
    <#include "/modal/company-info.ftl">
</@com.page>

</#escape>