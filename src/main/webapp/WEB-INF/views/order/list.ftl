<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="订单列表">

    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>订单列表</li>
        </ul>
    </div>
  
    <div class="container order-list-container">
        <div class="row">
            <div class="col-sm-3">
                <#include "/order/sider.ftl">
            </div>

            <div class="col-sm-9 order-list">
                <#list orders as order>
                <#if value == data && order.status == 0>
                <div class="order-list-item">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>单据编号：<span class="text-navy">${order.orderId}</span></h4>
                            <p><span>${order.productName}, ${order.duration}个月, ${order.price}元</span></p>
                            <p>
                                <span><strong>下单日期：</strong> ${order.createDate?datetime}</span>
                            </p>
                        </div>
    
                        <div class="col-sm-6 text-right">
                            <address class="addr">
                                <strong>${order.companyInfo.name}</strong><br> ${order.companyInfo.receiverAddress}<br>
                                <abbr title="Phone">Tel：</abbr> <span>${order.companyInfo.phone}</span>&nbsp;&nbsp;<span>${order.companyInfo.receiverPhone}</span></span>
                            </address>
                            <p>
                                <span><strong>公司代表人：</strong> ${order.companyInfo.manager}</span>
                            </p>
                            <a href="/order/orderToBuy/${order.orderId}" class="btn btn-success list-item-btn">立即付款</button>
                            <a href="/order/detail/${order.orderId}" class="btn btn-default list-item-btn">订单详情</a>
                            <button href="#" class="btn btn-default list-item-btn">取消订单</button>
                        </div>
                    </div>
                </div>
                </#if> 
                </#list>
                <p class="tip-text"><span>已完成购买订单</span></p>
                
<#list orders as order>
             <#if value == data && order.status == 1>
                <div class="order-list-item">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>单据编号：<span class="text-navy">${order.orderId}</span></h4>
                            <p><span>${order.productName}, ${order.duration}个月</span></p>
                            <p>
                                <span><strong>下单日期：</strong> ${order.createDate?datetime}</span>
                            </p>
                        </div>
    
                        <div class="col-sm-6 text-right">
                            <address class="addr">
                                <strong>${order.companyInfo.name}</strong>
                                <br> 
                                ${order.companyInfo.receiverAddress}
                                <br>
                                <abbr title="Phone">Tel：</abbr> 
                                <span> ${order.companyInfo.phone} </span>
                                <span> ${order.companyInfo.receiverPhone} </span>
                            </address>
                            <p>
                                <span><strong>公司代表人：</strong> ${order.companyInfo.manager}</span>
                            </p>
                            <a href="/order/detail/${order.orderId}" class="btn btn-default list-item-btn">订单详情</a>
                        </div>
                    </div>
                </div>
                </#if> 
                </#list>
            </div>
        </div>
    </div>

    <#include "/modal/pay-code.ftl">
    <#include "/modal/company-info.ftl">
</@com.page>

</#escape>