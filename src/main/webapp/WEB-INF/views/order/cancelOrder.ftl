
<div class="modal fade in" id="modal-cancel-order" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="loginModalLabel"> 取消订单 </h4>
            </div>
            
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="input-phone" class="col-sm-3 control-label">客官，确定取消订单吗？</label>
                    </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link pull-left" id="btn-ok">确定</button>
                <button type="button" class="btn btn-success" id="btn-cancel">取消</button>
            </div>
            
        </div>
    </div>
</div>