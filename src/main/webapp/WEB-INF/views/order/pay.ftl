<#import "/layout/layout.ftl" as com>
<#escape x as x?html>

<@com.page title="下单成功">
    
    <div class="container">
        <ul class="breadcrumb hd-label">
            <li><a href="/">首页</a></li>
            <li>下单结果</li>
        </ul>
    </div>
    
    <div class="container order-info">
        <div class="order-info-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="col-md-3 text-center">
                        <span class="mark"><i class="glyphicon glyphicon-ok"></i></span>
                    </div>
                    <div class="col-md-9 order-detail">
                        <h2>订单提交成功，应付金额 <span class="order-price">${order.price}</span> 元</h2>
                        <p>订单号：${order.orderId}，请您在尽快完成支付。</p>
                        
                        <hr />
                        
                        <div class="form-group">
                            <span class="control-label">所选套餐</span>
                            <p class="form-control-static">${order.productName} - ${props} X ${order.duration}</p>
                        </div>
                        
                        <hr />
                        
                        <form id="payment_form"
                              action="/deposit"
                              method="post"
                              target="_blank">
                            <input type="hidden" name='orderId' value="${order.orderId}"/>
                            <input type="hidden" name='WIDout_trade_no' value="${order.orderId}"/>
                            <input type="hidden" name='WIDsubject' value="${order.productName}"/>
                            <input type="hidden" name='WIDtotal_fee' value="${order.price}"/>
                            <input type="hidden" name='WIDbody' value="${order.productProps}"/>
                            <div class="radio-inline">
                                <label class="pay-method">
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                    <img src="/public/images/alipay-logo.png">
                                </label>
                            </div>
                        </form>
                        
                        <hr />
    
                        <button id="btn-go-to-pay" type="button" class="btn btn-success btn-lg">立即支付</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <#include "modal/pay-code.ftl">
    <script src="/public/js/app/to-pay.js"></script>
</@com.page>

</#escape>