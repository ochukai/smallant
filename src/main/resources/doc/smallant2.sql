/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : smallant

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-09-04 11:00:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `ltd_id` int(11) NOT NULL,
  `password` varchar(64) NOT NULL,
  `login_name` varchar(18) NOT NULL,
  `name` varchar(18) NOT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  `is_online` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk_admin_role` (`role_id`),
  KEY `fk_admin_ltd` (`ltd_id`),
  CONSTRAINT `fk_admin_ltd` FOREIGN KEY (`ltd_id`) REFERENCES `co_ltd` (`id`),
  CONSTRAINT `fk_admin_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'sma01', 'Admin 01', '2015-07-31 23:23:29', '', '2015-08-09 10:31:32', '');
INSERT INTO `admin` VALUES ('10', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', '1111122', '家啊快点啦', '2015-08-09 10:45:29', '', '2015-08-09 14:36:52', '');
INSERT INTO `admin` VALUES ('11', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'nihao', '啊士大夫看见', '2015-08-09 13:41:19', '', '2015-08-09 13:41:19', '');
INSERT INTO `admin` VALUES ('12', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'woshiguanliyuan', '我是管理员', '2015-08-09 13:41:36', '', '2015-08-09 13:41:36', '');
INSERT INTO `admin` VALUES ('13', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'niyeshi', '你也是管理员', '2015-08-09 13:41:50', '', '2015-08-09 13:41:50', '');
INSERT INTO `admin` VALUES ('14', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', '我是kefu', '你是什么', '2015-08-09 13:42:02', '', '2015-08-09 13:42:02', '');
INSERT INTO `admin` VALUES ('15', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', '12345', '一页显示', '2015-08-09 13:42:19', '', '2015-08-09 13:42:19', '');
INSERT INTO `admin` VALUES ('16', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'xiang', '你想干嘛', '2015-08-09 13:42:34', '', '2015-08-09 13:42:34', '');
INSERT INTO `admin` VALUES ('17', '1', '1', 'e10adc3949ba59abbe56e057f20f883e', '嗯也是一个kefu', '士大夫艰苦了', '2015-08-09 13:42:59', '', '2015-08-09 13:42:59', '');

-- ----------------------------
-- Table structure for `company_info`
-- ----------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tax_code` varchar(255) NOT NULL,
  `manager` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `receiver_address` varchar(255) NOT NULL,
  `receiver` varchar(255) NOT NULL,
  `receiver_phone` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `mark_for_delete` bit(1) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_info
-- ----------------------------
INSERT INTO `company_info` VALUES ('1', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 00:32:45', '', '2015-08-26 00:32:45');
INSERT INTO `company_info` VALUES ('2', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 01:00:12', '', '2015-08-26 01:00:12');
INSERT INTO `company_info` VALUES ('3', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 01:13:31', '', '2015-08-26 01:13:31');
INSERT INTO `company_info` VALUES ('4', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 01:13:31', '', '2015-08-26 01:13:31');
INSERT INTO `company_info` VALUES ('5', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 01:15:27', '', '2015-08-26 01:15:27');
INSERT INTO `company_info` VALUES ('6', '1', 'xi小敏有限公司', 's1234234', 'sdfsdf', '021-55462551', '上海-黄浦区-和平小区', 'sdadsfasdf', '021-12345789', '2015-08-26 01:19:49', '', '2015-08-26 01:19:49');

-- ----------------------------
-- Table structure for `co_ltd`
-- ----------------------------
DROP TABLE IF EXISTS `co_ltd`;
CREATE TABLE `co_ltd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `decription` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` tinyint(4) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of co_ltd
-- ----------------------------
INSERT INTO `co_ltd` VALUES ('1', 'smallant', 'our company', '2015-07-05 14:12:48', '0', '2015-07-05 14:13:08');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '订单', '/order');
INSERT INTO `menu` VALUES ('2', '产品', '/product');
INSERT INTO `menu` VALUES ('3', '管理员', '/admin');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `assign_time` datetime DEFAULT NULL,
  `company_info_id` int(11) DEFAULT NULL,
  `price` double NOT NULL,
  `status` int(11) NOT NULL,
  `payment` varchar(255) DEFAULT '' COMMENT '付款方式',
  `pay_time` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_props` varchar(255) NOT NULL,
  `product_pic` varchar(255) DEFAULT NULL,
  `product_price` double NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `con_order_id` (`order_id`) USING BTREE,
  KEY `fk_order_user` (`user_id`),
  KEY `fk_order_admin` (`admin_id`),
  KEY `fk_order_company_info` (`company_info_id`),
  KEY `fk_order_product_id` (`product_id`),
  CONSTRAINT `fk_order_admin` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`),
  CONSTRAINT `fk_order_company_info` FOREIGN KEY (`company_info_id`) REFERENCES `company_info` (`id`),
  CONSTRAINT `fk_order_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `fk_order_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('2', '2479640634078208', '1', '1', null, null, '84', '0', null, null, '10', 'xiaoguimo', '大小-大,颜色-白', '', '14', '6', '2015-08-01', '2016-02-01', '2015-08-22 02:03:48', '2015-08-22 02:03:48', '');
INSERT INTO `orders` VALUES ('3', '2480295030453248', '1', '1', null, null, '39', '0', null, null, '10', 'xiaoguimo', '颜色-黑,大小-大', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '13', '3', '2015-08-01', '2015-11-01', '2015-08-22 13:09:29', '2015-08-22 13:09:29', '');
INSERT INTO `orders` VALUES ('4', '2480297546462208', '1', '11', null, null, '84', '0', null, null, '10', 'xiaoguimo', '大小-大,颜色-白', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '14', '6', '2015-08-01', '2016-02-01', '2015-08-22 13:12:02', '2015-08-22 13:12:02', '');
INSERT INTO `orders` VALUES ('5', '2480300261127168', '1', '11', null, null, '42', '2', null, null, '10', 'xiaoguimo', '大小-大,颜色-白', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '14', '3', '2015-08-01', '2015-11-01', '2015-08-22 13:14:48', '2015-08-22 13:14:48', '');
INSERT INTO `orders` VALUES ('6', '2480495628923904', '1', '12', null, null, '12', '2', null, null, '8', '1234', '大小-小', '/uploads/files/2015-08-12/20150812224555+0800.jpg', '12', '1', '2015-08-01', '2015-09-01', '2015-08-22 16:33:32', '2015-08-22 16:33:32', '');
INSERT INTO `orders` VALUES ('7', '2481671102919680', '1', '11', null, null, '84', '0', null, null, '10', 'xiaoguimo', '大小-大,颜色-白', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '14', '6', '2015-08-01', '2016-02-01', '2015-08-23 12:29:18', '2015-08-23 12:29:18', '');
INSERT INTO `orders` VALUES ('8', '2485020976329728', '1', '12', null, null, '39', '0', null, null, '10', 'xiaoguimo', '颜色-黑,大小-大', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '13', '3', '2015-08-01', '2015-11-01', '2015-08-25 21:16:58', '2015-08-25 21:16:58', '');
INSERT INTO `orders` VALUES ('9', '2485141563438080', '1', '11', null, null, '39', '0', null, null, '10', 'xiaoguimo', '颜色-黑,大小-大', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '13', '3', '2015-08-01', '2015-11-01', '2015-08-25 23:19:38', '2015-08-25 23:19:38', '');
INSERT INTO `orders` VALUES ('10', '2485237817111552', '1', '12', null, '6', '168', '0', null, null, '10', 'xiaoguimo', '大小-大,颜色-白', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '14', '12', '2015-08-01', '2016-08-01', '2015-08-26 00:57:33', '2015-08-26 00:57:33', '');
INSERT INTO `orders` VALUES ('12', '2486505815323648', '1', '10', '2015-08-26 22:27:29', null, '39', '0', '', null, '10', 'xiaoguimo', '颜色-黑,大小-大', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '13', '3', '2015-08-01', '2015-11-01', '2015-08-26 22:27:25', '2015-08-26 22:27:25', '');
INSERT INTO `orders` VALUES ('13', '2486757592232960', '1', '16', '2015-08-27 02:43:32', null, '999', '0', '', null, '15', '外资', '外资性质-企业', '/uploads/files/2015-08-27/20150827001222+0800.jpg', '999', null, null, null, '2015-08-27 02:43:32', '2015-08-27 02:43:32', '');
INSERT INTO `orders` VALUES ('14', '2486759317533696', '1', '17', '2015-08-27 02:45:18', null, '999', '0', '', null, '15', '外资', '外资性质-企业', '/uploads/files/2015-08-27/20150827001222+0800.jpg', '999', '1', null, null, '2015-08-27 02:45:18', '2015-08-27 02:45:18', '');
INSERT INTO `orders` VALUES ('15', '2493661913052160', '1', '13', '2015-08-31 23:46:59', null, '297', '0', '', null, '13', '零申报', '套餐-零申报', 'null', '99', '3', null, null, '2015-08-31 23:46:59', '2015-08-31 23:46:59', '');
INSERT INTO `orders` VALUES ('16', '2493673561011200', '1', '14', '2015-08-31 23:58:50', null, '1797', '0', '', null, '15', '外资', '外资性质-代表处', '/uploads/files/2015-08-27/20150827001222+0800.jpg', '599', '3', null, null, '2015-08-31 23:58:50', '2015-08-31 23:58:50', '');
INSERT INTO `orders` VALUES ('17', '2496564234023936', '1', '15', '2015-09-03 00:59:22', null, '297', '0', '', null, '13', '零申报', '套餐-零申报', 'null', '99', '3', null, null, '2015-09-03 00:59:22', '2015-09-03 00:59:22', '');
INSERT INTO `orders` VALUES ('18', '2496594812466176', '1', '10', '2015-09-03 01:30:29', null, '999', '0', '', null, '15', '外资', '外资性质-企业', '/uploads/files/2015-08-27/20150827001222+0800.jpg', '999', '1', null, null, '2015-09-03 01:30:29', '2015-09-03 01:30:29', '');
INSERT INTO `orders` VALUES ('19', '2498530751820800', '1', '17', '2015-09-04 10:19:49', null, '1197', '0', '', null, '14', '一般纳税人', '进项发票-10张以上', null, '399', '3', null, null, '2015-09-04 10:19:49', '2015-09-04 10:19:49', '');

-- ----------------------------
-- Table structure for `order_progress`
-- ----------------------------
DROP TABLE IF EXISTS `order_progress`;
CREATE TABLE `order_progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `work_flow_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `expect_end_date` datetime NOT NULL,
  `month` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_work_flow` (`work_flow_id`),
  KEY `fk_order__order_id` (`order_id`),
  CONSTRAINT `fk_order_work_flow` FOREIGN KEY (`work_flow_id`) REFERENCES `work_flow` (`id`),
  CONSTRAINT `fk_order__order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_progress
-- ----------------------------

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '显示状态',
  `pic` varchar(255) DEFAULT NULL,
  `default_price` double NOT NULL,
  `features` varchar(255) DEFAULT NULL,
  `description` longtext,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('6', '123', '0', '/uploads/files/2015-08-06/20150806000558+0800.jpg', '123', '第一条;第二条', '123123', '2015-08-06 00:06:30', '', '2015-08-06 00:06:30');
INSERT INTO `product` VALUES ('7', '123', '0', '/uploads/files/2015-08-06/20150806000845+0800.jpg', '123', '第一条;第二条', '123123', '2015-08-06 00:08:57', '', '2015-08-06 00:08:57');
INSERT INTO `product` VALUES ('8', '1234', '0', '/uploads/files/2015-08-12/20150812224555+0800.jpg', '12', '123', '<p>\n	1231231234<img src=\"uploads/images/2015-08-12/20150812231156+0800.png\" width=\"400\" height=\"837\" alt=\"\" />\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<h2 class=\"entry-title\" style=\"font-family:inherit;font-size:2.4rem;font-style:inherit;vertical-align:baseline;\">\n		<a href=\"http://oyaoya.sinaapp.com/remember-password-in-git-bash/\">git bash 记住密码</a>\n	</h2>\n	<div class=\"entry-summary\" style=\"font-family:inherit;font-style:inherit;font-weight:inherit;margin:1.5em 0px 0px;padding:0px 0px 0.8rem;vertical-align:baseline;\">\n		<div id=\"crayon-55d4b39040b79475483155\" class=\"crayon-syntax crayon-theme-ado crayon-font-consolas crayon-os-pc print-yes notranslate\" style=\"font-family:Monaco, MonacoRegular, \'Courier New\', monospace;font-style:inherit;margin:12px 0px;padding:0px;vertical-align:baseline;border:1px solid #999999 !important;font-size:14px !important;background:#FDFDFD !important;\">\n			<div class=\"crayon-plain-wrap\" style=\"border:0px;font-style:inherit;font-weight:inherit;vertical-align:baseline;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;margin:0px !important;padding:0px !important;background:0px 50%;\">\n			</div>\n			<div class=\"crayon-main\" style=\"border:0px;font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;background:0px 50%;\">\n				<table class=\"crayon-table\" style=\"font-size:12px;font-style:inherit;font-weight:inherit;border:none !important;padding:0px !important;width:auto !important;background:none !important;\">\n					<tbody>\n						<tr class=\"crayon-row\">\n							<td class=\"crayon-nums \" style=\"font-style:inherit;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;vertical-align:top !important;color:#DEDEDE !important;background:#333333 !important;\">\n								<div class=\"crayon-nums-content\" style=\"border:0px;font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;font-size:14px !important;background:0px 50%;\">\n									<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n										1\n									</div>\n									<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n										2\n									</div>\n									<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n										3\n									</div>\n									<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n										4\n									</div>\n								</div>\n							</td>\n							<td class=\"crayon-code\" style=\"border:0px;font-style:inherit;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;vertical-align:top !important;background:0px 50%;\">\n								<div class=\"crayon-pre\" style=\"font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;border:none !important;font-size:14px !important;background:none !important;\">\n									<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-1\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n										&nbsp;\n									</div>\n									<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-2\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n										<span class=\"crayon-c\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\"># 因为 window 不允许之间新建 . 开头的文件，所以使用 mv 的方式</span>\n									</div>\n									<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-3\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n										<span class=\"crayon-r\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#800080 !important;\">mv</span><span class=\"crayon-h\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\"> </span><span class=\"crayon-v\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#002D7A !important;\">git</span><span class=\"crayon-o\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\">-</span><span class=\"crayon-i\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\">credentials</span><span class=\"crayon-h\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\"> </span><span class=\"crayon-e\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#004ED0 !important;\">.git</span><span class=\"crayon-o\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\">-</span><span class=\"crayon-i\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\">credentials</span>\n									</div>\n									<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-4\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n										&nbsp;\n									</div>\n								</div>\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</div>\n		</div>\n		<p style=\"font-family:inherit;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">\n			然后编辑 .git-credentials …\n		</p>\n	</div>\n	<div class=\"entry-meta\" style=\"border:0px;font-family:\'Comic Sans MS\';font-size:1.3rem;font-style:inherit;font-weight:inherit;margin:10px 0px;padding:0px;vertical-align:baseline;color:#9EABB3;\">\n		<a href=\"http://oyaoya.sinaapp.com/wamp-mysql-chinese-question-mark/\">2015-08-09</a>&nbsp;<span class=\"cat-links\" style=\"font-family:inherit;font-size:13px;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">on&nbsp;<a href=\"http://oyaoya.sinaapp.com/category/mysql/\">MySQL</a> </span><span class=\"tags-links\" style=\"font-family:inherit;font-size:13px;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">tagged&nbsp;<a href=\"http://oyaoya.sinaapp.com/tag/mysql-2/\">mysql</a>,&nbsp;<a href=\"http://oyaoya.sinaapp.com/tag/%e4%b8%ad%e6%96%87%e4%b9%b1%e7%a0%81/\">中文乱码</a></span>\n	</div>\n	<h2 class=\"entry-title\" style=\"font-family:inherit;font-size:2.4rem;font-style:inherit;vertical-align:baseline;\">\n		<a href=\"http://oyaoya.sinaapp.com/wamp-mysql-chinese-question-mark/\">wamp mysql 中文显示为？</a>\n	</h2>\n	<div class=\"entry-summary\" style=\"font-family:inherit;font-style:inherit;font-weight:inherit;margin:1.5em 0px 0px;padding:0px 0px 0.8rem;vertical-align:baseline;\">\n		<p style=\"font-family:inherit;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">\n			发现中文显示为“？”，然后我看了一下 mysql.ini，添加了两行 [crayon-55d4b3905f2b …\n		</p>\n	</div>\n</p>', '2015-08-06 00:11:25', '', '2015-08-20 00:50:27');
INSERT INTO `product` VALUES ('9', 'ss', '0', '/uploads/files/2015-08-12/20150812232402+0800.jpg', '12', '第一条;第二条', '<img src=\"../uploads/images/2015-08-12/20150812232313+0800.jpg\" width=\"620\" height=\"391\" alt=\"\" />', '2015-08-12 23:24:02', '', '2015-08-12 23:24:02');
INSERT INTO `product` VALUES ('10', 'xiaoguimo', '0', '/uploads/files/2015-08-20/20150820234356+0800.jpg', '12', '第一条;第二条', '<p>\n	12345677890\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<h2 class=\"entry-title\" style=\"font-family:inherit;font-size:2.4rem;font-style:inherit;vertical-align:baseline;\">\n	<a href=\"http://oyaoya.sinaapp.com/remember-password-in-git-bash/\">git bash 记住密码</a> \n</h2>\n<div class=\"entry-summary\" style=\"font-family:inherit;font-style:inherit;font-weight:inherit;margin:1.5em 0px 0px;padding:0px 0px 0.8rem;vertical-align:baseline;\">\n	<div id=\"crayon-55d4b39040b79475483155\" class=\"crayon-syntax crayon-theme-ado crayon-font-consolas crayon-os-pc print-yes notranslate\" style=\"font-family:Monaco, MonacoRegular, \'Courier New\', monospace;font-style:inherit;margin:12px 0px;padding:0px;vertical-align:baseline;border:1px solid #999999 !important;font-size:14px !important;background:#FDFDFD !important;\">\n		<div class=\"crayon-plain-wrap\" style=\"border:0px;font-style:inherit;font-weight:inherit;vertical-align:baseline;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;margin:0px !important;padding:0px !important;background:0px 50%;\">\n		</div>\n		<div class=\"crayon-main\" style=\"border:0px;font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;background:0px 50%;\">\n			<table class=\"crayon-table\" style=\"font-size:12px;font-style:inherit;font-weight:inherit;border:none !important;padding:0px !important;width:auto !important;background:none !important;\">\n				<tbody>\n					<tr class=\"crayon-row\">\n						<td class=\"crayon-nums \" style=\"font-style:inherit;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;vertical-align:top !important;color:#DEDEDE !important;background:#333333 !important;\">\n							<div class=\"crayon-nums-content\" style=\"border:0px;font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;font-size:14px !important;background:0px 50%;\">\n								<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n									1\n								</div>\n								<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n									2\n								</div>\n								<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n									3\n								</div>\n								<div class=\"crayon-num\" style=\"border:0px;font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;margin:0px;padding:0px 5px;vertical-align:baseline;text-align:center;background:0px 50%;\">\n									4\n								</div>\n							</div>\n						</td>\n						<td class=\"crayon-code\" style=\"border:0px;font-style:inherit;font-family:Consolas, ConsolasRegular, \'Courier New\', monospace !important;vertical-align:top !important;background:0px 50%;\">\n							<div class=\"crayon-pre\" style=\"font-style:inherit;font-weight:inherit;margin:0px;padding:0px;vertical-align:baseline;border:none !important;font-size:14px !important;background:none !important;\">\n								<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-1\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n									&nbsp;\n								</div>\n								<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-2\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n									<span class=\"crayon-c\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\"># 因为 window 不允许之间新建 . 开头的文件，所以使用 mv 的方式</span> \n								</div>\n								<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-3\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n									<span class=\"crayon-r\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#800080 !important;\">mv</span><span class=\"crayon-h\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\"> </span><span class=\"crayon-v\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#002D7A !important;\">git</span><span class=\"crayon-o\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\">-</span><span class=\"crayon-i\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\">credentials</span><span class=\"crayon-h\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\"> </span><span class=\"crayon-e\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#004ED0 !important;\">.git</span><span class=\"crayon-o\" style=\"font-family:inherit;font-size:inherit !important;font-style:inherit;font-weight:inherit !important;vertical-align:baseline;line-height:inherit !important;color:#006FE0 !important;\">-</span><span class=\"crayon-i\" style=\"font-family:inherit;font-style:inherit;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;\">credentials</span> \n								</div>\n								<div class=\"crayon-line\" id=\"crayon-55d4b39040b79475483155-4\" style=\"border:0px;font-family:inherit;font-style:inherit;margin:0px;padding:0px 5px;vertical-align:baseline;font-size:inherit !important;font-weight:inherit !important;background:0px 50%;\">\n									&nbsp;\n								</div>\n							</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n		</div>\n	</div>\n	<p style=\"font-family:inherit;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">\n		然后编辑 .git-credentials …\n	</p>\n</div>\n<div class=\"entry-meta\" style=\"border:0px;font-family:\'Comic Sans MS\';font-size:1.3rem;font-style:inherit;font-weight:inherit;margin:10px 0px;padding:0px;vertical-align:baseline;color:#9EABB3;\">\n	<a href=\"http://oyaoya.sinaapp.com/wamp-mysql-chinese-question-mark/\">2015-08-09</a>&nbsp;<span class=\"cat-links\" style=\"font-family:inherit;font-size:13px;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">on&nbsp;<a href=\"http://oyaoya.sinaapp.com/category/mysql/\">MySQL</a> </span><span class=\"tags-links\" style=\"font-family:inherit;font-size:13px;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">tagged&nbsp;<a href=\"http://oyaoya.sinaapp.com/tag/mysql-2/\">mysql</a>,&nbsp;<a href=\"http://oyaoya.sinaapp.com/tag/%e4%b8%ad%e6%96%87%e4%b9%b1%e7%a0%81/\">中文乱码</a></span> \n</div>\n<h2 class=\"entry-title\" style=\"font-family:inherit;font-size:2.4rem;font-style:inherit;vertical-align:baseline;\">\n	<a href=\"http://oyaoya.sinaapp.com/wamp-mysql-chinese-question-mark/\">wamp mysql 中文显示为？</a> \n</h2>\n<div class=\"entry-summary\" style=\"font-family:inherit;font-style:inherit;font-weight:inherit;margin:1.5em 0px 0px;padding:0px 0px 0.8rem;vertical-align:baseline;\">\n	<p style=\"font-family:inherit;font-style:inherit;font-weight:inherit;vertical-align:baseline;\">\n		发现中文显示为“？”，然后我看了一下 mysql.ini，添加了两行 [crayon-55d4b3905f2b …\n	</p>\n</div>\n<p>\n	<br />\n</p>', '2015-08-17 23:48:30', '', '2015-08-21 20:55:23');
INSERT INTO `product` VALUES ('11', '扶持大学生创业', '1', null, '0', '记账;报税;财税咨询', '          ', '2015-08-26 23:54:05', '', '2015-08-26 23:54:05');
INSERT INTO `product` VALUES ('12', '小规模纳税人', '1', 'null', '199', '记账;报税;财税咨询;信息强大', '          ', '2015-08-26 23:55:01', '', '2015-08-27 02:38:58');
INSERT INTO `product` VALUES ('13', '零申报', '1', 'null', '99', '记账;报税;财务咨询', '          ', '2015-08-27 00:05:51', '', '2015-08-27 00:21:21');
INSERT INTO `product` VALUES ('14', '一般纳税人', '1', null, '299', '记账;报税;财税咨询', '          ', '2015-08-27 00:10:55', '', '2015-08-27 00:10:55');
INSERT INTO `product` VALUES ('15', '外资', '1', '/uploads/files/2015-08-27/20150827001222+0800.jpg', '599', '记账;报税;财税咨询;快速响应;质量保障', 'a', '2015-08-27 00:12:22', '', '2015-08-27 02:38:44');

-- ----------------------------
-- Table structure for `product_prop`
-- ----------------------------
DROP TABLE IF EXISTS `product_prop`;
CREATE TABLE `product_prop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `prop_name` varchar(200) NOT NULL,
  `prop_value` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_prop_1` (`product_id`),
  CONSTRAINT `fk_product_prop_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_prop
-- ----------------------------
INSERT INTO `product_prop` VALUES ('4', '7', '123', '111', '2015-08-06 00:09:38', '', '2015-08-06 00:09:38');
INSERT INTO `product_prop` VALUES ('13', '9', '你好', '受到收到受到收到', '2015-08-12 23:24:02', '', '2015-08-12 23:24:02');
INSERT INTO `product_prop` VALUES ('24', '8', '大小', '大', '2015-08-20 00:50:27', '', '2015-08-20 00:50:27');
INSERT INTO `product_prop` VALUES ('25', '8', '大小', '小', '2015-08-20 00:50:27', '', '2015-08-20 00:50:27');
INSERT INTO `product_prop` VALUES ('45', '10', '大小', '小', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `product_prop` VALUES ('46', '10', '颜色', '黑', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `product_prop` VALUES ('47', '10', '大小', '大', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `product_prop` VALUES ('48', '10', '颜色', '白', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `product_prop` VALUES ('49', '11', '特色', '免费一年', '2015-08-26 23:54:06', '', '2015-08-26 23:54:06');
INSERT INTO `product_prop` VALUES ('50', '14', '进项发票', '10张以上', '2015-08-27 00:10:55', '', '2015-08-27 00:10:55');
INSERT INTO `product_prop` VALUES ('51', '14', '进项发票', '10张以内', '2015-08-27 00:10:55', '', '2015-08-27 00:10:55');
INSERT INTO `product_prop` VALUES ('54', '13', '套餐', '零申报', '2015-08-27 00:21:21', '', '2015-08-27 00:21:21');
INSERT INTO `product_prop` VALUES ('58', '15', '外资性质', '代表处', '2015-08-27 02:38:44', '', '2015-08-27 02:38:44');
INSERT INTO `product_prop` VALUES ('59', '15', '外资性质', '企业', '2015-08-27 02:38:44', '', '2015-08-27 02:38:44');
INSERT INTO `product_prop` VALUES ('60', '12', '套餐', '小规模纳税人', '2015-08-27 02:38:58', '', '2015-08-27 02:38:58');

-- ----------------------------
-- Table structure for `prop_comb_price`
-- ----------------------------
DROP TABLE IF EXISTS `prop_comb_price`;
CREATE TABLE `prop_comb_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `prop_ids` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prop_price_product` (`product_id`),
  CONSTRAINT `fk_prop_price_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prop_comb_price
-- ----------------------------
INSERT INTO `prop_comb_price` VALUES ('7', '9', '13', '13', '2015-08-12 23:24:02', '', '2015-08-12 23:24:02');
INSERT INTO `prop_comb_price` VALUES ('18', '8', '24', '14', '2015-08-20 00:50:27', '', '2015-08-20 00:50:27');
INSERT INTO `prop_comb_price` VALUES ('19', '8', '25', '12', '2015-08-20 00:50:27', '', '2015-08-20 00:50:27');
INSERT INTO `prop_comb_price` VALUES ('36', '10', '45,46', '11', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `prop_comb_price` VALUES ('37', '10', '45,48', '12', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `prop_comb_price` VALUES ('38', '10', '47,46', '13', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `prop_comb_price` VALUES ('39', '10', '47,48', '14', '2015-08-21 20:55:23', '', '2015-08-21 20:55:23');
INSERT INTO `prop_comb_price` VALUES ('40', '11', '49', '0', '2015-08-26 23:54:06', '', '2015-08-26 23:54:06');
INSERT INTO `prop_comb_price` VALUES ('41', '14', '51', '299', '2015-08-27 00:10:55', '', '2015-08-27 00:10:55');
INSERT INTO `prop_comb_price` VALUES ('42', '14', '50', '399', '2015-08-27 00:10:55', '', '2015-08-27 00:10:55');
INSERT INTO `prop_comb_price` VALUES ('45', '13', '54', '99', '2015-08-27 00:21:21', '', '2015-08-27 00:21:21');
INSERT INTO `prop_comb_price` VALUES ('49', '15', '58', '599', '2015-08-27 02:38:44', '', '2015-08-27 02:38:44');
INSERT INTO `prop_comb_price` VALUES ('50', '15', '59', '999', '2015-08-27 02:38:44', '', '2015-08-27 02:38:44');
INSERT INTO `prop_comb_price` VALUES ('51', '12', '60', '199', '2015-08-27 02:38:58', '', '2015-08-27 02:38:58');

-- ----------------------------
-- Table structure for `readily_bill`
-- ----------------------------
DROP TABLE IF EXISTS `readily_bill`;
CREATE TABLE `readily_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `upload_time` datetime NOT NULL,
  `mark_for_delete` bit(1) DEFAULT NULL,
  `order_progress_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_progress_pic` (`order_progress_id`),
  CONSTRAINT `fk_order_progress_pic` FOREIGN KEY (`order_progress_id`) REFERENCES `order_progress` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of readily_bill
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  CONSTRAINT `fk_role_id_id` FOREIGN KEY (`id`) REFERENCES `role_menu` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员');
INSERT INTO `role` VALUES ('2', '客服');

-- ----------------------------
-- Table structure for `role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `menu_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `fk_menu_id_id` (`menu_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `fk_menu_id_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('1', '1');
INSERT INTO `role_menu` VALUES ('2', '1');
INSERT INTO `role_menu` VALUES ('1', '2');
INSERT INTO `role_menu` VALUES ('2', '2');
INSERT INTO `role_menu` VALUES ('1', '3');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(18) NOT NULL,
  `password` varchar(32) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_phone` (`phone`),
  UNIQUE KEY `index_user_token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '13188873073', 'ef1913d275d05312a43cbbc5c88c1042', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMzE4ODg3MzA3MyJ9.ze_iL8hQD3sS8375ontZwCow-qYYHGRFophxIRpneOHxHYonI3XHeqABLEHdimbVxOVUnabHogNIrn2lkE4Mvw', '2015-08-19 02:02:43', '', '2015-08-19 02:02:43');

-- ----------------------------
-- Table structure for `work_flow`
-- ----------------------------
DROP TABLE IF EXISTS `work_flow`;
CREATE TABLE `work_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `expect_duration` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `mark_for_delete` bit(1) NOT NULL DEFAULT b'0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of work_flow
-- ----------------------------
INSERT INTO `work_flow` VALUES ('1', '0', '收集票据', '客户自行收集票据', '28', '2015-08-02 23:32:54', '', '2015-08-02 23:32:51');
INSERT INTO `work_flow` VALUES ('2', '0', '寄给会计', '客户在收集完票据后寄给会计', '3', '2015-08-27 23:59:42', '', '2015-08-27 23:59:48');
INSERT INTO `work_flow` VALUES ('3', '0', '会计记账', '会计进行记账整理', '14', '2015-08-28 00:02:01', '', '2015-08-28 00:02:04');
INSERT INTO `work_flow` VALUES ('4', '0', '纳税申报', '进行纳税申报', '15', '2015-08-28 00:02:45', '', '2015-08-28 00:02:48');
DROP TRIGGER IF EXISTS `trigger_after_order_insert`;
DELIMITER ;;
CREATE TRIGGER `trigger_after_order_insert` BEFORE INSERT ON `orders` FOR EACH ROW set 
new.assign_time = now(),
new.admin_id = (
    select 
        a.id
    from admin a
    left join
        (
            select 
                o.admin_id, 
                count(1) count 
            from orders o 
            where 
                o.mark_for_delete = false 
                and o.status != 2 
                and o.status != 3 
            group by o.admin_id
        ) b 
        on a.id = b.admin_id
    where 
        a.mark_for_delete = false
        -- and a.is_online = true
    order by b.count asc
    limit 1
)
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tr_order`;
DELIMITER ;;
CREATE TRIGGER `tr_order` AFTER UPDATE ON `orders` FOR EACH ROW BEGIN
	declare start_month integer;
	declare end_month integer;
	declare start_year integer;
	declare end_year integer;
	declare count integer;
	declare mark integer;
   
   set start_month = month(new.start_time);
   set end_month = Month(new.end_time);
   set start_year = YEAR(new.start_time);
   set end_year = YEAR(new.end_time);
   set count  = end_month - start_month + 1;
   set mark = 0;
   
    IF (NEW.status =1 && old.status = 0) THEN

        
        if(start_year < end_year)
        then
            set count = count +12;
        end IF;
        while mark < count DO
            insert into `order_progress` (
			  `order_id`,
			  `user_id`,
			  `work_flow_id`,
			  `status`,
			  `start_date`,
			  `expect_end_date`,
			  `month`,
			  `create_date`,
			  `mark_for_delete`,
			  `update_date`
			) 
            select 
				new.id,
				new.user_id,
				flow.id,
				0,
				case when start_month + mark <= 12 
					then concat(start_year,"-" ,(start_month + mark),"-1") 
					else concat(end_year, "-" , (start_month + mark -12),"-1") end,
				case when flow.name = "收集票据" 
					then (
						case when start_month + mark <= 12 
							then concat(start_year,"-" ,(start_month -1 + mark),'-', expect_duration) 
							else concat(end_year, "-",(start_month -1 + mark -12),'-' , expect_duration) end
					)
					ELSE (
						case when start_month + mark <= 12 
							then concat(start_year,"-" ,(start_month + mark),'-', expect_duration) 
							else concat(end_year, "-",(start_month + mark -12),'-' , expect_duration) end
					) END,
				case when start_month + mark <= 12 
					then start_year*100 + (start_month + mark) 
					else end_year*100 + (start_month + mark -12)end,
				now(),
				0,
				now()
			from
				work_flow as flow;

           set mark = mark + 1;
        end WHILE;
    END IF;
END
;;
DELIMITER ;
