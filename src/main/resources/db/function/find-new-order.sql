DROP FUNCTION IF EXISTS find_new_order ;

CREATE FUNCTION find_new_order ( admin_id INT, last_find_date datetime )
RETURNS INT
DETERMINISTIC
BEGIN
    DECLARE order_id INT;
    
    

    SELECT 
        id
    INTO
        order_id
    FROM orders 
    WHERE 
        create_date > last_find_date
    ORDER BY create_date DESC
    LIMIT 1;

    if order_id is null then
        return -1;
    end if;
    
    update orders
    set 
        assign_time = now(),
        admin_id = admin_id
    where
        id = order_id;
    
    RETURN order_id;
END